package com.hdm.commerce.catalog.facade.server.services.search.expression.solr;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.common.exception.ExceptionHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.expression.SearchExpressionProvider;
import com.ibm.commerce.foundation.server.services.search.expression.solr.AbstractSolrSearchExpressionProvider;

public class HDMSolrRESTSearchByFacetExpressionProvider extends
		AbstractSolrSearchExpressionProvider implements
		SearchExpressionProvider {
	private static final String CLASSNAME = HDMSolrRESTSearchByFacetExpressionProvider.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;

	public HDMSolrRESTSearchByFacetExpressionProvider(String componentId) {
		iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria)
			throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		try {
			super.invoke(selectionCriteria);

			List lst = getControlParameterValues(HDMConstants._WCF_SEARCH_FACET_PROPERTIES);
			final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
			String facetName = null;
			/**
			 * Logic for replacing the contract id with the physical store id.
			 */
			String physicalStoreId = HDMUtils
					.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
							+ storeId);
			boolean isPriceFacet = false;
			boolean isItmFulfilmntFacet = false;
			for (int i = 0; i < lst.size(); i++) {
				final String facetFields = lst.get(i).toString();
				if (facetFields.indexOf(HDMConstants.PRICE_FACET_NAME_PREFEIX) != -1) {
					final String oldFacetValue = facetFields;
					String strCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
					String[] splitFacet = oldFacetValue.split(":");
					if (splitFacet[0]
							.indexOf(HDMConstants.PRICE_FACET_NAME_PREFEIX
									.concat(strCurrencyCode)
									.concat(HDMConstants.UNDERSCORE)
									.concat(physicalStoreId)) == -1) {

						removeControlParameterValue(
								HDMConstants._WCF_SEARCH_FACET_PROPERTIES,
								oldFacetValue);

						String str = splitFacet[0].concat(
								HDMConstants.UNDERSCORE)
								.concat(physicalStoreId);
						if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
							String proUserLevel = HDMUtils
									.getCookieValue(HDMConstants.CUSTOMERLEVEL_COOKIENAME);
							if (proUserLevel != null) {
								str = str.concat(HDMConstants.UNDERSCORE)
										.concat(proUserLevel);
							}
						}

						facetName = str.concat(":").concat(splitFacet[1]);
						addControlParameterValue(
								HDMConstants._WCF_SEARCH_FACET_PROPERTIES,
								facetName);
						isPriceFacet = true;
						if (isItmFulfilmntFacet) {
							break;
						}
					}
				}

				if (facetFields != null
						&& facetFields.indexOf("itmFulfillmentType") != -1) {
					String oldFacetValue = facetFields;
					String market_Id = HDMUtils
							.getCookieValue(HDMConstants.MARKETID_COOKIENAME
									+ "_" + storeId);
					removeControlParameterValue(
							HDMConstants._WCF_SEARCH_FACET_PROPERTIES,
							oldFacetValue);
					oldFacetValue = oldFacetValue.replaceAll("<PH>",
							physicalStoreId);
					oldFacetValue = oldFacetValue.replaceAll("<MK>", market_Id);
					oldFacetValue = oldFacetValue.replaceAll("<strBuyEx>",
							"storeBuyableExclude");
					oldFacetValue = oldFacetValue.replaceAll("<onlne>",
							"onlineOnlySellingStores");
					if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
						oldFacetValue = oldFacetValue.replaceAll("storeBuyableExclude",
								HDMConstants.PRO_EXCLUDESTORE);
					}
					
					facetName = oldFacetValue.replaceAll("<stronly>",
							"storeonlyavailable_ntk_cs");

					addControlParameterValue(
							HDMConstants._WCF_SEARCH_FACET_PROPERTIES,
							facetName);
					isItmFulfilmntFacet = true;
					if (isPriceFacet) {
						break;
					}
				}
			}
		} catch (Exception e) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.WARNING, CLASSNAME,
						"invoke(SelectionCriteria, Object[])",
						"No final usable contract: {0}",
						ExceptionHelper.convertStackTraceToString(e));
			}

			throw new RuntimeException(e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}
