package com.hdm.commerce.store.facade.server.util;

import java.math.BigDecimal;
import java.util.logging.Logger;

import com.hdm.commerce.store.facade.datatypes.PhysicalStoreGeoCodeType;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;

public class DistanceCalculationHelper {
	public static final String COPYRIGHT = "(c) Copyright International Business Machines Corporation 1996,2008";
	private static final String CLASSNAME = DistanceCalculationHelper.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(DistanceCalculationHelper.class);
	private static final double EARTH_RADIUS = 6367.0D;
	private static final double ONE_DEGREE_LATITUDE_DISTANCE = 111.09999999999999D;
	private static final double ONE_DEGREE_LONGITUDE_AT_EQUATOR = 111.0D;
	private static final float FLOAT_RATIO_MILE_TO_KM = 0.6213712F;
	private static final double DOUBLE_RATIO_MILE_TO_KM = 0.621371192237D;
	private static final double ONE_PERCENT = 1.01D;
	private static final int NINTY_DEGREE = 90;
	private static final int ONE_EIGHTY_DEGREE = 180;
	private static final int THREE_SIXTY_DEGREE = 360;
	private static final double ONE_EIGHTY_DEGREE_DOUBLE = 180.0D;

	public static double getDistanceBetweenGeoCodes(
			PhysicalStoreGeoCodeType aGeoCode1,
			PhysicalStoreGeoCodeType aGeoCode2) {
		String METHODNAME = "getDistanceBetweenGeoCodes";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, "getDistanceBetweenGeoCodes",
					new Object[] { aGeoCode1, aGeoCode2 });
		}

		double nDistance = 0.0D;
		double nLat1 = degreesToRadians(aGeoCode1.getLatitude().doubleValue());
		double nLon1 = degreesToRadians(aGeoCode1.getLongitude().doubleValue());
		double nLat2 = degreesToRadians(aGeoCode2.getLatitude().doubleValue());
		double nLon2 = degreesToRadians(aGeoCode2.getLongitude().doubleValue());

		nDistance = 12734.0D * Math.asin(Math.sqrt(Math.pow(
				Math.sin((nLat1 - nLat2) / 2.0D), 2.0D)
				+ Math.cos(nLat1)
				* Math.cos(nLat2)
				* Math.pow(Math.sin((nLon1 - nLon2) / 2.0D), 2.0D)));

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, "getDistanceBetweenGeoCodes", new Double(
					nDistance));
		}

		return nDistance;
	}

	public static double convertDoubleKilometersToMiles(double anKilometerValue) {
		return (anKilometerValue * 0.621371192237D);
	}

	public static double convertDoubleMilesToKilometers(double aMileValue) {
		return (aMileValue / 0.621371192237D);
	}

	public static float convertFloatKilometersToMiles(float anKilometerValue) {
		return (anKilometerValue * 0.6213712F);
	}

	public static float convertfloatMilesToKilometers(float aMileValue) {
		return (aMileValue / 0.6213712F);
	}

	public static PhysicalStoreGeoCodeType getNorthWestGeoCode(
			PhysicalStoreGeoCodeType aCenterGeoCode, double anRadius) {
		String METHODNAME = "getNorthWestGeoCode";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, "getNorthWestGeoCode", new Object[] {
					aCenterGeoCode, new Double(anRadius) });
		}

		PhysicalStoreGeoCodeType result = null;
		double nLatitude = aCenterGeoCode.getLatitude().doubleValue()
				+ 0.009000900090009001D * anRadius * 1.01D;
		if (nLatitude > 90.0D) {
			nLatitude = -1.0D * (180.0D - nLatitude);
		}

		double oneDegreeLong = get1DegreeLongitude(aCenterGeoCode);
		double nLongitude = aCenterGeoCode.getLongitude().doubleValue()
				- (1.0D / oneDegreeLong * anRadius * 1.01D);
		if (nLongitude < -180.0D) {
			nLongitude = -1.0D * (360.0D + nLongitude);
		}

		result = new PhysicalStoreGeoCodeType();
		result.setLatitude(new BigDecimal(nLatitude));
		result.setLongitude(new BigDecimal(nLongitude));

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, "getNorthWestGeoCode", result);
		}

		return result;
	}

	public static PhysicalStoreGeoCodeType getSouthEastGeoCode(
			PhysicalStoreGeoCodeType aCenterGeoCode, double anRadius) {
		String METHODNAME = "getSouthEastGeoCode";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, "getSouthEastGeoCode", new Object[] {
					aCenterGeoCode, new Double(anRadius) });
		}

		PhysicalStoreGeoCodeType result = null;

		double nLatitude = aCenterGeoCode.getLatitude().doubleValue()
				- (0.009000900090009001D * anRadius * 1.01D);
		if (nLatitude < -90.0D) {
			nLatitude = -1.0D * (180.0D + nLatitude);
		}

		double oneDegreeLong = get1DegreeLongitude(aCenterGeoCode);
		double nLongitude = aCenterGeoCode.getLongitude().doubleValue() + 1.0D
				/ oneDegreeLong * anRadius * 1.01D;

		if (nLongitude > 180.0D) {
			nLongitude = -1.0D * (360.0D - nLongitude);
		}

		result = new PhysicalStoreGeoCodeType();
		result.setLatitude(new BigDecimal(nLatitude));
		result.setLongitude(new BigDecimal(nLongitude));

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, "getSouthEastGeoCode", result);
		}

		return result;
	}

	public static double degreesToRadians(double anDegrees) {
		return (0.0174532925199433D * anDegrees);
	}

	public static double radiansToDegrees(double anRadians) {
		return (57.295779513082323D * anRadians);
	}

	protected static double get1DegreeLongitude(
			PhysicalStoreGeoCodeType aCenterGeoCode) {
		double result = 111.0D * Math.abs(Math
				.cos(degreesToRadians(aCenterGeoCode.getLatitude()
						.doubleValue())));
		return result;
	}

	public static PhysicalStoreGeoCodeType roundGeoCode(PhysicalStoreGeoCodeType geoCode) {
		if (geoCode != null) {
			BigDecimal lan = geoCode.getLatitude();
			BigDecimal lng = geoCode.getLongitude();
			if ((lan != null) && (lan.scale() > 5)) {
				lan = lan.setScale(5, 6);
				geoCode.setLatitude(lan);
			}
			if ((lng != null) && (lng.scale() > 5)) {
				lng = lng.setScale(5, 6);
				geoCode.setLongitude(lng);
			}
		}
		return geoCode;
	}

}