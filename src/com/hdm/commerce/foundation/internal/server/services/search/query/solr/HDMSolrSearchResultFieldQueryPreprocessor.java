package com.hdm.commerce.foundation.internal.server.services.search.query.solr;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.foundation.server.services.rest.search.helper.HDMEntitlementHelper;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author TCS This class will modify the Solr field 'price_*' to
 *         'price_{currencyCode}_{physicalStoreId}' to get the price based on
 *         the physical store.
 *
 */

public class HDMSolrSearchResultFieldQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {

	private static final String CLASS_NAME = HDMSolrSearchResultFieldQueryPreprocessor.class
			.getName();

	private static final Logger LOGGER = LoggingHelper.getLogger(CLASS_NAME);

	private String iComponentId = null;
	private static final String PLUS = "+";
	private static final String MINUS = "-";
	private static final String LISTPRICE_PREFIX = "listprice_";
	private static final String PRICE_WILDCARD = "price_*";
	private static final String LISTPRICE_WILDCARD = "listprice_*";
	private static final String OFFER_PRICE_CURRENCY = "CatalogEntryView/Price/Value[(Currency='*')]";
	private static final String OFFER_PRICE_CURRENCY_AND_PRICEUSAGE = "CatalogEntryView/Price/Value[(Currency='*') AND (../PriceUsage='Display')]";

	public HDMSolrSearchResultFieldQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	/*
	 * This method will remove the old solr field 'price_*' and 'listprice_*'
	 * and replace it with 'price_{currencyCode}_{physicalStoreId}' and
	 * 'listprice_{currencyCode}_{physicalStoreId}' respectively
	 */
	@Override
	public void invoke(final SelectionCriteria selectionCriteria,
			final Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASS_NAME, METHODNAME);
		}
		super.invoke(selectionCriteria, queryRequestObjects);

		final SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(iComponentId);
		final String profile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
		final String indexName = searchRegistry.getIndexName(profile);
		boolean isOverride = false;

		final String[] resultFields = searchRegistry.getResultFields(profile);
		final String currencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
		final Set<String> searchProfileResultFields = new LinkedHashSet();
		final Set<String> searchRequestResponseFields = new LinkedHashSet();

		if (resultFields != null) {
			String[] arrayOfString1;
			final int j = (arrayOfString1 = resultFields).length;
			for (int i = 0; i < j; i++) {
				final String resultField = arrayOfString1[i];
				searchProfileResultFields.add(searchRegistry
						.getIndexColumnNameByParameter(indexName, resultField));
			}
		}

		final String responseFields = getControlParameterValue(HDMConstants._WCF_SEARCH_INTERNAL_RESPONSE_FIELDS);
		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(Level.INFO, CLASS_NAME, METHODNAME,
					"Response fields:: {0} ", responseFields);
		}
		if (responseFields != null) {
			final String[] fields = responseFields.split(HDMConstants.COMMA);
			String[] arrayOfString2;
			final int m = (arrayOfString2 = fields).length;
			for (int k = 0; k < m; k++) {
				final String field = arrayOfString2[k];

				if (field.startsWith(PLUS)) {
					(searchRequestResponseFields).add(searchRegistry
							.getIndexColumnNameByParameter(indexName,
									field.substring(1)));
				} else if (field.startsWith(MINUS)) {
					searchProfileResultFields.remove(searchRegistry
							.getIndexColumnNameByParameter(indexName,
									field.substring(1)));
				} else {
					searchRequestResponseFields.add(searchRegistry
							.getIndexColumnNameByParameter(indexName, field));
					isOverride = true;
				}
			}
		}
		if (!isOverride) {
			(searchRequestResponseFields).addAll(searchProfileResultFields);
		}
		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(Level.INFO, CLASS_NAME, METHODNAME,
					"List of merged result fields:: {0} ",
					Arrays.asList(new Set[] { searchProfileResultFields }));
		}

		if (searchRequestResponseFields != null
				&& !searchRequestResponseFields.isEmpty()) {

			String oldQueryFields = iSolrQuery.getFields();
			if (StringUtils.isNotBlank(oldQueryFields)) {
				iSolrQuery.setFields("");

				oldQueryFields = oldQueryFields.replace(LISTPRICE_PREFIX
						.concat(currencyCode).concat(HDMConstants.COMMA), "");
				oldQueryFields = oldQueryFields.replace(
						HDMConstants.PRICE_FACET_NAME_PREFEIX.concat(
								currencyCode).concat(HDMConstants.COMMA), "");
				iSolrQuery.setFields(oldQueryFields);
			}

			final Iterator<String> itr = searchRequestResponseFields.iterator();
			while (itr.hasNext()) {
				final String searchRequestResponseField = itr.next();
				if (searchRequestResponseField.equals(PRICE_WILDCARD)) {
					String tempPhysicalField = searchRegistry
							.getIndexFieldName(profile, OFFER_PRICE_CURRENCY);
					if (tempPhysicalField != null) {
						tempPhysicalField = tempPhysicalField.replace(
								HDMConstants.WILD_CARD, currencyCode);
					}
					if (LoggingHelper.isTraceEnabled(LOGGER)) {
						LOGGER.logp(Level.INFO, CLASS_NAME, METHODNAME,
								"New field name for offerprice :: {0}",
								tempPhysicalField);
					}
					if (tempPhysicalField != null) {
						final List<String> pricePhysicalFieldList = HDMEntitlementHelper
								.applyFieldNamingPattern(tempPhysicalField,
										selectionCriteria);
						for (final Iterator localIterator = pricePhysicalFieldList
								.iterator(); localIterator.hasNext();) {
							final String pricePhysicalField = (String) localIterator
									.next();
							final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
							if (!HDMConstants.PRO_STORE_ID.equals(storeId)) {
							final String pricePhysicalFieldMin = pricePhysicalField
									.concat(HDMConstants.UNDERSCORE).concat(
											HDMConstants.MIN);
							final String pricePhysicalFieldMax = pricePhysicalField
									.concat(HDMConstants.UNDERSCORE).concat(
											HDMConstants.MAX);
							iSolrQuery.addField(pricePhysicalFieldMin);
							iSolrQuery.addField(pricePhysicalFieldMax);
							}
							iSolrQuery.addField(pricePhysicalField);
							if (LoggingHelper.isTraceEnabled(LOGGER)) {
							LOGGER.logp(Level.INFO, CLASS_NAME, METHODNAME,
									"pricePhysicalField_new:: {0}",
									pricePhysicalField);
													}
						}
					}
				} else if (searchRequestResponseField
						.equals(LISTPRICE_WILDCARD)) {
					// List price changes start here(Logic to append
					// currencycode & physical store id to list price field)
					String tempPhysicalField = searchRegistry
							.getIndexFieldName(profile,
									OFFER_PRICE_CURRENCY_AND_PRICEUSAGE);
					if (tempPhysicalField != null) {
						tempPhysicalField = tempPhysicalField.replace(
								HDMConstants.WILD_CARD, currencyCode);
					}
					if (LoggingHelper.isTraceEnabled(LOGGER)) {
						LOGGER.logp(Level.INFO, CLASS_NAME, METHODNAME,
								"New field name for listprice :: {0}",
								tempPhysicalField);
					}
					if (tempPhysicalField != null) {

						final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
						final String physicalStore_id = HDMUtils
								.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME
										+ "_" + storeId);
						if (StringUtils.isNotBlank(physicalStore_id)) {
							tempPhysicalField = tempPhysicalField.concat(
									HDMConstants.UNDERSCORE).concat(
									physicalStore_id);

						}
						/*if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
							String proUserLevel = HDMUtils
									.getCookieValue(HDMConstants.CUSTOMERLEVEL_COOKIENAME);
							if (proUserLevel != null) {
								tempPhysicalField = tempPhysicalField.concat(HDMConstants.UNDERSCORE)
										.concat(proUserLevel);
							}
						} */
						iSolrQuery.addField(tempPhysicalField);
					}
				} // List price changes end here
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASS_NAME, METHODNAME);
		}
	}

}
