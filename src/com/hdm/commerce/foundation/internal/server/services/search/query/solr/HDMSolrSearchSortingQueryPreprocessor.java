package com.hdm.commerce.foundation.internal.server.services.search.query.solr;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.solr.client.solrj.SolrQuery;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.foundation.server.services.rest.search.helper.HDMEntitlementHelper;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

public class HDMSolrSearchSortingQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrSearchSortingQueryPreprocessor.class
			.getName();

	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);

	private String iComponentId = null;
	private static final String SRT_SP = "<SRT@SP>";
	private static final int TWO = 2;
	private static final int FOUR = 4;
	private static final int FIVE = 5;

	public HDMSolrSearchSortingQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[]";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);

		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(iComponentId);
		String profile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
		String sort = getControlParameterValue(HDMConstants._WCF_SEARCH_SORT);
		String[] sortFields;
		int i;
		String sortFieldValue;
		if ((sort != null) && (sort.length() > 0)) {
			String tempPhysicalField = searchRegistry.getIndexFieldName(
					profile, "CatalogEntryView/Price/Value[(Currency='*')]");
			String pricePhysicalField = null;
			List<String> fieldWithPatternList = null;

			/**
			 * Change to append the physical store id in price field to allow
			 * sorting on basis of price
			 */
			if (tempPhysicalField != null) {
				String currencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
				pricePhysicalField = tempPhysicalField.replace(
						HDMConstants.WILD_CARD, currencyCode);
			}
			sortFields = searchRegistry.getSortFields(profile);
			if ((sortFields != null) && (sortFields.length > 0)) {
				for (i = 0; i < sortFields.length; i++) {
					if (sortFields[i].equals(sort)) {
						sortFieldValue = searchRegistry.getSortFieldValue(
								profile, sortFields[i]);

						if ((sortFieldValue != null)
								&& (sortFieldValue.length() > 0)) {
							String[] sortValues = sortFieldValue
									.split(HDMConstants.COMMA);
							String[] arrayOfString1;
							int j = (arrayOfString1 = sortValues).length;
							for (int k = 0; k < j; k++) {
								String sortValue = arrayOfString1[k];

								String sortFieldName = sortValue.trim();
								SolrQuery.ORDER sortOrder = SolrQuery.ORDER.asc;

								if (sortFieldName
										.endsWith(HDMConstants.SORT_ASC)) {
									sortFieldName = sortFieldName.substring(0,
											sortFieldName.length() - FOUR);
								} else if (sortFieldName
										.endsWith(HDMConstants.SORT_DESC)) {
									sortOrder = SolrQuery.ORDER.desc;
									sortFieldName = sortFieldName.substring(0,
											sortFieldName.length() - FIVE);
								}

								if (sortFieldName.trim().equals(
										tempPhysicalField)) {
									fieldWithPatternList = applyFieldNamingPattern(pricePhysicalField);
									for (String priceSortFieldName : fieldWithPatternList) {
										iSolrQuery.addSort(priceSortFieldName,
												sortOrder);

									}
								} else {
									iSolrQuery
											.addSort(sortFieldName, sortOrder);

								}
							}
							break;
						}
					}
				}
			}
		}

		String internalSort = getControlParameterValue(HDMConstants._WCF_SEARCH_INTERNAL_SORT);
		if ((internalSort != null) && (internalSort.length() > 0)) {
			String[] sortClauses = internalSort.split(HDMConstants.COMMA);
			String[] sortFldValue;
			i = (sortFldValue = sortClauses).length;
			for (int k = 0; k < i; k++) {
				String strSortFieldAndDirectionPair = sortFldValue[k];
				String[] arrstrFieldAndDirection = strSortFieldAndDirectionPair
						.trim().split("\\s+");
				if (!updateQueryWithSortParameter(iSolrQuery,
						arrstrFieldAndDirection)) {
					LOGGER.logp(
							Level.INFO,
							CLASSNAME,
							METHODNAME,
							"Sort field and direction pair '{0}'  is in the wrong format; it will be ignored",
							strSortFieldAndDirectionPair);
				}
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	protected List<String> applyFieldNamingPattern(String origFieldName) {
		return HDMEntitlementHelper.applyFieldNamingPattern(origFieldName,
				iSelectionCriteria);
	}

	private boolean updateQueryWithSortParameter(SolrQuery aSolrQ,
			String[] aarrstrFieldAndDirection) {
		boolean bUpdateSuccessful = false;
		final String METHODNAME = "updateQueryWithSortParameter";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, aarrstrFieldAndDirection);
		}

		if ((aSolrQ != null) && (aarrstrFieldAndDirection != null)
				&& (aarrstrFieldAndDirection.length == TWO)) {
			String strSortIndexField = aarrstrFieldAndDirection[0];

			strSortIndexField = strSortIndexField.replaceAll(SRT_SP,
					HDMConstants.COMMA);

			strSortIndexField = strSortIndexField.replace(SRT_SP,
					HDMConstants.COMMA);
			String strSortDirection = aarrstrFieldAndDirection[1];
			SolrQuery.ORDER sortOrder = null;

			if (strSortDirection.equalsIgnoreCase("asc")) {
				sortOrder = SolrQuery.ORDER.asc;
			} else if (strSortDirection.equalsIgnoreCase("desc")) {
				sortOrder = SolrQuery.ORDER.desc;
			}
			if (sortOrder != null) {
				aSolrQ.addSort(strSortIndexField, sortOrder);
				bUpdateSuccessful = true;
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"Sort by {0} ", strSortIndexField);
				}
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME,
					Boolean.valueOf(bUpdateSuccessful));
		}
		return bUpdateSuccessful;
	}

}
