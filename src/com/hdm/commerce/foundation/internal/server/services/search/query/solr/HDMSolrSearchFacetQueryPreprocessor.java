package com.hdm.commerce.foundation.internal.server.services.search.query.solr;

import java.util.List;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

public class HDMSolrSearchFacetQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrSearchFacetQueryPreprocessor.class
			.getName();

	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);

	private static final String FACET_CONF_DEFAULT_FOR_SORT = "facetConfigurationDefaultForSort";
	private static final String FACET_CONF_DEFAULT_FOR_MIN_COUNT = "facetConfigurationDefaultForMinimumCount";
	private static final String FS_SP = "<FS@SP>";

	private String iComponentId = null;

	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[]";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);

		String iFacetConfigurationDefaultForSort = null;
		Integer iFacetConfigurationDefaultForMinimumCount = null;
		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(iComponentId);
		String facetConfigurationDefaultForSort = searchRegistry
				.getExtendedConfigurationPropertyValue(FACET_CONF_DEFAULT_FOR_SORT);
		if ((facetConfigurationDefaultForSort != null)
				&& (facetConfigurationDefaultForSort.length() > 0)) {
			iFacetConfigurationDefaultForSort = facetConfigurationDefaultForSort;
		} else {
			iFacetConfigurationDefaultForSort = "count";
		}
		String facetConfigurationDefaultForMinimumCount = searchRegistry
				.getExtendedConfigurationPropertyValue(FACET_CONF_DEFAULT_FOR_MIN_COUNT);
		if ((facetConfigurationDefaultForMinimumCount != null)
				&& (facetConfigurationDefaultForMinimumCount.length() > 0)) {
			iFacetConfigurationDefaultForMinimumCount = Integer
					.valueOf(facetConfigurationDefaultForMinimumCount);
		} else {
			iFacetConfigurationDefaultForMinimumCount = Integer.valueOf(0);
		}
		List<String> facetProperties = getControlParameterValues(HDMConstants._WCF_SEARCH_FACET_PROPERTIES);
		String[] properties;
		if (facetProperties != null && !facetProperties.isEmpty()) {
			for (int i = 0; i < facetProperties.size(); i++) {
				String encodedProperties = facetProperties.get(i);
				properties = encodedProperties.split(FS_SP);
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}