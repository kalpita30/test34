package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.FacetHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author TCS
 *
 *         this pre processor class is having logic change in solr query to not
 *         to refresh facet values count of promotion icon facet
 */
public class HDMSolrRESTPromoIconFacetQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrRESTPromoIconFacetQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;

	/**
	 * @param componentId
	 */
	public HDMSolrRESTPromoIconFacetQueryPreprocessor(String componentId) {
		this.iComponentId = componentId;
	}

	/**
	 * main method having business logic
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryRequestObjects });
		}
		super.invoke(selectionCriteria, queryRequestObjects);

		try {
			processPromotionIconsFacet();
		} catch (Exception e) {
			LOGGER.logp(Level.FINE, CLASSNAME, METHODNAME,
					"selectionCriteria: {0}",
					new Object[] { selectionCriteria });
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * change in solr query to include tagging and exclusion query for promotion
	 * icon to not to refresh facet counts of facet values
	 * 
	 * @throws Exception
	 */
	private void processPromotionIconsFacet() throws Exception {
		final String METHODNAME = "processPromotionIconsFacet()";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		final String strSearchProfile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
		final String strCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
		final String languageId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_LANGUAGE);
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		final String indexName = "CatalogEntry";
		final String strCategory = getControlParameterValue(HDMConstants._WCF_SEARCH_CATEGORY);
		final String strTerm = getControlParameterValue(HDMConstants._WCF_SEARCH_TERM);
		final String strCatalogId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CATALOG);

		Map<String, Map<String, String>> facetProperties = new HashMap();
		Set<Entry<String, Map<String, String>>> facetPropertyValues = null;
		List<String> promoIconAttributes = null;
		List<String> storeAvailabilityAttributes = null;
		List<String> appliedPromoIcons = new ArrayList();
		List<String> appliedPromoIconsQuery = new ArrayList();
		final String physicalStore = HDMUtils
				.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
						+ storeId);

		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(
					Level.INFO,
					CLASSNAME,
					METHODNAME,
					"strSearchProfile{0},strCurrencyCode:{1},storeId:{2},indexName:{3),strCategory:{4},strTerm:{5},strCatalogId:{6},physicalStore:{3}",
					new Object[] { strSearchProfile, strCurrencyCode, storeId,
							indexName, strCategory, strTerm, strCatalogId,
							physicalStore });
		}

		if ((strCategory != null) && (strCategory.length() > 0)
				&& ((strTerm == null) || (strTerm.equals("")))) {
			facetProperties.putAll(FacetHelper
					.getFacetConfigurationForCategory(strCategory, languageId,
							indexName, strSearchProfile, Boolean.valueOf(true),
							Integer.valueOf(storeId), strCurrencyCode,
							strCatalogId));
		} else {
			facetProperties.putAll(FacetHelper
					.getFacetConfigurationForKeywordSearch(indexName,
							languageId, Integer.valueOf(storeId),
							strSearchProfile, strCurrencyCode, strCategory));
		}

		// group only promotion icon facet(attridentifier = PROMOICON_*)
		if (facetProperties != null && facetProperties.size() > 0) {
			facetPropertyValues = facetProperties.entrySet();
			Iterator<Entry<String, Map<String, String>>> iterator = facetPropertyValues
					.iterator();
			promoIconAttributes = new ArrayList();
			storeAvailabilityAttributes = new ArrayList();
			while (iterator.hasNext()) {
				Entry<String, Map<String, String>> entry = iterator.next();
				Map<String, String> valueMap = entry.getValue();
				if (valueMap.get(HDMConstants.ATTR_IDENTIFIER) != null
						&& valueMap.get(HDMConstants.ATTR_IDENTIFIER)
								.startsWith(HDMConstants.PROMOICON)) {
					promoIconAttributes.add(entry.getKey());
				} else if (valueMap.get(HDMConstants.ATTR_IDENTIFIER) != null
						&& valueMap
								.get(HDMConstants.ATTR_IDENTIFIER)
								.startsWith(HDMConstants.ATTR_FULLFILLMENT_TYPE)) {
					storeAvailabilityAttributes.add(entry.getKey());
				}
			}
		}

		if (promoIconAttributes != null && !promoIconAttributes.isEmpty()) {
			boolean promoIconApplied = false;
			// delete original exclude fields from solr query
			for (final String field : this.iSolrQuery.getFacetFields()) {
				if (field.startsWith(HDMConstants.EX)) {
					final String attr = field.split(HDMConstants.CLOSEBRACES)[1];
					if (promoIconAttributes.contains(attr)) {
						promoIconApplied = true;
						this.iSolrQuery.removeFacetField(field);
					}
				}
			}

			// delete original tagging fields from solr query
			for (final String fq : this.iSolrQuery.getFilterQueries()) {
				if (fq.startsWith(HDMConstants.TAG)) {
					final String attr = fq.split(HDMConstants.EQUAL)[1];
					final String attr1 = attr.split(HDMConstants.CLOSEBRACES)[0];
					if (promoIconAttributes.contains(attr1)) {
						appliedPromoIconsQuery.add(attr1 + ":(\""
								+ physicalStore + "\")");
						appliedPromoIcons.add(attr1);
						this.iSolrQuery.removeFilterQuery(fq);
					}
				}
			}

			if (promoIconApplied) {
				// change exclude fields
				String commaSeperatedAttr = StringUtils.join(
						promoIconAttributes, HDMConstants.COMMA);
				commaSeperatedAttr = HDMConstants.EX + commaSeperatedAttr
						+ HDMConstants.CLOSEBRACES;
				for (String attr : promoIconAttributes) {
					this.iSolrQuery.addFacetField(commaSeperatedAttr + attr);
				}

				// change tagging fields
				String oredIconTagValues = StringUtils.join(
						appliedPromoIconsQuery, HDMConstants.OR);
				for (String icon : appliedPromoIcons) {
					this.iSolrQuery.addFilterQuery(HDMConstants.TAG + icon
							+ HDMConstants.CLOSEBRACES + oredIconTagValues);
				}
			}
		}
		if (storeAvailabilityAttributes != null
				&& !storeAvailabilityAttributes.isEmpty()) {
			boolean storeAvailibityFacet = false;
			String updatedFilterQuery = null;
			for (final String fq : this.iSolrQuery.getFilterQueries()) {
				if (fq.startsWith(HDMConstants.TAG)) {
					final String attr = fq.split(HDMConstants.EQUAL)[1];
					final String attr1 = attr.split(HDMConstants.CLOSEBRACES)[0];
					if (fq.contains(attr1)) {
						updatedFilterQuery = fq.replace(HDMConstants.COMMA
								.concat(attr1).concat(HDMConstants.COLON), "");
						storeAvailibityFacet = true;
					}
					this.iSolrQuery.removeFilterQuery(fq);
					this.iSolrQuery.addFilterQuery(updatedFilterQuery);
				}
			}
			if (storeAvailibityFacet) {
				this.iSolrQuery.addFilterQuery(updatedFilterQuery);
			}
		}

		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(
					Level.INFO,
					CLASSNAME,
					METHODNAME,
					"facetProperties{0},facetPropertyValues:{1},promoIconAttributes:{2},storeAvailabilityAttributes:{3),appliedPromoIcons:{4},appliedPromoIconsQuery:{5}",
					new Object[] { Arrays.asList(facetProperties),
							Arrays.asList(new Set[] { facetPropertyValues }),
							Arrays.asList(promoIconAttributes),
							Arrays.asList(storeAvailabilityAttributes),
							Arrays.asList(appliedPromoIcons),
							Arrays.asList(appliedPromoIconsQuery) });
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
