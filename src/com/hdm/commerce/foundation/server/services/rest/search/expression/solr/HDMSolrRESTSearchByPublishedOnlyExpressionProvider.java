package com.hdm.commerce.foundation.server.services.rest.search.expression.solr;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.FacetHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.expression.SearchExpressionProvider;
import com.ibm.commerce.foundation.server.services.search.expression.solr.AbstractSolrSearchExpressionProvider;

/**
 * @author usrecomm This expression provider is called in product listing and
 *         search results pages. This will check if a product is buyable in the
 *         current market where the user is present. If not the product will not
 *         be displayed in product listing and search results pages.
 */
public class HDMSolrRESTSearchByPublishedOnlyExpressionProvider extends
		AbstractSolrSearchExpressionProvider implements
		SearchExpressionProvider {
	private static final String CLASSNAME = HDMSolrRESTSearchByPublishedOnlyExpressionProvider.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;

	public HDMSolrRESTSearchByPublishedOnlyExpressionProvider(
			final String componentId) {
		iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria)
			throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria selectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		try {
			super.invoke(selectionCriteria);
			final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
			final String searchAttrIdentifier = getAttributeName();
			final String marketId = HDMUtils
					.getCookieValue(HDMConstants.MARKETID_COOKIENAME + "_"
							+ storeId);

			if (StringUtils.isNotBlank(searchAttrIdentifier)) {
				final StringBuilder sb = new StringBuilder(
						HDMConstants.TEXT_NOT);
				sb.append(HDMConstants.OPEN_PARANTHESIS);
				sb.append(searchAttrIdentifier);
				sb.append(HDMConstants.COLON);
				sb.append(marketId);
				sb.append(HDMConstants.CLOSE_PARANTHESIS);
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"market exclude filter query:: {0} ", sb.toString());
				}

				// fq=NOT(ads_f14001_ntk_cs:10)
				addControlParameterValue(
						HDMConstants._WCF_SEARCH_INTERNAL_FILTERQUERY,
						sb.toString());
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * @return searchAttrIdentifier
	 * @throws NumberFormatException
	 * @throws Exception
	 * 
	 *             This method is used to get the attribute identifier for
	 *             MARKET_BUYABLE_EXCLUDED attribute.
	 * 
	 */
	private String getAttributeName() throws NumberFormatException, Exception {
		final String METHODNAME = "getAttributeName";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		String searchAttrIdentifier = null;
		final String strSearchProfile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
		final String strCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
		final String languageId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_LANGUAGE);
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		final String strCategory = getControlParameterValue(HDMConstants._WCF_SEARCH_CATEGORY);
		final String strTerm = getControlParameterValue(HDMConstants._WCF_SEARCH_TERM);
		final String strCatalogId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CATALOG);

		final Map<String, Map<String, String>> facetProperties = new HashMap();
		Set<Entry<String, Map<String, String>>> facetPropertyValues = null;

		if (StringUtils.isNotBlank(strCategory) && StringUtils.isBlank(strTerm)) {
			facetProperties.putAll(FacetHelper
					.getFacetConfigurationForCategory(strCategory, languageId,
							HDMConstants.CATALOGENTRY, strSearchProfile,
							Boolean.valueOf(true), Integer.valueOf(storeId),
							strCurrencyCode, strCatalogId));
		} else {
			facetProperties.putAll(FacetHelper
					.getFacetConfigurationForKeywordSearch(
							HDMConstants.CATALOGENTRY, languageId,
							Integer.valueOf(storeId), strSearchProfile,
							strCurrencyCode, strCategory));
		}

		if (facetProperties != null && facetProperties.size() > 0) {
			facetPropertyValues = facetProperties.entrySet();
			final Iterator<Entry<String, Map<String, String>>> iterator = facetPropertyValues
					.iterator();
			while (iterator.hasNext()) {
				final Entry<String, Map<String, String>> entry = iterator
						.next();
				final Map<String, String> valueMap = entry.getValue();
				if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
					if (valueMap.get(HDMConstants.ATTR_IDENTIFIER) != null
							&& valueMap
									.get(HDMConstants.ATTR_IDENTIFIER)
									.equals(HDMConstants.PRO_MARKET_BUYABLE_EXCLUDE)) {
						searchAttrIdentifier = valueMap
								.get(HDMConstants.PROPERTY_VALUE);
						break;
					}
				} else {
					if (valueMap.get(HDMConstants.ATTR_IDENTIFIER) != null
							&& valueMap
									.get(HDMConstants.ATTR_IDENTIFIER)
									.equals(HDMConstants.MARKET_BUYABLE_EXCLUDE)) {
						searchAttrIdentifier = valueMap
								.get(HDMConstants.PROPERTY_VALUE);
						break;
					}
				}
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, searchAttrIdentifier);
		}
		return searchAttrIdentifier;
	}

}
