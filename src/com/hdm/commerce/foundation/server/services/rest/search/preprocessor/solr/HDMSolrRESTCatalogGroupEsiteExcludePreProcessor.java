/**
 * 
 */
package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.context.RemoteContextServiceFactory;
import com.ibm.commerce.foundation.internal.server.services.registry.BaseStoreProperty;
import com.ibm.commerce.foundation.internal.server.services.registry.StoreObject;
import com.ibm.commerce.foundation.internal.server.services.registry.StoreRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author usrecomm
 *
 */
public class HDMSolrRESTCatalogGroupEsiteExcludePreProcessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {

	private static final String CLASSNAME = HDMSolrRESTCatalogGroupEsiteExcludePreProcessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;

	/**
	 * @param componentId
	 */
	public HDMSolrRESTCatalogGroupEsiteExcludePreProcessor(String componentId) {
		iComponentId = componentId;
	}

	/**
	 * main method having business logic
	 */
	public void invoke(final SelectionCriteria selectionCriteria,
			final Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);
		final String storeId = getControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		String masterCatalogId = null;
		if (storeId != null) {
			final StoreObject store = StoreRegistry.getInstance().get(
					Integer.valueOf(Integer.parseInt(storeId)));
			if (store != null) {
				final BaseStoreProperty baseStoreProperty = (BaseStoreProperty) store
						.getStoreProperty("masterCatalogId");
				masterCatalogId = baseStoreProperty.getMasterCatalogId()
						.toString();
			} else {
				masterCatalogId = RemoteContextServiceFactory
						.getContextAsString("com.ibm.commerce.context.catalog.catalog");
			}
		} else {
			masterCatalogId = RemoteContextServiceFactory
					.getContextAsString("com.ibm.commerce.context.catalog.catalog");
		}
		String esiteType = "DIY";
		if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
			esiteType = "PRO";
		}
		final String aStringArray = (HDMConstants.OPENBRACES)
				.concat(HDMConstants.ESITEEXCLUDE)
				.concat(masterCatalogId)
				.concat(HDMConstants.UNDERSCORE)
				.concat(HDMConstants.CATALOGENTRY)
				.concat(HDMConstants.CLOSEBRACES)
				.concat(HDMConstants.HYPHEN
						.concat(HDMConstants.OPEN_PARANTHESIS)
						.concat(HDMConstants.EXCLUDSITE)
						.concat(HDMConstants.COLON).concat(esiteType)
						.concat(HDMConstants.CLOSE_PARANTHESIS));

		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME, "aStringArray :{0}",
					new Object[] { aStringArray });
		}
		iSolrQuery.addFilterQuery(aStringArray);
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}
