package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.solr.common.SolrDocumentList;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;

public class HDMSolrRESTSearchKeywordSuggestionQueryPostprocessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {

	private static final String CLASSNAME = HDMSolrRESTSearchKeywordSuggestionQueryPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private static final Integer DEFAULT_LIMIT = Integer.valueOf(4);
	private static final Integer DEFAULT_MAX_SHINGLE_SIZE = Integer.valueOf(3);
	private static final Boolean DEFAULT_SORR_BY_FREQUENCY = Boolean
			.valueOf(true);
	private static Pattern pattern = null;
	private Integer maxShingleSize = null;
	private Boolean sortByFrequency = null;
	String iComponentId = null;

	public HDMSolrRESTSearchKeywordSuggestionQueryPostprocessor(
			final String componentId) {
		iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria,
			Object[] queryResponseObjects) throws RuntimeException {

		String methodName = "invoke(SelectionCriteria, Object[])";

		Matcher stopWordMatcher = null;
		Matcher removeOther = null;

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, methodName, new Object[] {
					selectionCriteria, queryResponseObjects });
		}

		super.invoke(selectionCriteria, queryResponseObjects);
		List lSuggestionList = new ArrayList();

		String term = getControlParameterValue(HDMConstants._WCF_SEARCH_TERM);

		if ((term != null) && (term.trim().length() > 0)) {
			SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
					.getInstance();
			int limit = ((getControlParameterValue(HDMConstants._WCF_SEARCH_SUGGESTION_LIMIT) != null) ? Integer
					.valueOf(getControlParameterValue(HDMConstants._WCF_SEARCH_SUGGESTION_LIMIT))
					: DEFAULT_LIMIT).intValue();

			if (pattern == null) {
				String stopWords = (searchRegistry
						.getExtendedConfigurationPropertyValue(HDMConstants.SKIPWORDS) != null) ? String
						.valueOf(searchRegistry
								.getExtendedConfigurationPropertyValue(HDMConstants.SKIPWORDS))
						: "\\b(?i:a|an|and|are|as|at|be|but|by|for|if|in|into|is|it|no|not|of|on|or|such|that|the|their|then|there|these|they|this|to|was|will|with|-)\\b\\s*|\\.|-";
				if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
					LOGGER.entering(CLASSNAME, methodName,
							new Object[] { "SkipWords is: " + stopWords });
				}
				stopWordMatcher = Pattern.compile(stopWords, 34).matcher("");
				removeOther = Pattern
						.compile(
								"[^a-zA-Z\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\\s-]")
						.matcher("");

			}

			if (maxShingleSize == null) {
				maxShingleSize = (searchRegistry
						.getExtendedConfigurationPropertyValue(HDMConstants.SEARCHBASEDKEYWORDSUGGESTIONSMAXSHINGLESIZE) != null) ? Integer
						.valueOf(searchRegistry
								.getExtendedConfigurationPropertyValue(HDMConstants.SEARCHBASEDKEYWORDSUGGESTIONSMAXSHINGLESIZE))
						: DEFAULT_MAX_SHINGLE_SIZE;
			}

			if (sortByFrequency == null) {
				sortByFrequency = (searchRegistry
						.getExtendedConfigurationPropertyValue(HDMConstants.SEARCHBASEDKEYWORDSUGGESTIONSSORTBYFREQUENCYOPTION) != null) ? Boolean
						.valueOf(searchRegistry
								.getExtendedConfigurationPropertyValue(HDMConstants.SEARCHBASEDKEYWORDSUGGESTIONSSORTBYFREQUENCYOPTION))
						: DEFAULT_SORR_BY_FREQUENCY;
			}

			String[] searchTermTokens = term.split(" ");

			Object documentsListObj = this.iSearchResponseObject.getResponse()
					.remove(HDMConstants.DOCUMENTLIST);

			if (documentsListObj instanceof SolrDocumentList) {
				SolrDocumentList documentsList = (SolrDocumentList) documentsListObj;
				if ((documentsList != null) && (!(documentsList.isEmpty()))) {
					int j = 0;

					Map suggestionsFrequency = new HashMap();
					for (Map solrDocument : documentsList) {
						Object termSuggestionObj = solrDocument
								.get(HDMConstants.TERM_SUGGEST);
						if (termSuggestionObj != null) {
							List<String> termSuggestionList = new ArrayList();
							if (termSuggestionObj instanceof List)
								termSuggestionList = (List) termSuggestionObj;
							else {
								termSuggestionList.add(String
										.valueOf(termSuggestionObj));
							}

							for (String termSuggestion : termSuggestionList) {
								termSuggestion = stopWordMatcher.reset(
										removeOther.reset(
												termSuggestion.trim()
														.toLowerCase())
												.replaceAll(""))
										.replaceAll(" ");
								int index = termSuggestion.indexOf(term
										.toLowerCase());
								if (index > -1) {
									termSuggestion = termSuggestion.trim();
									String matchedSubStringOfSearchTerm = termSuggestion
											.substring(index);
									StringBuilder suggestion = new StringBuilder();
									String[] suggestionTerms = matchedSubStringOfSearchTerm
											.split(" ");

									int i = 0;
									for (String suggestionTerm : suggestionTerms) {
										suggestionTerm = suggestionTerm.trim();
										suggestion.append(suggestionTerm);
										if (++i >= searchTermTokens.length) {
											String finalSuggetion = String
													.valueOf(suggestion).trim();
											Integer frequencey = Integer
													.valueOf((suggestionsFrequency
															.containsKey(finalSuggetion)) ? ((Integer) suggestionsFrequency
															.get(finalSuggetion))
															.intValue() + 1
															: 1);
											suggestionsFrequency.put(
													finalSuggetion, frequencey);

											j = suggestionsFrequency.size();
											if (i == maxShingleSize.intValue()) {
												break;
											}
										}
										suggestion.append(" ");
										if ((!(sortByFrequency.booleanValue()))
												&& (j == limit)) {
											break;
										}
									}
								}
								if ((!(sortByFrequency.booleanValue()))
										&& (j == limit)) {
									break;
								}
							}
						}
						if ((!(sortByFrequency.booleanValue())) && (j == limit))
							break;
					}
					Map.Entry entry;
					if ((sortByFrequency.booleanValue())
							&& (!(suggestionsFrequency.isEmpty()))) {
						int i = 0;

						PriorityQueue pq = new PriorityQueue(
								suggestionsFrequency.size(),
								new Comparator<Map.Entry<String, Integer>>() {
									public int compare(
											Map.Entry<String, Integer> arg0,
											Map.Entry<String, Integer> arg1) {
										return (arg1.getValue()).compareTo(arg0
												.getValue());
									}
								});
						pq.addAll(suggestionsFrequency.entrySet());
						while (!(pq.isEmpty())) {
							entry = (Map.Entry) pq.poll();
							Map map = new HashMap(1);
							map.put("term", entry.getKey());
							map.put("frequency",
									String.valueOf(entry.getValue()));
							lSuggestionList.add(map);
							if (++i == limit)
								break;
						}
					}
				}

			}

		}

		Map keywordSuggestions = new HashMap();
		keywordSuggestions.put("identifier", "Keyword");
		keywordSuggestions.put("entry", lSuggestionList);

		List container = new LinkedList();
		container.add(keywordSuggestions);

		Object suggestionView = this.iSearchResponseObject.getResponse().get(
				HDMConstants.SUGGESTIONVIEW);
		if (suggestionView != null)
			((List) suggestionView).add(container);
		else {
			this.iSearchResponseObject.getResponse().put(
					HDMConstants.SUGGESTIONVIEW, container);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER))
			LOGGER.exiting(CLASSNAME, methodName);

	}
}
