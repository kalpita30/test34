package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.internal.server.services.search.util.SpecialCharacterHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;

public class HDMSolrRESTFulFilmentFacetQueryPostProcessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {

	private static final String CLASSNAME = HDMSolrRESTFacetQueryPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTFacetQueryPostprocessor.class);
	private String iComponentId = null;
	private SolrSearchConfigurationRegistry iSearchRegistry = null;

	public HDMSolrRESTFulFilmentFacetQueryPostProcessor(String componentId) {
		this.iComponentId = componentId;
		this.iSearchRegistry = SolrSearchConfigurationRegistry
				.getInstance(this.iComponentId);
	}

	/**
	 * main method having custom logic to group FulfillmentType Facet
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryResponseObjects) throws RuntimeException {

		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}

		super.invoke(selectionCriteria, queryResponseObjects);

		List<Map<String, Object>> facetViews = (LinkedList<Map<String, Object>>) iSearchResponseObject
				.getResponse().get(HDMConstants.FACET_VIEW);

		List<Map<String, Object>> originalFacetViews = facetViews;
		if (facetViews != null) {
			List<Map<String, Object>> storeAvaialbilityFacet = new LinkedList<>();
			for (Iterator<Map<String, Object>> iter = facetViews.iterator(); iter
					.hasNext();) {
				Map<String, Object> facetView = iter.next();
				if (HDMConstants.TYPE_OF_PRODUCTS_FACET.equals(facetView
						.get(HDMConstants.NAME))) {
					facetView.put(HDMConstants.NAME,
							HDMConstants.TYPE_OF_PRODUCTS_HDR);
					Map<String, Object> extendedData = (Map<String, Object>) facetView
							.get(HDMConstants.EXTENDED_DATA);
					List<Map<String, Object>> facetEntries1 = (LinkedList<Map<String, Object>>) facetView
							.get(HDMConstants.ENTRY);
					List<Map<String, Object>> facetEntries2 = facetEntries1;
					for (Iterator<Map<String, Object>> iter1 = facetEntries2
							.iterator(); iter1.hasNext();) {
						Map<String, Object> facetEntryNew = iter1.next();
						String label = (String) facetEntryNew
								.get(HDMConstants.LABEL);
						if (StringUtils.isNotBlank(label)) {
							if (label
									.indexOf(HDMConstants.TYPE_OF_PRODUCTS_INSTORE) > 0) {
								facetEntryNew.put(HDMConstants.LABEL,
										HDMConstants.ONLINE_STORE_FACET_LABEL);
								// Added code to modify uniqueId to handle
								// different store - Begin
								String uniqueId = SpecialCharacterHelper
										.convertStrToAscii(HDMConstants.ONLINE_STORE_FACET_LABEL);
								Map<String, Object> extData = (Map<String, Object>) facetEntryNew
										.get(HDMConstants.EXTENDED_DATA);
								if (null != extData && null != uniqueId) {
									if (null != extData
											.get(HDMConstants.UNIQUEID)) {
										extData.put(HDMConstants.UNIQUEID,
												uniqueId);
									}
								}

								// Added code to modify uniqueId - End
							} else if (label
									.indexOf(HDMConstants.TYPE_OF_PRODUCTS_ONLINE) > 0) {
								facetEntryNew.put(HDMConstants.LABEL,
										HDMConstants.ONLINE_LABEL);
								// Added code to modify uniqueId to handle
								// different store - Begin
								String uniqueId = SpecialCharacterHelper
										.convertStrToAscii(HDMConstants.ONLINE_LABEL);
								Map<String, Object> extData = (Map<String, Object>) facetEntryNew
										.get(HDMConstants.EXTENDED_DATA);
								if (null != extData && null != uniqueId) {
									if (null != extData
											.get(HDMConstants.UNIQUEID)) {
										extData.put(HDMConstants.UNIQUEID,
												uniqueId);
									}
								}
								// Added code to modify uniqueId - End
							}
						}
					}
				}
			}
		}
		List<Map<String, Object>> facetViewss = (LinkedList<Map<String, Object>>) iSearchResponseObject
				.getResponse().get(HDMConstants.FACET_VIEW);

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME,
					new Object[] { Arrays.asList(facetViewss) });
		}
	}
}
