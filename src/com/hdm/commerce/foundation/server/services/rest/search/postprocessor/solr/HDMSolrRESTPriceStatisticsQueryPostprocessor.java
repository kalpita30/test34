package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.solr.client.solrj.response.FieldStatsInfo;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.foundation.server.services.rest.search.helper.HDMEntitlementHelper;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;

/**
 * @author TCS
 * 
 *         this post processor class is having logic to fetch statistics info
 *         from solr response and place it in rest resonse
 */
public class HDMSolrRESTPriceStatisticsQueryPostprocessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {
	private static final String CLASSNAME = HDMSolrRESTPriceStatisticsQueryPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;
	private SolrSearchConfigurationRegistry iSearchRegistry = null;
	private static final String PRICE_RANGE_STATISTICS_MIN = "statMinPrice";
	private static final String PRICE_RANGE_STATISTICS_MAX = "statMaxPrice";

	/**
	 * @param componentId
	 */
	public HDMSolrRESTPriceStatisticsQueryPostprocessor(String componentId) {
		this.iComponentId = componentId;
		this.iSearchRegistry = SolrSearchConfigurationRegistry
				.getInstance(this.iComponentId);
	}

	/**
	 * main method having business logic
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryResponseObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}
		super.invoke(selectionCriteria, queryResponseObjects);

		final String iCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
		final String priceFacetField = HDMConstants.PRICE_FACET_NAME_PREFEIX
				.concat(iCurrencyCode);
		List<Map<String, Object>> facetViewss = (LinkedList<Map<String, Object>>) iSearchResponseObject
				.getResponse().get(HDMConstants.FACET_VIEW);
		Iterator facetStatsIter = this.iQueryResponse.getFieldStatsInfo()
				.entrySet().iterator();

		final List<String> pricePhysicalFieldList = HDMEntitlementHelper
				.applyFieldNamingPattern(priceFacetField, selectionCriteria);
		for (Iterator localIterator = pricePhysicalFieldList.iterator(); localIterator
				.hasNext();) {
			final String pricePhysicalField = (String) localIterator.next();

			while (facetStatsIter.hasNext()) {
				Map.Entry<String, FieldStatsInfo> facetStats = (Map.Entry) facetStatsIter
						.next();
				if (facetStats.getKey().equals(pricePhysicalField)) {
					FieldStatsInfo statsInfo = facetStats.getValue();
					BigDecimal minPrice = new BigDecimal(
							(Double) statsInfo.getMin());
					BigDecimal maxPrice = new BigDecimal(
							(Double) statsInfo.getMax());
					minPrice = minPrice.setScale(5, BigDecimal.ROUND_HALF_UP);
					maxPrice = maxPrice.setScale(5, BigDecimal.ROUND_HALF_UP);

					this.iSearchResponseObject.getResponse().put(
							PRICE_RANGE_STATISTICS_MIN, minPrice);
					this.iSearchResponseObject.getResponse().put(
							PRICE_RANGE_STATISTICS_MAX, maxPrice);
				}
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME,
					new Object[] { Arrays.asList(facetViewss) });
		}
	}
}
