/**
 * 
 */
package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author usrecomm
 *
 */
public class HDMSolrRESTChildCatentryFilterQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrRESTChildCatentryFilterQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTChildCatentryFilterQueryPreprocessor.class);
	private String iComponentId = null;

	/**
	 * @param componentId
	 */
	public HDMSolrRESTChildCatentryFilterQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	/**
	 * this query is to filter products only with child entries to be displayed
	 * in portal
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);

		// (((catenttype_id_ntk_cs:ProductBean) AND (childCatentry_id:*)) ||
		// (-(catenttype_id_ntk_cs:ProductBean) AND (components:*)))

		/*String aStringArray = HDMConstants.OPEN_PARANTHESIS
				.concat(HDMConstants.OPEN_PARANTHESIS)
				.concat(HDMConstants.OPEN_PARANTHESIS)
				.concat(HDMConstants.PRODUCT_TYPE).concat(HDMConstants.COLON)
				.concat(HDMConstants.PRODUCT_BEAN)
				.concat(HDMConstants.CLOSE_PARANTHESIS)
				.concat(HDMConstants.AND).concat(HDMConstants.OPEN_PARANTHESIS)
				.concat(HDMConstants.CHILDCATENTRYID)
				.concat(HDMConstants.COLON).concat(HDMConstants.WILD_CARD)
				.concat(HDMConstants.CLOSE_PARANTHESIS)
				.concat(HDMConstants.CLOSE_PARANTHESIS).concat(HDMConstants.OR)
				.concat(HDMConstants.OPEN_PARANTHESIS)
				.concat(HDMConstants.TEXT_NOT)
				.concat(HDMConstants.OPEN_PARANTHESIS)
				.concat(HDMConstants.PRODUCT_TYPE).concat(HDMConstants.COLON)
				.concat(HDMConstants.PRODUCT_BEAN)
				.concat(HDMConstants.CLOSE_PARANTHESIS)
				.concat(HDMConstants.AND).concat(HDMConstants.OPEN_PARANTHESIS)
				.concat(HDMConstants.COMPONENTS).concat(HDMConstants.COLON)
				.concat(HDMConstants.WILD_CARD)
				.concat(HDMConstants.CLOSE_PARANTHESIS)
				.concat(HDMConstants.CLOSE_PARANTHESIS)
				.concat(HDMConstants.CLOSE_PARANTHESIS);*/
				
		//Modified for Super Sku Requirement to display PLP by ItemId
		String aStringArray = "("
				.concat("catenttype_id_ntk_cs").concat(":")
				.concat("ItemBean")
				.concat(")");

		if (LoggingHelper.isTraceEnabled(LOGGER)) {

			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME, "aStringArray :{0}",
					new Object[] { aStringArray });

		}
		// iSolrQuery.addFilterQuery(aStringArray);
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
