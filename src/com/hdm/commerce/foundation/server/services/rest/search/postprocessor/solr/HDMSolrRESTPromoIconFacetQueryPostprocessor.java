package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.internal.server.services.search.util.SpecialCharacterHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;

/**
 * @author TCS
 * 
 *         facet icon MSI - facet1(PROMOICON_MSI) facet icon CE -
 *         facet2(PROMOICON_CE) facet icon NLP - facet3(PROMOICON_NLP)
 * 
 *         grouped icon/main icon - facet(PROMOICON)
 * 
 *         PROMOICON facet will be shown in front end with facet values as MSI
 *         CE NLP
 * 
 *         this post processor class is having required changes/customizations
 *         in the logic to group individual promotion icon
 *         attributes(PROMOICON_MSI, PROMOICON_CE, PROMOICON_NLP...) facets
 *         those are applicable selected store into single PROMOTION ICON facet
 */
public class HDMSolrRESTPromoIconFacetQueryPostprocessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {
	private static final String CLASSNAME = HDMSolrRESTFacetQueryPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;
	private SolrSearchConfigurationRegistry iSearchRegistry = null;

	/**
	 * @param componentId
	 */
	public HDMSolrRESTPromoIconFacetQueryPostprocessor(String componentId) {
		this.iComponentId = componentId;
		this.iSearchRegistry = SolrSearchConfigurationRegistry
				.getInstance(this.iComponentId);
	}

	/**
	 * main method having custom logic to group promotion icons
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryResponseObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}

		super.invoke(selectionCriteria, queryResponseObjects);
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		final String physicalStore_id = HDMUtils
				.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
						+ storeId);

		List<Map<String, Object>> facetViews = (LinkedList<Map<String, Object>>) iSearchResponseObject
				.getResponse().get(HDMConstants.FACET_VIEW);

		if (facetViews != null) {

			// individual promotion icon attributes(MSI, CE, MSI...) facet value
			// equal to the store selected by user, is added to the list of
			// facet entries
			// (finalFacetEntriesToMerge) to be shown in front end in a single
			// icon

			List<Map<String, Object>> finalFacetEntriesToMerge = new LinkedList<>();
			for (Iterator<Map<String, Object>> iter = facetViews.iterator(); iter
					.hasNext();) {
				Map<String, Object> facetView = iter.next();
				String facetName = facetView.get(HDMConstants.NAME) != null ? facetView
						.get(HDMConstants.NAME).toString() : "";

				Map<String, Object> extendedData = (Map<String, Object>) facetView
						.get(HDMConstants.EXTENDED_DATA);
				String attridentifier = (extendedData != null && extendedData
						.containsKey(HDMConstants.ATTR_IDENTIFIER)) ? (String) extendedData
						.get(HDMConstants.ATTR_IDENTIFIER) : "";
				if (attridentifier.startsWith(HDMConstants.PROMOICON)) {
					List<Map<String, Object>> facetEntries = (LinkedList<Map<String, Object>>) facetView
							.get(HDMConstants.ENTRY);

					for (Iterator<Map<String, Object>> iter1 = facetEntries
							.iterator(); iter1.hasNext();) {
						Map<String, Object> facetEntry = iter1.next();
						String label = (String) facetEntry
								.get(HDMConstants.LABEL);
						if (label.equalsIgnoreCase(physicalStore_id)) {
							facetEntry.put(HDMConstants.LABEL, facetName);

							// Added code to modify uniqueId, to facilitate url
							// sharing with facet selection - Begin
							String value = (String) facetEntry
									.get(HDMConstants.VALUE);
							if (null != value) {
								try {
									value = java.net.URLDecoder.decode(value,
											"UTF-8");
								} catch (UnsupportedEncodingException e) {
									LOGGER.logp(Level.INFO, CLASSNAME,
											METHODNAME,
											"UnsupportedEncodingException while modifying uniqueId of promotion");
								}
								value = value.split(HDMConstants.COLON)[0];

								String uniqueId = SpecialCharacterHelper
										.convertStrToAscii(value);
								Map<String, Object> extData = (Map<String, Object>) facetEntry
										.get(HDMConstants.EXTENDED_DATA);
								if (null != extData && null != uniqueId) {
									if (null != extData
											.get(HDMConstants.UNIQUEID)) {
										extData.put(HDMConstants.UNIQUEID,
												uniqueId);
									}
								}
							}

							// Added code to modify uniqueId - End

							finalFacetEntriesToMerge.add(facetEntry);
						} else {
							iter1.remove();
						}
					}

				}
			}

			// replacing PROMOICON's facetentries with
			// finalFacetEntriesToMerge(the list formed before)
			for (Iterator<Map<String, Object>> iter = facetViews.iterator(); iter
					.hasNext();) {
				Map<String, Object> facetView = iter.next();
				Map<String, Object> extendedData = (Map<String, Object>) facetView
						.get(HDMConstants.EXTENDED_DATA);
				String attridentifier = (extendedData != null && extendedData
						.containsKey(HDMConstants.ATTR_IDENTIFIER)) ? (String) extendedData
						.get(HDMConstants.ATTR_IDENTIFIER) : "";
				String facetName = facetView.get(HDMConstants.VALUE) != null ? facetView
						.get(HDMConstants.VALUE).toString() : "";
				if (attridentifier.startsWith(HDMConstants.PROMOICON)) {
					iter.remove();
				} else if (facetName.startsWith(HDMConstants.XX_PROMOICON)) {
					if (finalFacetEntriesToMerge != null
							&& !finalFacetEntriesToMerge.isEmpty()) {
						List<Map<String, Object>> facetEntries = (LinkedList<Map<String, Object>>) facetView
								.get(HDMConstants.ENTRY);

						for (Iterator<Map<String, Object>> iter1 = facetEntries
								.iterator(); iter1.hasNext();) {
							Map<String, Object> facetEntry = iter1.next();
							String label = (String) facetEntry
									.get(HDMConstants.LABEL);
							if (StringUtils.isNotBlank(label)) {
								iter1.remove();
							}
						}
						// replace name of the facet to name from the facetdesc

						String fname = (extendedData != null && extendedData
								.containsKey(HDMConstants.FNAME)) ? (String) extendedData
								.get(HDMConstants.FNAME) : "";
						facetView.put(HDMConstants.NAME, fname);
						// merge entries of promoicon attributes to promoicon
						// facet
						facetEntries.addAll(finalFacetEntriesToMerge);
					} else {
						// display promoicon facet only if promoicon attributes
						// exist
						iter.remove();
					}
				}
			}

		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
