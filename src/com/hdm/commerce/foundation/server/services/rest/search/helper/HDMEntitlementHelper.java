package com.hdm.commerce.foundation.server.services.rest.search.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.component.contextservice.ActivityToken;
import com.ibm.commerce.component.contextservice.BusinessContextService;
import com.ibm.commerce.component.contextservice.BusinessContextServiceFactory;
import com.ibm.commerce.component.contextserviceimpl.BusinessContextInternalService;
import com.ibm.commerce.context.entitlement.EntitlementContext;
import com.ibm.commerce.context.exception.BusinessContextServiceException;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.common.exception.ExceptionHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.internal.server.services.search.util.StoreHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.rest.search.SearchCriteria;

/**
 * @author TCS
 * 
 *         this helper class is having OOB FacetHelper logic with required
 *         changes/customizations in the logic changes for category facet
 *         change1:in case of the request is from browsing pages - to fetch
 *         parent,current and all level of child categories at all times
 *         change2:in case of the request is from search pages - to fetch only
 *         department(top category) at initial search and not clicked category
 *         facet change3:in case of the request is from search pages - to fetch
 *         only 1st level of child categories on click of category facet in
 *         search result changes for promotion icons change1:2 methods were made
 *         as public
 */
public class HDMEntitlementHelper {
	private static final String CLASSNAME = HDMEntitlementHelper.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private static final String ONE = "1";
	private static final String TRUE = "true";
	private static final String FLOAT_ONE = "1.0";
	private static final String CLASS_ENTITLEMENT_CONTEXT = "com.ibm.commerce.context.entitlement.EntitlementContext";

	private HDMEntitlementHelper() {
		// this method is not used
	}

	public static List<String> applyFieldNamingPattern(String origFieldName,
			final SelectionCriteria iSelectionCriteria) {
		final String METHODNAME = "applyFieldNamingPattern(String origFieldName,iSelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, origFieldName);
		}
		final ArrayList<String> fieldNameWithPattern = new ArrayList();
		if ((origFieldName != null)
				&& (origFieldName
						.startsWith(HDMConstants.PRICE_FACET_NAME_PREFEIX))) {
			final SearchCriteria searchCriteria = new SearchCriteria(
					iSelectionCriteria);
			final String storeId = searchCriteria
					.getControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
			final String compatiblePriceIndexMode = StoreHelper
					.getCompatiblePriceIndexMode(storeId);
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				if (compatiblePriceIndexMode != null) {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"compatiblePriceIndexMode:: {0}",
							compatiblePriceIndexMode);
				} else {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"Not define compatiblePriceIndexMode, will use compatible mode");
				}
			}

			if ((compatiblePriceIndexMode != null)
					&& (FLOAT_ONE.equals(compatiblePriceIndexMode))) {
				String strContractId = null;
				final boolean isRunningOnSearchServer = SolrSearchConfigurationRegistry
						.getInstance().isRunningOnSearchServer();
				if (isRunningOnSearchServer) {
					strContractId = getFinalUsableContract((SearchCriteria) iSelectionCriteria);
				} else {
					final BusinessContextService service = BusinessContextServiceFactory
							.getBusinessContextService();
					final ActivityToken activityToken = ((BusinessContextInternalService) service)
							.getActivityToken();
					try {
						final EntitlementContext entitlementCtx = (EntitlementContext) service
								.findContext(activityToken,
										CLASS_ENTITLEMENT_CONTEXT);
						strContractId = entitlementCtx
								.getCurrentTradingAgreementIds();
					} catch (BusinessContextServiceException e) {
						if (LoggingHelper.isTraceEnabled(LOGGER)) {
							LOGGER.logp(Level.WARNING, CLASSNAME, METHODNAME,
									"No final usable contract in context: {0}",
									ExceptionHelper
											.convertStackTraceToString(e));
						}
						throw new RuntimeException(e);
					}
				}
				final List<String> contractIdList = getContractIdAsStringListFromString(strContractId);
				for (final String contract : contractIdList) {
					final String newFieldName = origFieldName
							+ HDMConstants.UNDERSCORE + contract;
					fieldNameWithPattern.add(newFieldName);
					if (LoggingHelper.isTraceEnabled(LOGGER)) {
						LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
								"create new field name with: {0}", newFieldName);
					}
				}
			} else {
				final String physicalStore_id = HDMUtils
						.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME
								+ "_" + storeId);
				if (StringUtils.isNotBlank(physicalStore_id)) {
					origFieldName = origFieldName.concat(
							HDMConstants.UNDERSCORE).concat(physicalStore_id);
				}

				/**
				 * HDM PRO changes - update level of the org also to the price
				 * field price_MXN_8701_0 for instance - Divya
				 */
				if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
					final String orgLevel = HDMUtils
							.getCookieValue(HDMConstants.CUSTOMERLEVEL_COOKIENAME);
					String DIYPriceField = origFieldName ;
					if (StringUtils.isNotBlank(orgLevel)) {
						origFieldName = origFieldName.concat(
								HDMConstants.UNDERSCORE).concat(orgLevel);
					}
					fieldNameWithPattern.add(DIYPriceField);
				}
				fieldNameWithPattern.add(origFieldName);
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"applied field pattern: {0}", origFieldName);
				}
			}
		} else {
			fieldNameWithPattern.add(origFieldName);
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"applied field pattern: {0}", origFieldName);
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, fieldNameWithPattern);
		}
		return fieldNameWithPattern;
	}

	public static String getFinalUsableContract(
			final SearchCriteria searchCriteria) {
		final String METHODNAME = "getFinalUsableContract(SearchCriteria searchCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, searchCriteria);
		}

		String finalUsableContract = null;
		final List<Object> userSpecifiedContract = searchCriteria
				.getControlParameterValues(HDMConstants._WCF_SEARCH_CONTRACT);
		final String strStoreId = searchCriteria
				.getControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		if ((userSpecifiedContract == null)
				|| (userSpecifiedContract.isEmpty())) {
			final Long defaultContractId = StoreHelper
					.getDefaultContract(strStoreId);
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"Default contract is: {0}", defaultContractId);
			}

			if (defaultContractId == null) {
				throw new RuntimeException("No default contract for this store");
			}
			finalUsableContract = defaultContractId.toString();
			if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"default contract was found: {0}", defaultContractId);
			}
		} else {
			final String checkEntitlement = StoreHelper
					.getCheckEntitlementFlag(
							searchCriteria,
							searchCriteria
									.getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE),
							strStoreId);

			if ((TRUE.equals(checkEntitlement))
					|| (ONE.equals(checkEntitlement))) {
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"Check entitlement is enabled");
				}

				finalUsableContract = getContractFromRemoteOrLocal(searchCriteria);
			} else {
				finalUsableContract = getContractIdAsStringFromObjectList(userSpecifiedContract);
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, finalUsableContract);
		}
		return finalUsableContract;
	}

	public static String getContractIdAsStringFromObjectList(
			final List<Object> contractIdsAsArrayList) {
		final String METHODNAME = "getContractIdAsStringFromObjectList(List<Object> contractIdsAsArrayList)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, contractIdsAsArrayList);
		}

		if (contractIdsAsArrayList == null) {
			return "";
		}
		final String separator = " ";
		final StringBuilder sb = new StringBuilder();
		for (final Object contractIDElement : contractIdsAsArrayList) {
			sb.append(contractIDElement.toString());
			sb.append(separator);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, sb.toString().trim());
		}
		return sb.toString().trim();
	}

	public static List<String> getContractIdAsStringListFromString(
			final String contractIdsString) {
		final String METHODNAME = "getContractIdAsStringListFromString(String contractIdsString)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, contractIdsString);
		}
		ArrayList<String> contractIdAsList = new ArrayList();
		final String separator = " ";
		if ((contractIdsString != null)
				&& (contractIdsString.trim().length() != 0)) {
			if (contractIdsString.indexOf(HDMConstants.SEMI_COLON) != -1) {
				final String[] contractIdElement = contractIdsString.trim()
						.split(HDMConstants.SEMI_COLON);
				if ((contractIdElement != null)
						&& (contractIdElement.length > 0)) {
					contractIdAsList = new ArrayList(contractIdElement.length);
					for (int i = 0; i < contractIdElement.length; i++) {
						if (LoggingHelper.isTraceEnabled(LOGGER)) {
							LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
									"Contract {0} : {1}", new Object[] { i,
											contractIdElement[i] });
						}
						contractIdAsList.add(contractIdElement[i]);
					}
				}
			} else {
				final String[] contractIdElement = contractIdsString.trim()
						.split(separator);
				if ((contractIdElement != null)
						&& (contractIdElement.length > 0)) {
					contractIdAsList = new ArrayList(contractIdElement.length);
					for (int i = 0; i < contractIdElement.length; i++) {
						if (LoggingHelper.isTraceEnabled(LOGGER)) {
							LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
									"Contract {0} : {1}", new Object[] { i,
											contractIdElement[i] });
						}
						contractIdAsList.add(contractIdElement[i]);
					}
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
		return contractIdAsList;
	}

	public static List<String> getContractIdAsStringListFromObjectList(
			final List contractIdsAsObjectList) {
		final String METHODNAME = "getContractIdAsStringListFromObjectList(List contractIdsAsObjectList)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, contractIdsAsObjectList);
		}
		final String separator = " ";
		final ArrayList<String> contractIdsAsStringList = new ArrayList();
		StringTokenizer st;
		for (final Iterator localIterator = contractIdsAsObjectList.iterator(); localIterator
				.hasNext(); st.hasMoreElements()) {
			final Object contractIDElement = localIterator.next();
			final String strContractIDElement = contractIDElement.toString()
					.trim();
			st = new StringTokenizer(strContractIDElement, separator);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, contractIdsAsStringList);
		}

		return contractIdsAsStringList;
	}

	public static String getContractFromRemoteOrLocal(
			final SearchCriteria searchCriteria) {
		final String METHODNAME = "getContractFromRemoteOrLocal(SearchCriteria searchCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, searchCriteria);
		}
		String contractAsString = null;

		try {
			final List<Object> userSpecifiedContract = searchCriteria
					.getControlParameterValues(HDMConstants._WCF_SEARCH_CONTRACT);
			final List<String> userSpecifiedContractAsList = getContractIdAsStringListFromObjectList(userSpecifiedContract);

			final String strStoreId = searchCriteria
					.getControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
			contractAsString = StoreHelper.getUsableContractAsString(null,
					strStoreId, userSpecifiedContractAsList);

			searchCriteria.setControlParameterValue(
					HDMConstants._WCF_SEARCH_CONTRACT, contractAsString);
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"Set object: {0} into _wcf.search.contract",
						contractAsString);
			}
		} catch (Exception e) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.WARNING, CLASSNAME, METHODNAME,
						"No final usable contract: {0}",
						ExceptionHelper.convertStackTraceToString(e));
			}
			throw new RuntimeException(e);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, contractAsString);
		}

		return contractAsString;
	}
}