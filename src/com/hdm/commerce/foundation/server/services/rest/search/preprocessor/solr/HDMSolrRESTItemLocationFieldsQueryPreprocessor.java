package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

public class HDMSolrRESTItemLocationFieldsQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrRESTItemLocationFieldsQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTItemLocationFieldsQueryPreprocessor.class);
	private String iComponentId = null;

	public HDMSolrRESTItemLocationFieldsQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);

		try {
			String physicalStore_id = HDMUtils
					.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
							+ storeId);
			String oldQueryFields = iSolrQuery.getFields();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(oldQueryFields).append(HDMConstants.COMMA)
					.append(HDMConstants.AREA).append(HDMConstants.UNDERSCORE)
					.append(physicalStore_id).append(HDMConstants.COMMA)
					.append(HDMConstants.AISLE).append(HDMConstants.UNDERSCORE)
					.append(physicalStore_id).append(HDMConstants.COMMA)
					.append(HDMConstants.DESCRIPTION)
					.append(HDMConstants.UNDERSCORE).append(physicalStore_id)
					.append(HDMConstants.COMMA).append(HDMConstants.BAY)
					.append(HDMConstants.UNDERSCORE).append(physicalStore_id);
			iSolrQuery.setFields(stringBuilder.toString());
		} catch (Exception e) {
			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
					"Failed during itemlocation solr query construction");
			LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME, e.getMessage(), e);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

	}
}
