package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

public class HDMSolrSearchAllContentProductQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASS_NAME = HDMSolrSearchAllContentProductQueryPreprocessor.class
			.getName(),
			CONTROL_PARAMNAME = "hdm.search.hdmContentType",
			INDEXFIELD_EXTERNALVALUE = "hdmContentType",
			DEFAULT_FILTER = "TODO";
	private String iComponentId = null;
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASS_NAME);

	public HDMSolrSearchAllContentProductQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		final String METHOD_NAME = "invoke";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASS_NAME, METHOD_NAME);
		}

		try {
			super.invoke(selectionCriteria, queryRequestObjects);

			SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
					.getInstance(iComponentId);
			final String profile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
			String hdmContentTypeValue = getControlParameterValue(CONTROL_PARAMNAME);
			if (LoggingHelper.isTraceEnabled(LOGGER))
				LOGGER.logp(Level.INFO, CLASS_NAME, METHOD_NAME,
						"HDMContentType value is {0}",
						new Object[] { hdmContentTypeValue });
			final String fieldName = searchRegistry.getIndexFieldName(profile,
					INDEXFIELD_EXTERNALVALUE);
			StringBuilder excludeBuilder = new StringBuilder();

			/*
			 * Filter non content products in normal CLP and display content
			 * products in Content list page based on filter types
			 */

			String contentTypeValue = "*", exclude = "";
			if (StringUtils.isNotBlank(hdmContentTypeValue)
					&& !DEFAULT_FILTER.equalsIgnoreCase(hdmContentTypeValue)) {
				contentTypeValue = hdmContentTypeValue;
			} else if (!DEFAULT_FILTER.equalsIgnoreCase(hdmContentTypeValue)) {
				exclude = "-";
			}
			excludeBuilder.append(exclude).append(String.valueOf(fieldName))
					.append(":").append(contentTypeValue).toString();

			iSolrQuery.addFilterQuery(excludeBuilder.toString());
			String eSiteExclusiveContent;
			StringBuilder esiteExcludeBuilder = new StringBuilder();
			if (!exclude.equals("-")) {
				final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
				if (!storeId.equals(HDMConstants.PRO_STORE_ID)) {
					eSiteExclusiveContent = "PRO";

				} else {
					eSiteExclusiveContent = "DIY";
				}

				esiteExcludeBuilder.append("-").append("EsiteExCntProduct")
						.append(":").append(eSiteExclusiveContent).toString();

				iSolrQuery.addFilterQuery(esiteExcludeBuilder.toString());
			}
			if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
				LOGGER.exiting(CLASS_NAME, METHOD_NAME);
			}
		} catch (Exception e) {
			LOGGER.logp(Level.SEVERE, CLASS_NAME, METHOD_NAME, e.getMessage(),
					e);

		}
	}
}
