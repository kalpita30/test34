package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.exception.AbstractApplicationException;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.HierarchyHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.rest.search.SearchCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.valuemapping.ValueMappingService;

public class HDMSolrRESTCategoryHierarchyPostprocessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {

	private static final String CLASSNAME = HDMSolrRESTCategoryHierarchyPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iCatalogId = null;
	private String iComponentId = null;
	private ValueMappingService iMappingService = null;
	private String iCatalogEntryViewMapper = null;
	private String iCatalogEntryViewName = null;
	private String iCatalogEntryIdName = null;
	private String iCatalogEntryGroupViewName = null;

	public HDMSolrRESTCategoryHierarchyPostprocessor(final String componentId) {
		iComponentId = componentId;
	}

	@Override
	public void invoke(final SelectionCriteria selectionCriteria,
			final Object... queryResponseObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}

		super.invoke(selectionCriteria, queryResponseObjects);

		initMappingParameters();
		List<Map<String, Object>> catalogEntryViews = (LinkedList) iSearchResponseObject
				.getResponse().get(iCatalogEntryViewName);

		if (LoggingHelper.isTraceEnabled(LOGGER))
			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
					"catalogEntryViews is {0}",
					new Object[] { Arrays.asList(catalogEntryViews) });

		if ((catalogEntryViews == null) || (catalogEntryViews.isEmpty())) {
			Map<String, LinkedList<Map<String, Object>>> catalogEntryGroups = (Map) iSearchResponseObject
					.getResponse().get(iCatalogEntryGroupViewName);
			if ((catalogEntryGroups != null) && (catalogEntryGroups.size() > 0)) {
				catalogEntryViews = new LinkedList();

				final Iterator localIterator1 = catalogEntryGroups.entrySet()
						.iterator();
				while (localIterator1.hasNext()) {
					final Map.Entry<String, LinkedList<Map<String, Object>>> catalogEntryGroup = (Map.Entry) localIterator1
							.next();
					catalogEntryViews.addAll((Collection) catalogEntryGroup
							.getValue());
				}
			}
		}

		for (final Map<String, Object> catalogEntryView : catalogEntryViews) {
			Object obj = catalogEntryView.get("parentCatalogGroupID");
			if (obj instanceof ArrayList<?>) {
				final ArrayList<String> parentIds = (ArrayList<String>) catalogEntryView
						.get("parentCatalogGroupID");
				String catuniqueId = null;
				for (final Iterator localIterator1 = parentIds.iterator(); localIterator1
						.hasNext();) {
					final String parent = (String) localIterator1.next();
					if (parent.startsWith(iCatalogId)) {
						final String[] parentIdsArray = parent.split("_");
						final Integer parentIdsArraySize = parentIdsArray.length;
						catuniqueId = parentIdsArray[parentIdsArraySize - 1];
						break;
					}
				}
				if (catuniqueId != null) {
					try {
						final List<List<String>> ancestorCategoriesList = HierarchyHelper
								.getNavigationPath(catuniqueId,
										(SearchCriteria) selectionCriteria);
						if (ancestorCategoriesList != null) {
							final List<String> categoryIdsList = ancestorCategoriesList
									.get(0);
							catalogEntryView.put("topeParentIds",
									categoryIdsList);
						}
					} catch (AbstractApplicationException e) {
						LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME,
								e.getMessage(), e);
					}
				}
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	protected void initMappingParameters() {
		final String METHODNAME = "initMappingParameters";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		iMappingService = ValueMappingService.getInstance(iSelectionCriteria
				.getComponentId());
		final SearchCriteria searchCriteria = new SearchCriteria(
				iSelectionCriteria);
		final String resourceName = searchCriteria
				.getControlParameterValue("_wcf.search.internal.service.resource");

		if (LoggingHelper.isTraceEnabled(LOGGER))
			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
					"resourceName is {0}", new Object[] { resourceName });
		iCatalogEntryViewMapper = getMapperName(searchCriteria, resourceName,
				"CatalogEntryView");
		iCatalogId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CATALOG);
		iCatalogEntryViewName = getExternalFieldName(iMappingService,
				iCatalogEntryViewMapper, "catalogEntryView");
		iCatalogEntryIdName = getExternalFieldName(iMappingService,
				iCatalogEntryViewMapper, "catentry_id");
		iCatalogEntryGroupViewName = getExternalFieldName(iMappingService,
				"XPathToGroupingBODResponseFieldNameMapping",
				"catalogEntryGroupView");
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}
