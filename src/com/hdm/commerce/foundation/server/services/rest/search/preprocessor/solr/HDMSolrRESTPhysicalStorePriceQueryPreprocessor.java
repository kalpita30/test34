package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.apache.solr.core.CoreContainer;
import org.apache.wink.client.ApacheHttpClientConfig;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

public class HDMSolrRESTPhysicalStorePriceQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrRESTPhysicalStorePriceQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);

	private String iComponentId = null;

	public HDMSolrRESTPhysicalStorePriceQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	@Override
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}
		super.invoke(selectionCriteria, queryRequestObjects);

		SolrSearchConfigurationRegistry configurationRegistry = SolrSearchConfigurationRegistry
				.getInstance();

		final String solsearchServerName = getControlParameterValue(HDMConstants.HDM_SEARCH_SERVERNAME);
		final String solsearchServerPort = getControlParameterValue(HDMConstants.HDM_SEARCH_SERVERPORT);

		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(
					Level.INFO,
					CLASSNAME,
					METHODNAME,
					"The searchServerName and searchServerPort in Search - Rest is  {0} and {1}",
					new Object[] { solsearchServerName, solsearchServerPort });
		}
		try {

			String customPriceSubcoreName = null;
			CoreContainer coreContainer = configurationRegistry
					.getCoreContainer();
			for (String coreName : coreContainer.getAllCoreNames()) {
				if (coreName.endsWith(HDMConstants.CUSTOM_PRICE_CORE_NAME)) {
					customPriceSubcoreName = coreName;
				}
			}

			final StringBuilder stringBuilder = new StringBuilder();
			final String restURL = stringBuilder.append(HDMConstants.HTTP)
					.append(solsearchServerName).append(HDMConstants.COLON)
					.append(solsearchServerPort)
					.append(HDMConstants.FORWARD_SLASH)
					.append(HDMConstants.SOLR)
					.append(HDMConstants.FORWARD_SLASH)
					.append(customPriceSubcoreName)
					.append(HDMConstants.FORWARD_SLASH)
					.append(HDMConstants.SELECT)
					.append(HDMConstants.QUESTION_MARK)
					.append(HDMConstants.GET_PRICE_PHYSICAL_STORE_FIELDS)
					.toString();
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"The restURL constructed in the class {0} is {1}",
						new Object[] { CLASSNAME, restURL });
			}
			final ClientConfig clientConfig = new ApacheHttpClientConfig();
			final RestClient restClient = new RestClient(clientConfig);
			final Resource restResource = restClient.resource(restURL);
			restResource.accept(MediaType.APPLICATION_JSON);
			final ClientResponse clientResponse = restResource.get();
			final String responseEntity = clientResponse
					.getEntity(String.class);
			iSolrQuery.addField(responseEntity);
		} catch (Exception e) {
			LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME, e.getMessage(), e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}
