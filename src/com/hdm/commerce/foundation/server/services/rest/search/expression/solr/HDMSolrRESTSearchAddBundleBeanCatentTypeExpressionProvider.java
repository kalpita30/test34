package com.hdm.commerce.foundation.server.services.rest.search.expression.solr;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.expression.SearchExpressionProvider;
import com.ibm.commerce.foundation.server.services.search.expression.solr.AbstractSolrSearchExpressionProvider;

public class HDMSolrRESTSearchAddBundleBeanCatentTypeExpressionProvider extends
AbstractSolrSearchExpressionProvider implements
SearchExpressionProvider{
	private static final String CLASSNAME = HDMSolrRESTSearchAddBundleBeanCatentTypeExpressionProvider.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTSearchAddBundleBeanCatentTypeExpressionProvider.class);
	private String iComponentId = null;
	private static final String CATALOG_ENTRY_TYPE_PRODUCT_ITEM = "ItemBean";
	private static final String CATALOG_ENTRY_TYPE_BUNDLE = "BundleBean";
	private static final String CONTROLPARAM_FILTERQUERY = "_wcf.search.internal.filterquery";
	private static final String CATALOGENTRY_TYPECODE = "CatalogEntryView/CatalogEntryTypeCode";

	public HDMSolrRESTSearchAddBundleBeanCatentTypeExpressionProvider(String componentId) {
		this.iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria)
			throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria)";
		
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					new Object[] { selectionCriteria });
		}
		
		super.invoke(selectionCriteria);
		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(this.iComponentId);
		String profile = getControlParameterValue("_wcf.search.profile");
		
		List<String> filterQueryList = getControlParameterValues(CONTROLPARAM_FILTERQUERY);
		for (int i = 0; i < filterQueryList.size(); i++) {
			String filterQuery = filterQueryList.get(i);
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						" filterQuery: {0}", filterQuery);
			}
			if (StringUtils.isNotEmpty(filterQuery)
					&& filterQuery
							.equalsIgnoreCase("+catenttype_id_ntk_cs:ItemBean")) {
				removeControlParameterValue(CONTROLPARAM_FILTERQUERY,
						"+catenttype_id_ntk_cs:ItemBean");
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"Removed successfully}");
				}
			}
		}		
			
		String searchExpression = "+"
				+ searchRegistry.getIndexFieldName(profile,
						CATALOGENTRY_TYPECODE) + ":("
				+ CATALOG_ENTRY_TYPE_PRODUCT_ITEM + " "
				+ CATALOG_ENTRY_TYPE_BUNDLE + ")";
		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
					"custom searchExpression: {0}", searchExpression);
		}
		addControlParameterValue(CONTROLPARAM_FILTERQUERY, searchExpression);
	}
}
