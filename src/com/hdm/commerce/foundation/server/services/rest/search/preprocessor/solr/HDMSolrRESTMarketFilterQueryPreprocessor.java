package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author TCS
 *
 *         this pre processor class is having logic change in solr query to
 *         fetch statistics info for price field
 */
public class HDMSolrRESTMarketFilterQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrRESTMarketFilterQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTMarketFilterQueryPreprocessor.class);
	private String iComponentId = null;

	/**
	 * @param componentId
	 */
	public HDMSolrRESTMarketFilterQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	/**
	 * main method having business logic
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);

		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		String excludeMktAttr = HDMConstants.EXCLUDEMARKET;
		String storeOnlyAvailAttr = HDMConstants.STORE_ONLY_AVAILABLE;
		if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
			excludeMktAttr = HDMConstants.PRO_EXCLUDEMARKET;
			storeOnlyAvailAttr = HDMConstants.PRO_STORE_ONLY_AVAILABLE;
		}
		String market_Id = HDMUtils
				.getCookieValue(HDMConstants.MARKETID_COOKIENAME + "_"
						+ storeId);
		String physicalStoreId = HDMUtils
				.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
						+ storeId);
		if (StringUtils.isNotBlank(physicalStoreId)
				&& StringUtils.isNotBlank(market_Id)) {
			String aStringArray = (HDMConstants.HYPHEN.concat(excludeMktAttr)
					.concat(HDMConstants.COLON).concat(market_Id));

			if (LoggingHelper.isTraceEnabled(LOGGER)) {

				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"aStringArray :{0}", new Object[] { aStringArray });

			}
			iSolrQuery.addFilterQuery(aStringArray);
		} else {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME,
						"physicalStoreId or market_Id missing in the request : "
								+ market_Id + ":" + physicalStoreId
								+ "iSolrQuery:" + iSolrQuery);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
