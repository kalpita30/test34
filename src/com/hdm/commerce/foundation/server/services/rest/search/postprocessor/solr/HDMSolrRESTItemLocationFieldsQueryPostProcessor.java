package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;

public class HDMSolrRESTItemLocationFieldsQueryPostProcessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {

	private static final String CLASSNAME = HDMSolrRESTItemLocationFieldsQueryPostProcessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);

	private String iComponentId = null;

	public HDMSolrRESTItemLocationFieldsQueryPostProcessor(
			final String componentId) {
		iComponentId = componentId;
	}

	@Override
	public void invoke(final SelectionCriteria selectionCriteria,
			final Object... queryResponseObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}

		super.invoke(selectionCriteria, queryResponseObjects);

		List<Map<String, Object>> catalogEntryViews = (LinkedList) iSearchResponseObject
				.getResponse().get("catalogEntryView");

		if ((catalogEntryViews == null) || (catalogEntryViews.isEmpty())) {
			Map<String, LinkedList<Map<String, Object>>> catalogEntryGroups = (Map) iSearchResponseObject
					.getResponse().get("catalogEntryGroupView");
			if ((catalogEntryGroups != null) && (catalogEntryGroups.size() > 0)) {
				catalogEntryViews = new LinkedList();
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(
							Level.INFO,
							CLASSNAME,
							METHODNAME,
							"catalogEntryViews was empty,so updating the details from entryGroupView {0}",
							new Object[] { catalogEntryViews });
				}
				final Iterator localIterator1 = catalogEntryGroups.entrySet()
						.iterator();
				while (localIterator1.hasNext()) {
					final Map.Entry<String, LinkedList<Map<String, Object>>> catalogEntryGroup = (Map.Entry) localIterator1
							.next();
					catalogEntryViews.addAll((Collection) catalogEntryGroup
							.getValue());
				}
			}
		}

		String areaField = HDMConstants.AREA;
		String bayField = HDMConstants.BAY;
		String descriptionField = HDMConstants.DESCRIPTION;
		String aisleField = HDMConstants.AISLE;
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);

		final String physicalStoreId = HDMUtils
				.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
						+ storeId);
		if (StringUtils.isNotBlank(physicalStoreId)) {
			areaField = areaField.concat(HDMConstants.UNDERSCORE).concat(
					physicalStoreId);
			bayField = bayField.concat(HDMConstants.UNDERSCORE).concat(
					physicalStoreId);
			descriptionField = descriptionField.concat(HDMConstants.UNDERSCORE)
					.concat(physicalStoreId);
			aisleField = aisleField.concat(HDMConstants.UNDERSCORE).concat(
					physicalStoreId);
		}

		if ((catalogEntryViews != null) && (!catalogEntryViews.isEmpty())) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"catalogEntryViews is not empty {0}",
						new Object[] { catalogEntryViews });
			}
			for (final Map<String, Object> catalogEntryView : catalogEntryViews) {
				if (catalogEntryView.containsKey(areaField)) {
					Object areaFieldValue = catalogEntryView.remove(areaField);
					catalogEntryView.put("area", areaFieldValue);
				}
				if (catalogEntryView.containsKey(bayField)) {
					Object bayFieldValue = catalogEntryView.remove(bayField);
					catalogEntryView.put("bay", bayFieldValue);
				}
				if (catalogEntryView.containsKey(descriptionField)) {
					Object descriptionFieldValue = catalogEntryView
							.remove(descriptionField);
					catalogEntryView.put("itemDescription",
							descriptionFieldValue);
				}
				if (catalogEntryView.containsKey(aisleField)) {
					Object aisleFieldValue = catalogEntryView
							.remove(aisleField);
					catalogEntryView.put("aisle", aisleFieldValue);
				}

			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}
