package com.hdm.commerce.foundation.server.services.rest.search.expression.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.expression.SearchExpressionProvider;
import com.ibm.commerce.foundation.server.services.search.expression.solr.AbstractSolrSearchExpressionProvider;

public class HDMSolrRESTSearchTypeExpressionProvider extends
		AbstractSolrSearchExpressionProvider implements
		SearchExpressionProvider {
	private static final String CLASSNAME = HDMSolrRESTSearchTypeExpressionProvider.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTSearchTypeExpressionProvider.class);

	private String iComponentId = null;
	private static final String CATALOG_ENTRY_TYPE_PRODUCT_ITEM = "ItemBean";
	private static final String CATALOG_ENTRY_TYPE_BUNDLE = "BundleBean";
	private static final String CONTROLPARAM_FILTERQUERY = "_wcf.search.internal.filterquery";
	private static final String CATALOGENTRY_TYPECODE = "CatalogEntryView/CatalogEntryTypeCode";

	public HDMSolrRESTSearchTypeExpressionProvider(String componentId) {
		this.iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria)
			throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					new Object[] { selectionCriteria });
		}

		super.invoke(selectionCriteria);

		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(this.iComponentId);
		String profile = getControlParameterValue("_wcf.search.profile");
		String type = getControlParameterValue("_wcf.search.type");
		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME, "Search type: {0}",
					type);
		}

		type = ((type == null) || (type.length() == 0)) ? searchRegistry
				.getSolrSearchProfileConfiguration(profile).getParameters()
				.get("searchType") : type;
		if ((type == null) || (type.length() == 0)) {
			type = "1000";
		}
		setControlParameterValue("_wcf.search.type", type);

		if ((type.equals("0")) || (type.equals("1")) || (type.equals("2"))
				|| (type.equals("3"))) {
			String searchExpression = "-"
					+ searchRegistry.getIndexFieldName(profile,
							CATALOGENTRY_TYPECODE) + ":"
					+ CATALOG_ENTRY_TYPE_PRODUCT_ITEM;

			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"searchExpression: {0} in type {1}", new Object[] {
								searchExpression, type });
			}
			addControlParameterValue(CONTROLPARAM_FILTERQUERY, searchExpression);
		} else if ((type.equals("100")) || (type.equals("101"))
				|| (type.equals("102")) || (type.equals("103"))) {
			String searchExpression = "+"
					+ searchRegistry.getIndexFieldName(profile,
							CATALOGENTRY_TYPECODE) + ":"
					+ CATALOG_ENTRY_TYPE_PRODUCT_ITEM;

			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"searchExpression: {0}", searchExpression);
			}
			addControlParameterValue(CONTROLPARAM_FILTERQUERY, searchExpression);
		} else if ((type.equals("1000")) || (type.equals("1001"))
				|| (type.equals("1002")) || (type.equals("1003"))) {
			String searchExpression = searchRegistry.getIndexFieldName(profile,
					CATALOGENTRY_TYPECODE) + ":(ItemBean BundleBean)";

			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"searchExpression:: {0}", searchExpression);
			}
			addControlParameterValue(CONTROLPARAM_FILTERQUERY, searchExpression);
		} else if ((type.equals("10000")) || (type.equals("10001"))
				|| (type.equals("10002")) || (type.equals("10003"))) {
			String searchExpression = searchRegistry.getIndexFieldName(profile,
					CATALOGENTRY_TYPECODE) + ":(ItemBean BundleBean)";

			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"searchExpression: {0}", searchExpression);
			}
			addControlParameterValue(CONTROLPARAM_FILTERQUERY, searchExpression);
		}
			else if (type.equals("105")) {
			String searchExpression = "+"
					+ searchRegistry.getIndexFieldName(profile,
							CATALOGENTRY_TYPECODE) + ":("
					+ CATALOG_ENTRY_TYPE_PRODUCT_ITEM + " "
					+ CATALOG_ENTRY_TYPE_BUNDLE + ")";
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"custom searchExpression: {0}", searchExpression);
			}
			addControlParameterValue(CONTROLPARAM_FILTERQUERY, searchExpression);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER))
			LOGGER.exiting(CLASSNAME, METHODNAME);
	}
}