package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.foundation.server.services.rest.search.helper.HDMEntitlementHelper;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.StoreHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.rest.search.SearchCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.valuemapping.ValueMappingService;

public class HDMSolrRESTSearchCatalogEntryViewPriceQueryPostprocessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {

	private static final String CLASSNAME = HDMSolrRESTSearchCatalogEntryViewPriceQueryPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);

	private String iComponentId = null;
	private ValueMappingService iMappingService = null;

	private String iCatalogEntryViewMapper = null;
	private String iPriceMapper = null;

	private String iCatalogEntryViewName = null;
	private String iCatalogEntryIdName = null;
	private String iCatalogEntryGroupViewName = null;

	private String iPriceName = null;
	private String iPriceValueName = null;
	private String iPriceCurrencyName = null;
	private String iPriceDescriptionName = null;
	private String iPriceUsageName = null;
	private String iPriceContractId = null;

	public HDMSolrRESTSearchCatalogEntryViewPriceQueryPostprocessor(
			final String componentId) {
		iComponentId = componentId;
	}

	@Override
	public void invoke(final SelectionCriteria selectionCriteria,
			final Object... queryResponseObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}

		super.invoke(selectionCriteria, queryResponseObjects);

		initMappingParameters();
		final String searchProfile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
		String priceMode = "1";
		final String iCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
		final String storeId = getControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		priceMode = StoreHelper.getPriceMode(searchProfile, storeId);
		if (priceMode != null) {
			Map<String, Object> metaData = (Map) iSearchResponseObject
					.getResponse().get("metaData");
			if (metaData == null) {
				metaData = new LinkedHashMap();
				iSearchResponseObject.getResponse().put("metaData", metaData);
			}
			metaData.put("price", priceMode);
		}

		List<Map<String, Object>> catalogEntryViews = (LinkedList) iSearchResponseObject
				.getResponse().get(iCatalogEntryViewName);

		if ((catalogEntryViews == null) || (catalogEntryViews.isEmpty())) {
			Map<String, LinkedList<Map<String, Object>>> catalogEntryGroups = (Map) iSearchResponseObject
					.getResponse().get(iCatalogEntryGroupViewName);
			if ((catalogEntryGroups != null) && (catalogEntryGroups.size() > 0)) {
				catalogEntryViews = new LinkedList();

				final Iterator localIterator1 = catalogEntryGroups.entrySet()
						.iterator();
				while (localIterator1.hasNext()) {
					final Map.Entry<String, LinkedList<Map<String, Object>>> catalogEntryGroup = (Map.Entry) localIterator1
							.next();
					catalogEntryViews.addAll((Collection) catalogEntryGroup
							.getValue());
				}
			}
		}

		String offerPriceFieldNamePrefix = HDMConstants.PRICE_FACET_NAME_PREFEIX;
		String maxOfferPriceFieldNamePrefix = HDMConstants.PRICE_FACET_NAME_PREFEIX;
		String minOfferPriceFieldNamePrefix = HDMConstants.PRICE_FACET_NAME_PREFEIX;
		final String physicalStoreId = HDMUtils
				.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
						+ storeId);
		String listPriceFieldName = "listprice_";
		String offerPriceFieldName = "price_";
		if (iCurrencyCode != null && null != physicalStoreId
				&& physicalStoreId.length() > 0) {
			listPriceFieldName = listPriceFieldName.concat(iCurrencyCode)
					.concat(HDMConstants.UNDERSCORE).concat(physicalStoreId);
			offerPriceFieldName = offerPriceFieldName.concat(iCurrencyCode)
					.concat(HDMConstants.UNDERSCORE).concat(physicalStoreId);
			offerPriceFieldNamePrefix = offerPriceFieldNamePrefix
					.concat(iCurrencyCode);
			maxOfferPriceFieldNamePrefix = maxOfferPriceFieldNamePrefix
					.concat(iCurrencyCode).concat(HDMConstants.UNDERSCORE)
					.concat(physicalStoreId).concat(HDMConstants.UNDERSCORE)
					.concat(HDMConstants.MAX);
			minOfferPriceFieldNamePrefix = minOfferPriceFieldNamePrefix
					.concat(iCurrencyCode).concat(HDMConstants.UNDERSCORE)
					.concat(physicalStoreId).concat(HDMConstants.UNDERSCORE)
					.concat(HDMConstants.MIN);
		}

		if ((catalogEntryViews != null) && (!catalogEntryViews.isEmpty())) {
			for (final Map<String, Object> catalogEntryView : catalogEntryViews) {
				if (catalogEntryView.containsKey(maxOfferPriceFieldNamePrefix)) {
					Object maxPriceValue = catalogEntryView
							.remove(maxOfferPriceFieldNamePrefix);
					catalogEntryView.put("maxOfferPrice", maxPriceValue);
				}
				if (catalogEntryView.containsKey(minOfferPriceFieldNamePrefix)) {
					Object minPriceValue = catalogEntryView
							.remove(minOfferPriceFieldNamePrefix);
					catalogEntryView.put("minOfferPrice", minPriceValue);
				}

				Map<String, Object> finalPriceMap = new HashMap();
				final List<Object> finalPriceList = new ArrayList();

				final String store_Id = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
				
				// List price changes start
				if (catalogEntryView.containsKey(listPriceFieldName)) {
					Object priceValue = catalogEntryView
							.remove(listPriceFieldName);
					finalPriceMap.put(iPriceValueName, priceValue);
				} else {
					finalPriceMap.put(iPriceValueName, "");
				}

				finalPriceMap.put(iPriceDescriptionName, "L");
				finalPriceMap.put(iPriceCurrencyName, iCurrencyCode);
				finalPriceMap.put(iPriceUsageName, "Display");
				finalPriceList.add(finalPriceMap);
				// List price changes till here end

				if ("1".equals(priceMode)) {
					final List<String> fieldNameWithPatternList = HDMEntitlementHelper
							.applyFieldNamingPattern(offerPriceFieldNamePrefix,
									selectionCriteria);
					for (final String priceFieldNameWithPattern : fieldNameWithPatternList) {
						finalPriceMap = new HashMap();
						final String contractId = getAppliedContractIdFromPriceField(priceFieldNameWithPattern);
						if (catalogEntryView
								.containsKey(priceFieldNameWithPattern)) {
							if(HDMConstants.PRO_STORE_ID.equals(store_Id) && priceFieldNameWithPattern.equalsIgnoreCase(offerPriceFieldName))
							{
								Object priceValue = catalogEntryView
										.remove(priceFieldNameWithPattern);

								finalPriceMap.put(iPriceValueName, priceValue);
								finalPriceMap.put(iPriceDescriptionName, "I");
								finalPriceMap
										.put(iPriceCurrencyName, iCurrencyCode);
								finalPriceMap.put(iPriceUsageName, "Offer_DIY");
								if (contractId != null) {
									finalPriceMap.put(iPriceContractId, contractId);
								}
							}
							else
							{
							Object priceValue = catalogEntryView
									.remove(priceFieldNameWithPattern);
							finalPriceMap.put(iPriceValueName, priceValue);
							finalPriceMap.put(iPriceDescriptionName, "I");
							finalPriceMap
									.put(iPriceCurrencyName, iCurrencyCode);
							finalPriceMap.put(iPriceUsageName, "Offer");
							if (contractId != null) {
								finalPriceMap.put(iPriceContractId, contractId);
							}
							}
							finalPriceList.add(finalPriceMap);
						} else {
							finalPriceMap.put(iPriceValueName, "");
							if (contractId != null) {
								finalPriceMap.put(iPriceContractId, contractId);
							}
							finalPriceList.add(finalPriceMap);
						}
						if (LoggingHelper.isTraceEnabled(LOGGER)) {
						LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
								"finalPriceList is :: {0}",
								finalPriceList);
						}
						
						
						catalogEntryView.put(iPriceName, finalPriceList);
					}
				}
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	protected String getAppliedContractIdFromPriceField(
			final String priceFieldName) {
		final String METHODNAME = "getAppliedContractIdFromPriceField";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, priceFieldName);
		}

		String contractId = null;

		if ((priceFieldName != null)
				&& (priceFieldName
						.startsWith(HDMConstants.PRICE_FACET_NAME_PREFEIX))) {
			final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
			final String compatiblePriceIndexMode = StoreHelper
					.getCompatiblePriceIndexMode(storeId);

			if ((compatiblePriceIndexMode != null)
					&& ("1.0".equals(compatiblePriceIndexMode))) {
				final int index = priceFieldName
						.lastIndexOf(HDMConstants.UNDERSCORE);
				contractId = priceFieldName.substring(index + 1);
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, contractId);
		}
		return contractId;
	}

	protected void initMappingParameters() {
		final String METHODNAME = "initMappingParameters";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		iMappingService = ValueMappingService.getInstance(iSelectionCriteria
				.getComponentId());
		final SearchCriteria searchCriteria = new SearchCriteria(
				iSelectionCriteria);
		final String resourceName = searchCriteria
				.getControlParameterValue("_wcf.search.internal.service.resource");
		iCatalogEntryViewMapper = getMapperName(searchCriteria, resourceName,
				"CatalogEntryView");

		iCatalogEntryViewName = getExternalFieldName(iMappingService,
				iCatalogEntryViewMapper, "catalogEntryView");
		iCatalogEntryIdName = getExternalFieldName(iMappingService,
				iCatalogEntryViewMapper, "catentry_id");
		iCatalogEntryGroupViewName = getExternalFieldName(iMappingService,
				"XPathToGroupingBODResponseFieldNameMapping",
				"catalogEntryGroupView");

		iPriceMapper = getMapperName(searchCriteria, resourceName,
				"CatalogEntryView/Price");

		iPriceName = getExternalFieldName(iMappingService, iPriceMapper,
				"price");
		iPriceValueName = getExternalFieldName(iMappingService, iPriceMapper,
				"value");
		iPriceDescriptionName = getExternalFieldName(iMappingService,
				iPriceMapper, "description");
		iPriceCurrencyName = getExternalFieldName(iMappingService,
				iPriceMapper, "currency");
		iPriceUsageName = getExternalFieldName(iMappingService, iPriceMapper,
				"usage");
		iPriceContractId = getExternalFieldName(iMappingService, iPriceMapper,
				"contractId");

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
