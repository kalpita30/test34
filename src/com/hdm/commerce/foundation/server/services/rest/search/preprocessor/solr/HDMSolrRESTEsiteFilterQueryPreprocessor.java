/**
 * 
 */
package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author TCS
 *
 */
public class HDMSolrRESTEsiteFilterQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {

	private static final String CLASSNAME = HDMSolrRESTEsiteFilterQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTEsiteFilterQueryPreprocessor.class);

	private String iComponentId = null;

	/**
	 * @param componentId
	 */
	public HDMSolrRESTEsiteFilterQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	/**
	 * main method having business logic
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		String storeType = "DIY";
		if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
			storeType = "PRO";
		}

		String aStringArray = (HDMConstants.HYPHEN
				.concat(HDMConstants.OPEN_PARANTHESIS)
				.concat(HDMConstants.EXCLUDSITE).concat(HDMConstants.COLON)
				.concat(storeType).concat(HDMConstants.CLOSE_PARANTHESIS));

		if (LoggingHelper.isTraceEnabled(LOGGER)) {

			LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME, "aStringArray :{0}",
					new Object[] { aStringArray });

		}
		iSolrQuery.addFilterQuery(aStringArray);
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}
