package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.solr.core.CoreContainer;
import org.apache.wink.client.ApacheHttpClientConfig;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author TCS This class is used to dynamically add the number of rows
 *         available in solr response to the query
 *
 */
public class HDMSolrRESTMaxRowsQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrRESTMaxRowsQueryPreprocessor.class
			.getName();
	private String iComponentId = null;
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);

	public HDMSolrRESTMaxRowsQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	@Override
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);
		try {

			SolrSearchConfigurationRegistry configurationRegistry = SolrSearchConfigurationRegistry
					.getInstance();
			final String searchProfileName = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
			final String solsearchServerName = getControlParameterValue(HDMConstants.HDM_SEARCH_SERVERNAME);
			final String solsearchServerPort = getControlParameterValue(HDMConstants.HDM_SEARCH_SERVERPORT);

			String catalogEntryCore = null, catalogGroupCore = null, core = null, timeAllowed = null;
			CoreContainer coreContainer = configurationRegistry
					.getCoreContainer();
			for (String coreName : coreContainer.getAllCoreNames()) {
				if (coreName.contains(HDMConstants.CATALOGENTRY)) {
					catalogEntryCore = coreName;
				} else if (coreName.contains(HDMConstants.CATALOGGROUP)) {
					catalogGroupCore = coreName;
				}
			}

			String queryParams = (HDMConstants.QUERY)
					.concat(HDMConstants.EQUAL).concat(HDMConstants.WILD_CARD)
					.concat(HDMConstants.COLON).concat(HDMConstants.WILD_CARD);
			if (HDMConstants.CATALOG_ENTRY_PROFILE_BV
					.equalsIgnoreCase(searchProfileName)
					|| HDMConstants.CATALOG_ENTRY_PROFILE_EYS
							.equalsIgnoreCase(searchProfileName)) {
				core = catalogEntryCore;
				timeAllowed = HDMConstants.CATALOGENTRY_TIMEALLOWED;
				queryParams = queryParams.concat(HDMConstants.AMPERSAND)
						.concat(HDMConstants.FILTER_QUERY)
						.concat(HDMConstants.EQUAL)
						.concat(HDMConstants.PRODUCT_TYPE)
						.concat(HDMConstants.COLON)
						.concat(HDMConstants.ITEM_BEAN);
			} else {
				core = catalogGroupCore;
				timeAllowed = HDMConstants.CATALOGGROUP_TIMEALLOWED;
			}

			StringBuilder stringBuilder = new StringBuilder();
			final String restURL = stringBuilder.append(HDMConstants.HTTP)
					.append(solsearchServerName).append(HDMConstants.COLON)
					.append(solsearchServerPort)
					.append(HDMConstants.FORWARD_SLASH)
					.append(HDMConstants.SOLR)
					.append(HDMConstants.FORWARD_SLASH).append(core)
					.append(HDMConstants.FORWARD_SLASH)
					.append(HDMConstants.SELECT)
					.append(HDMConstants.QUESTION_MARK).append(queryParams)
					.toString();

			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(
						Level.INFO,
						CLASSNAME,
						METHODNAME,
						"The restURL to get the total number of rows in solr {0}",
						new Object[] { restURL });
			}

			/**
			 * To get the total number of rows in solr for catalogEntry and
			 * catalogGroup
			 * 
			 * **/
			String recordSetTotal = null;
			final ClientConfig clientConfig = new ApacheHttpClientConfig();
			final RestClient restClient = new RestClient(clientConfig);
			final Resource restResource = restClient.resource(restURL);
			restResource.contentType(HDMConstants.PRODUCES_XML);
			restResource.accept(HDMConstants.PRODUCES_JSON);
			final ClientResponse clientResponse = restResource.get();
			final String responseEntity = clientResponse
					.getEntity(String.class);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			// Commenting these veracode fixes as solr will not be exposed
			// externally
			// Fix for Veracode violation fix - this to disable all the external
			// DTDs while parsing XML
			/*
			 * dbFactory.setFeature(
			 * "http://apache.org/xml/features/disallow-doctype-decl", true);
			 * dbFactory
			 * .setFeature("http://xml.org/sax/features/external-general-entities"
			 * , false); dbFactory.setFeature(
			 * "http://xml.org/sax/features/external-parameter-entities",
			 * false);
			 */

			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new StringReader(
					responseEntity)));
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName(HDMConstants.RESULT);
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					recordSetTotal = eElement
							.getAttribute(HDMConstants.RECORDSETTOTAL);
				}
			}

			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(
						Level.INFO,
						CLASSNAME,
						METHODNAME,
						" The restURL to get the total number of rows in solr {0} for the core {1}",
						new Object[] { recordSetTotal, core });
			}
			iSolrQuery.setRows(Integer.parseInt(recordSetTotal));
			iSolrQuery.setTimeAllowed(Integer.parseInt(timeAllowed));

		} catch (IOException | ParserConfigurationException | SAXException e) {
			LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME, e.getMessage(), e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
