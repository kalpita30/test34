package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;

/**
 * @author usrecomm
 *
 *         This Postprocessor is called in the search results page to remove the
 *         MARKET_BUYABLE_EXCLUDED facet from getting displayed in search
 *         results page.
 * 
 */
public class HDMSolrRESTPublishedOnlyFacetPostprocessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {
	private static final String CLASSNAME = HDMSolrRESTFacetQueryPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;

	public HDMSolrRESTPublishedOnlyFacetPostprocessor(String componentId) {
		this.iComponentId = componentId;
	}

	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryResponseObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}

		super.invoke(selectionCriteria, queryResponseObjects);

		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		String identifier_attr = HDMConstants.MARKET_BUYABLE_EXCLUDE;
		if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
			identifier_attr = HDMConstants.PRO_MARKET_BUYABLE_EXCLUDE;
		}

		final List<Map<String, Object>> facetViews = (LinkedList<Map<String, Object>>) iSearchResponseObject
				.getResponse().get(HDMConstants.FACET_VIEW);

		if (facetViews != null) {
			for (final Iterator<Map<String, Object>> iter = facetViews
					.iterator(); iter.hasNext();) {
				final Map<String, Object> facetView = iter.next();
				final Map<String, Object> extendedData = (Map<String, Object>) facetView
						.get(HDMConstants.EXTENDED_DATA);
				final String attridentifier = (extendedData != null && extendedData
						.containsKey(HDMConstants.ATTR_IDENTIFIER)) ? (String) extendedData
						.get(HDMConstants.ATTR_IDENTIFIER) : "";
				if (attridentifier.equals(identifier_attr)) {
					iter.remove();
					break;
				}
			}
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
