package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.foundation.server.services.rest.search.helper.HDMEntitlementHelper;
import com.hdm.commerce.search.utils.HDMUtils;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

/**
 * @author TCS
 *
 *         this pre processor class is having logic change in solr query to
 *         fetch statistics info for price field
 */
public class HDMSolrRESTFacetQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {
	private static final String CLASSNAME = HDMSolrRESTFacetQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTFacetQueryPreprocessor.class);
	private String iComponentId = null;

	/**
	 * @param componentId
	 */
	public HDMSolrRESTFacetQueryPreprocessor(String componentId) {
		iComponentId = componentId;
	}

	/**
	 * main method having business logic
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}

		super.invoke(selectionCriteria, queryRequestObjects);
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		String physicalStore_id = HDMUtils
				.getCookieValue(HDMConstants.PHYSICALSTORE_COOKIENAME + "_"
						+ storeId);
		final String iCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
		final String priceFacetField = HDMConstants.PRICE_FACET_NAME_PREFEIX
				.concat(iCurrencyCode);
		final List<String> pricePhysicalFieldList = HDMEntitlementHelper
				.applyFieldNamingPattern(priceFacetField, selectionCriteria);
		String[] fqList = iSolrQuery.getFilterQueries();
		String fqLists = iSolrQuery.getFields();
		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(
					Level.INFO,
					CLASSNAME,
					METHODNAME,
					"iCurrencyCode{0},priceFacetField:{1},pricePhysicalFieldList:{2},fqList:::{3}",
					new Object[] { iCurrencyCode, priceFacetField,
							Arrays.asList(pricePhysicalFieldList),
							Arrays.toString(fqList) });
		}
		if (fqList != null) {
			String[] aStringArray = new String[fqList.length];
			Integer count = 0;
			for (final String priceFacetQuery : fqList) {
				if ((priceFacetQuery.startsWith(priceFacetField))
						&& ((priceFacetQuery.indexOf(physicalStore_id) == -1))) {
					final String priceFacetFieldWithPhysicalStore = priceFacetField
							.concat(HDMConstants.UNDERSCORE).concat(
									physicalStore_id);
					String offerpriceFacet = priceFacetQuery.replaceFirst(
							priceFacetField, priceFacetFieldWithPhysicalStore);

					if (HDMConstants.PRO_STORE_ID.equals(storeId)) {
						final String proUserLevel = HDMUtils
								.getCookieValue(HDMConstants.CUSTOMERLEVEL_COOKIENAME);
						if (proUserLevel != null) {
							final String priceFacetFieldWithPhysicalStoreAndLevel = priceFacetFieldWithPhysicalStore
									.concat(HDMConstants.UNDERSCORE).concat(
											proUserLevel);
							offerpriceFacet = offerpriceFacet.replaceFirst(
									priceFacetFieldWithPhysicalStore,
									priceFacetFieldWithPhysicalStoreAndLevel);
						}
					}
					aStringArray[count] = offerpriceFacet;
				} else {
					aStringArray[count] = fqList[count];
				}
				count = count + 1;
			}
			iSolrQuery.setFilterQueries(aStringArray);
		}

		// sample query gets fired is given below
		// &facet.pivot={!stats=piv1}price_MXN&stats=true&stats.field={!tag=piv1)min=true
		// max=true}price_MXN

		iSolrQuery.set("stats", "true");
		int i = 1;
		for (Iterator localIterator = pricePhysicalFieldList.iterator(); localIterator
				.hasNext();) {
			final String pricePhysicalField = (String) localIterator.next();
			iSolrQuery.addFacetPivotField("{!stats=piv" + i + "}"
					+ pricePhysicalField);
			iSolrQuery.set("stats.field", "{!tag=piv" + i
					+ ")min=true max=true}" + pricePhysicalField);
			i++;
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}
}
