package com.hdm.commerce.foundation.server.services.rest.search.helper;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.context.content.ContentContext;
import com.ibm.commerce.datatype.AbstractFinderResult;
import com.ibm.commerce.datatype.CacheDependencyIdGenerator;
import com.ibm.commerce.datatype.CacheRule;
import com.ibm.commerce.datatype.CacheableDataGenerator;
import com.ibm.commerce.dynacache.commands.DistributedMapCache;
import com.ibm.commerce.foundation.client.util.oagis.RelationalExpression;
import com.ibm.commerce.foundation.common.exception.AbstractApplicationException;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.common.exception.ExceptionHelper;
import com.ibm.commerce.foundation.internal.server.services.context.RemoteContextServiceFactory;
import com.ibm.commerce.foundation.internal.server.services.registry.StoreObject;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchAttributeConfig;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.internal.server.services.search.util.HierarchyHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.SolrSearchWorkspaceHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.SpecialCharacterHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.StoreHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.JDBCQueryService;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.dataaccess.exception.QueryServiceApplicationException;
import com.ibm.commerce.foundation.server.services.rest.search.SearchCriteria;

/**
 * @author TCS
 * 
 *         this helper class is having OOB FacetHelper logic with required
 *         changes/customizations in the logic changes for category facet
 *         change1:in case of the request is from browsing pages - to fetch
 *         parent,current and all level of child categories at all times
 *         change2:in case of the request is from search pages - to fetch only
 *         department(top category) at initial search and not clicked category
 *         facet change3:in case of the request is from search pages - to fetch
 *         only 1st level of child categories on click of category facet in
 *         search result changes for promotion icons change1:2 methods were made
 *         as public
 */
public class HDMFacetHelper {
	private static final String CLASSNAME = HDMFacetHelper.class.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private static final String PROPERTY_USE_SKIP_FETCHING_FACET_VALUE_IMAGE_AND_SEQ = "skipFetchingFacetValueImageAndSequence";
	public static final String SRCHATTRPROP_PROPERTY_FACET = "facet";
	public static final String SRCHATTRPROP_PROPERTY_FACET_RANGE = "facet-range";
	public static final String SRCHATTRPROP_PROPERTY_FACET_CATEGORY = "facet-category";
	public static final String SRCHATTRPROP_PROPERTY_FACET_CLASSIC_ATTRIBUTE = "facet-classicAttribute";
	private static final String ATTRIBUTE_IS_NOT_FACETABLE = "0";
	private static final int DEFAULT_LOAD_FACTOR_NUMERATOR = 100;
	private static final int DEFAULT_LOAD_FACTOR_DENOMINATOR = 75;
	private static final int DEFAULT_MAX_FACET_FIELDS_TO_REQUEST = 50;
	private static final String CONFIG_KEY_MAX_FACET_FIELDS_TO_REQUEST = "maximumFacetFieldsToRequest";
	private static final String CACHE_NAME = CLASSNAME;
	private static final String LINE_SEPARATOR = System
			.getProperty("line.separator");
	private static final String SRCHATTR_TABLE = CacheRule.SRCHATTR_TABLE;
	private static final String SRCHATTRPROP_TABLE = CacheRule.SRCHATTRPROP_TABLE;
	private static final String FACET_TABLE = CacheRule.FACET_TABLE;
	private static final String FACETCATGRP_TABLE = CacheRule.FACETCATGRP_TABLE;
	private static final String FACETDESC_TABLE = CacheRule.FACETDESC_TABLE;
	private static final String STOREREL_TABLE = CacheRule.STOREREL_TABLE;
	private static final String ATTRDICTSRCHCONF_TABLE = CacheRule.ATTRDICTSRCHCONF_TABLE;
	private static final String ATTR_TABLE = CacheRule.ATTR_TABLE;
	private static final String ATTRDESC_TABLE = CacheRule.ATTRDESC_TABLE;
	private static final String ATTRVAL_TABLE = CacheRule.ATTRVAL_TABLE;
	private static final String QTYUNITDSC_TABLE = "QTYUNITDSC";
	private static final String ATTRVALDESC_TABLE = CacheRule.ATTRVALDESC_TABLE;
	private static final String STOREREL_KEY_STORE_ID_2 = "STORE_ID";
	private static final int SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION = 1;
	private static final int SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH = 2;
	private static final int SELECT_FACETABLE_INFORMATION_FOR_KEYWORD_SEARCH = 1;
	private static final int SELECT_FACETABLE_INFORMATION = 2;
	private static final String DEEP_SEARCH_STORE_CONFIG_FLAG = "ExpandedCategoryNavigation";
	private static final String GET_FACET_PROPERTIES = "getFacetProperties";
	private static final String COMPONENT_ID = "com.ibm.commerce.catalog";
	private static final String UNIQUE_ID = "uniqueId";
	private static final String PARENT_IDS = "parentIds";
	private static final String VALUE = "value";
	private static final String COUNT = "count";
	private static final String LABEL = "label";
	private static final String EXTENDED_DATA = "extendedData";
	private static final String ESCAPE_QUOTE = "\\\\\"";
	private static final String PARENT_CATGROUP_ID_SEARCH_FIELD = "parentCatgroup_id_search";
	private static final String PARENT_CATGROUP_ID_FACET_FIELD = "parentCatgroup_id_facet";
	public static final String PROPERTY_NAME_DISPLAY_LEAF_CATEGORIES_ONLY = "displayLeafCategoriesOnly";
	public static final String TRUE = "true";
	private static final String SELECT_FROM_STORECAT = "SELECT_CATALOG_ID_FROM_STORECAT";
	private static final String SRCHATTRID = "srchAttrId";
	private static final String SCHEMA = "$SCHEMA$";
	private static final String CATALOGENTRY = "CatalogEntry";
	private static final String EXCEPTION_LOGGER = "Exception call stack:{0}";

	private static final class MyGetFacetProperties implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyGetFacetProperties.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {
				new CacheRule(HDMFacetHelper.SRCHATTR_TABLE),
				new CacheRule(HDMFacetHelper.SRCHATTRPROP_TABLE),
				new CacheRule(HDMFacetHelper.FACET_TABLE),
				new CacheRule(HDMFacetHelper.FACETCATGRP_TABLE),
				new CacheRule(HDMFacetHelper.FACETDESC_TABLE),
				new CacheRule(HDMFacetHelper.ATTR_TABLE),
				new CacheRule(HDMFacetHelper.ATTRDESC_TABLE),
				new CacheRule(HDMFacetHelper.STOREREL_TABLE,
						new CacheRule.ColumnInfo[] { CacheRule.newColumnInfo(
								STOREREL_KEY_STORE_ID_2,
								CacheRule.newFinderArgIndex(5)) }) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			String strIndexName = (String) listFinderArgs.get(1);
			String strSearchProfile = (String) listFinderArgs.get(2);
			String strCurrencyCode = (String) listFinderArgs.get(3);
			String strLanguageId = (String) listFinderArgs.get(4);
			Integer nStoreId = (Integer) listFinderArgs.get(5);

			HashMap<String, Map<String, String>> facetPropertiesForIndexColumns = HDMFacetHelper
					.myGetFacetProperties(strIndexName, strSearchProfile,
							strCurrencyCode, strLanguageId, nStoreId);

			List listResult = new ArrayList(1);
			listResult.add(facetPropertiesForIndexColumns);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final MyGetFacetProperties MY_GET_FACET_PROPERTIES = new MyGetFacetProperties();
	private static final DistributedMapCache MY_GET_FACET_PROPERTIES_CACHE = myGetFacetPropertiesInitializeDistributedMapCache();

	private static final DistributedMapCache myGetFacetPropertiesInitializeDistributedMapCache() {
		DistributedMapCache dmapCache = new DistributedMapCache(CACHE_NAME);
		if (dmapCache.isDynamicCachingEnabled()) {
			CacheRule.enableDynamicInvalidationsForTable(SRCHATTR_TABLE,
					CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(SRCHATTRPROP_TABLE,
					CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(FACET_TABLE,
					CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(FACETCATGRP_TABLE,
					CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(FACETDESC_TABLE,
					CACHE_NAME);
			CacheRule
					.enableDynamicInvalidationsForTable(ATTR_TABLE, CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(ATTRDESC_TABLE,
					CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(STOREREL_TABLE,
					CACHE_NAME);
		}
		dmapCache.setCacheableDataGenerator(MY_GET_FACET_PROPERTIES);
		dmapCache.setDependencyIdGenerator(MY_GET_FACET_PROPERTIES);
		return dmapCache;
	}

	private static final class MyFilterFacets implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyFilterFacets.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {

				new CacheRule(HDMFacetHelper.SRCHATTR_TABLE),

				new CacheRule(HDMFacetHelper.SRCHATTRPROP_TABLE),

				new CacheRule(HDMFacetHelper.FACET_TABLE),

				new CacheRule(HDMFacetHelper.FACETCATGRP_TABLE),

				new CacheRule(HDMFacetHelper.FACETDESC_TABLE),

				new CacheRule(HDMFacetHelper.ATTR_TABLE),

				new CacheRule(HDMFacetHelper.ATTRDESC_TABLE),

				new CacheRule(HDMFacetHelper.STOREREL_TABLE,
						new CacheRule.ColumnInfo[] {

						CacheRule.newColumnInfo(STOREREL_KEY_STORE_ID_2,
								CacheRule.newFinderArgIndex(5)) }) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			String strIndexName = (String) listFinderArgs.get(1);
			String strSearchProfile = (String) listFinderArgs.get(2);
			String strCurrencyCode = (String) listFinderArgs.get(3);
			String strLanguageId = (String) listFinderArgs.get(4);
			Integer nStoreId = (Integer) listFinderArgs.get(5);
			String strCategoryId = (String) listFinderArgs.get(6);
			Long catalogId = (Long) listFinderArgs.get(7);
			Boolean bIncludeNonDisplayableFacets = (Boolean) listFinderArgs
					.get(8);

			HashMap<String, HashMap<String, String>> facetPropertiesForIndexColumns = null;

			Serializable[] key = { GET_FACET_PROPERTIES, strIndexName,
					strSearchProfile, strCurrencyCode, strLanguageId, nStoreId };

			Collection collResult = HDMFacetHelper.MY_GET_FACET_PROPERTIES_CACHE
					.getOrPut(key);
			if ((collResult != null) && (!collResult.isEmpty())) {
				facetPropertiesForIndexColumns = (HashMap) collResult
						.iterator().next();
			}
			if ((facetPropertiesForIndexColumns != null)
					&& (!facetPropertiesForIndexColumns.isEmpty())) {
				facetPropertiesForIndexColumns = (HashMap) facetPropertiesForIndexColumns
						.clone();

				Integer languageId = null;
				if ((strLanguageId != null) && (strLanguageId.length() != 0)) {
					languageId = Integer.valueOf(strLanguageId);
				}
				facetPropertiesForIndexColumns = HDMFacetHelper
						.determineFacetFieldsForCategory(strCategoryId,
								facetPropertiesForIndexColumns,
								strSearchProfile, nStoreId, languageId,
								catalogId);

				HashMap<String, HashMap<String, String>> filteredFacetPropertiesForIndexColumns = HDMFacetHelper
						.filterFacetsForCategory(nStoreId, strCategoryId,
								facetPropertiesForIndexColumns,
								bIncludeNonDisplayableFacets);

				int lenMap = filteredFacetPropertiesForIndexColumns == null ? 0
						: filteredFacetPropertiesForIndexColumns.size();
				facetPropertiesForIndexColumns = new HashMap(1 + lenMap
						* DEFAULT_LOAD_FACTOR_NUMERATOR
						/ DEFAULT_LOAD_FACTOR_DENOMINATOR);
				if (lenMap > 0) {
					facetPropertiesForIndexColumns
							.putAll(filteredFacetPropertiesForIndexColumns);
				}
			}
			List listResult = new ArrayList(1);
			listResult.add(facetPropertiesForIndexColumns);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final MyFilterFacets MY_FILTER_FACETS = new MyFilterFacets();

	private static final class MyFilterFacetsByKeyword implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyFilterFacetsByKeyword.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {

				new CacheRule(HDMFacetHelper.SRCHATTR_TABLE),

				new CacheRule(HDMFacetHelper.SRCHATTRPROP_TABLE),

				new CacheRule(HDMFacetHelper.FACET_TABLE),

				new CacheRule(HDMFacetHelper.FACETDESC_TABLE),

				new CacheRule(HDMFacetHelper.ATTR_TABLE),

				new CacheRule(HDMFacetHelper.ATTRDESC_TABLE),

				new CacheRule(HDMFacetHelper.STOREREL_TABLE,
						new CacheRule.ColumnInfo[] {

						CacheRule.newColumnInfo(STOREREL_KEY_STORE_ID_2,
								CacheRule.newFinderArgIndex(4)) }) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			String strIndexName = (String) listFinderArgs.get(1);
			String strLanguageId = (String) listFinderArgs.get(2);
			String strCurrencyCode = (String) listFinderArgs.get(3);
			Integer nStoreId = (Integer) listFinderArgs.get(4);
			String strSearchProfile = (String) listFinderArgs.get(5);
			String strCategoryId = (String) listFinderArgs.get(6);

			HashMap<String, Map<String, String>> facetPropertiesForIndexColumns = null;
			Map queryParameters = new HashMap(5);

			List storeList = HDMFacetHelper.getStoreIds(nStoreId);
			queryParameters.put(STORE_LIST_QUERY_PARAMETER, storeList);

			queryParameters.put(INDEX_TYPE_QUERY_PARAMETER,
					Collections.singletonList(strIndexName));

			queryParameters.put(LANGUAGE_ID_QUERY_PARAMETER,
					Collections.singletonList(strLanguageId));

			facetPropertiesForIndexColumns = HDMFacetHelper.fetchFacetableInfo(
					1, queryParameters, facetPropertiesForIndexColumns);

			String[] arrstPriceFieldNameAndPrefix = HDMFacetHelper
					.getPriceFieldNameAndPrefix(strSearchProfile,
							strCurrencyCode);

			String priceFieldNamePrefix = arrstPriceFieldNameAndPrefix[0];

			String priceFieldFullNameWithCurrency = arrstPriceFieldNameAndPrefix[1];
			Object[] arrFacetOrFacetQuery = facetPropertiesForIndexColumns
					.keySet().toArray();
			for (int idx = 0; idx < arrFacetOrFacetQuery.length; idx++) {
				String strFacetOrFacetQuery = (String) arrFacetOrFacetQuery[idx];
				if (strFacetOrFacetQuery.startsWith(priceFieldNamePrefix)
						&& !strFacetOrFacetQuery
								.startsWith(priceFieldFullNameWithCurrency)) {
					facetPropertiesForIndexColumns.remove(strFacetOrFacetQuery);
				}
			}
			if ((strCategoryId != null) && (strCategoryId.length() > 0)) {
				HashMap<String, HashMap<String, String>> filteredFacetPropertiesForIndexColumns = HDMFacetHelper
						.filterFacetsForCategory(nStoreId, strCategoryId,
								(HashMap) facetPropertiesForIndexColumns
										.clone(), Boolean.valueOf(true));
				facetPropertiesForIndexColumns = (HashMap) filteredFacetPropertiesForIndexColumns
						.clone();
			}
			int lenMap = facetPropertiesForIndexColumns == null ? 0
					: facetPropertiesForIndexColumns.size();
			HashMap<String, Map<String, String>> resultFacetPropertiesForIndexColumns = new HashMap(
					1 + lenMap * DEFAULT_LOAD_FACTOR_NUMERATOR
							/ DEFAULT_LOAD_FACTOR_DENOMINATOR);
			if (lenMap > 0) {
				resultFacetPropertiesForIndexColumns
						.putAll(facetPropertiesForIndexColumns);
			}
			List listResult = new ArrayList(1);
			listResult.add(resultFacetPropertiesForIndexColumns);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final MyFilterFacetsByKeyword MY_FILTER_FACETS_BY_KEYWORD = new MyFilterFacetsByKeyword();

	private static final DistributedMapCache myFilterFacetsByKeywordInitializeDistributedMapCache() {
		final String KEYWORD_CACHE_NAME = CACHE_NAME + ".keyword";
		DistributedMapCache dmapCache = new DistributedMapCache(
				KEYWORD_CACHE_NAME);
		if (dmapCache.isDynamicCachingEnabled()) {
			CacheRule.enableDynamicInvalidationsForTable(SRCHATTR_TABLE,
					KEYWORD_CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(SRCHATTRPROP_TABLE,
					KEYWORD_CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(FACET_TABLE,
					KEYWORD_CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(FACETDESC_TABLE,
					KEYWORD_CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(ATTR_TABLE,
					KEYWORD_CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(ATTRDESC_TABLE,
					KEYWORD_CACHE_NAME);
			CacheRule.enableDynamicInvalidationsForTable(STOREREL_TABLE,
					KEYWORD_CACHE_NAME);
		}
		dmapCache.setCacheableDataGenerator(MY_FILTER_FACETS_BY_KEYWORD);
		dmapCache.setDependencyIdGenerator(MY_FILTER_FACETS_BY_KEYWORD);
		return dmapCache;
	}

	private static final class MyGetFacetSearchColumns implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyGetFacetSearchColumns.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {

				new CacheRule(HDMFacetHelper.SRCHATTRPROP_TABLE),

				new CacheRule(HDMFacetHelper.FACET_TABLE),

				new CacheRule(HDMFacetHelper.ATTR_TABLE),
				new CacheRule(HDMFacetHelper.STOREREL_TABLE,
						new CacheRule.ColumnInfo[] {

						CacheRule.newColumnInfo(STOREREL_KEY_STORE_ID_2,
								CacheRule.newFinderArgIndex(1)) }) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			Integer storeId = (Integer) listFinderArgs.get(1);
			ArrayList<String> searchIndexColumns = (ArrayList) listFinderArgs
					.get(2);

			Map<String, Map<String, String>> facetConfiguration = new HashMap();

			Map queryParameters = new HashMap(3);

			queryParameters.put(STORE_LIST_QUERY_PARAMETER,
					HDMFacetHelper.getStoreIds(storeId));

			int limit = 950;
			if (searchIndexColumns.size() <= limit) {
				queryParameters.put(PROPERTY_VALUE_LIST_QUERY_PARAMETER,
						searchIndexColumns);
				facetConfiguration = HDMFacetHelper.fetchFacetConfigurations(
						queryParameters, facetConfiguration);
			} else {
				String[] searchIndexColumnsCopy = searchIndexColumns
						.toArray(new String[searchIndexColumns.size()]);
				List searchColumnsSubset = new ArrayList();
				for (int i = 0; i < searchIndexColumnsCopy.length; i++) {
					String searchColumn = searchIndexColumnsCopy[i];
					searchColumnsSubset.add(searchColumn);
					if ((i > 0) && (i % limit == 0)) {
						queryParameters.put(
								PROPERTY_VALUE_LIST_QUERY_PARAMETER,
								searchColumnsSubset);
						if (HDMFacetHelper.LOGGER.isLoggable(Level.FINER)) {
							HDMFacetHelper.LOGGER.logp(Level.FINER,
									HDMFacetHelper.CLASSNAME, METHODNAME,
									"propertyValueList: {0}",
									searchColumnsSubset);
						}
						facetConfiguration = HDMFacetHelper
								.fetchFacetConfigurations(queryParameters,
										facetConfiguration);

						searchColumnsSubset = new ArrayList();
					}
				}
				if (!searchColumnsSubset.isEmpty()) {
					queryParameters.put(PROPERTY_VALUE_LIST_QUERY_PARAMETER,
							searchColumnsSubset);
					if (HDMFacetHelper.LOGGER.isLoggable(Level.FINER)) {
						HDMFacetHelper.LOGGER.logp(Level.FINER,
								HDMFacetHelper.CLASSNAME, METHODNAME,
								"propertyValue: {0}", searchColumnsSubset);
					}
					facetConfiguration = HDMFacetHelper
							.fetchFacetConfigurations(queryParameters,
									facetConfiguration);
				}
			}
			List listResult = new ArrayList(1);
			listResult.add(facetConfiguration);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final class MyGetAttrValueAndSeq implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyGetAttrValueAndSeq.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {
				new CacheRule(HDMFacetHelper.ATTRDICTSRCHCONF_TABLE),
				new CacheRule(HDMFacetHelper.ATTR_TABLE),
				new CacheRule(HDMFacetHelper.ATTRDESC_TABLE),
				new CacheRule(HDMFacetHelper.ATTRVAL_TABLE),
				new CacheRule(QTYUNITDSC_TABLE),
				new CacheRule(HDMFacetHelper.ATTRVALDESC_TABLE),
				new CacheRule(HDMFacetHelper.STOREREL_TABLE,
						new CacheRule.ColumnInfo[] { CacheRule.newColumnInfo(
								STOREREL_KEY_STORE_ID_2,
								CacheRule.newFinderArgIndex(1)) }) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			Integer storeId = (Integer) listFinderArgs.get(1);
			Integer languageId = (Integer) listFinderArgs.get(2);
			Long catalogId = (Long) listFinderArgs.get(3);

			String srchFieldName = (String) listFinderArgs.get(4);
			Map queryParameters = new HashMap();
			List languageIdList = new ArrayList();
			languageIdList.add(languageId);
			queryParameters.put(LANGUAGE_ID_QUERY_PARAMETER, languageIdList);
			queryParameters.put(STORE_LIST_QUERY_PARAMETER,
					HDMFacetHelper.getStoreIds(storeId));

			List catalogIdList = new ArrayList();
			catalogIdList.add(catalogId);
			queryParameters.put(CATALOG_ID_QUERY_PARAMETER, catalogIdList);

			List srchFieldNameList = new ArrayList();
			srchFieldNameList.add(srchFieldName);
			queryParameters
					.put(SEARCH_FIELD_QUERY_PARAMETER, srchFieldNameList);

			Map<String, List> facetValueImageAndSequenceInfo = HDMFacetHelper
					.fetchAttributeImageAndSeq(queryParameters);
			List listResult = new ArrayList(1);
			listResult.add(facetValueImageAndSequenceInfo);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final class MyGetSortedFacetsForNavigation implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyGetSortedFacetsForNavigation.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {
				new CacheRule(HDMFacetHelper.FACET_TABLE),
				new CacheRule(HDMFacetHelper.FACETCATGRP_TABLE),
				new CacheRule(HDMFacetHelper.ATTR_TABLE),
				new CacheRule(HDMFacetHelper.STOREREL_TABLE,
						new CacheRule.ColumnInfo[] { CacheRule.newColumnInfo(
								STOREREL_KEY_STORE_ID_2,
								CacheRule.newFinderArgIndex(1)) }) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			Integer storeId = (Integer) listFinderArgs.get(1);
			String category = (String) listFinderArgs.get(2);
			TreeSet<Long> aFacetableAttributesToSort = (TreeSet) listFinderArgs
					.get(3);

			List<Long> lSortedFacetableAttributeIds = new ArrayList(0);
			if (aFacetableAttributesToSort != null) {
				try {
					lSortedFacetableAttributeIds = HDMFacetHelper
							.sortFacetsForNavigation(category,
									aFacetableAttributesToSort, storeId);
				} catch (Exception e) {
					String strExceptionMessage = ExceptionHelper
							.convertStackTraceToString(e);
					if (HDMFacetHelper.LOGGER.isLoggable(Level.SEVERE)) {
						HDMFacetHelper.LOGGER.logp(Level.SEVERE,
								HDMFacetHelper.CLASSNAME, METHODNAME,
								EXCEPTION_LOGGER, strExceptionMessage);
					}
					throw e;
				}
			}
			List listResult = new ArrayList(1);
			listResult.add(lSortedFacetableAttributeIds);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final class MyGetSortedFacetsForKeywordSearch implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyGetSortedFacetsForKeywordSearch.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {
				new CacheRule(HDMFacetHelper.FACET_TABLE),
				new CacheRule(HDMFacetHelper.ATTR_TABLE) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			TreeSet<Long> aFacetableAttributesToSort = (TreeSet) listFinderArgs
					.get(1);

			List<Long> lSortedFacetableAttributeIds = new ArrayList(0);
			if (aFacetableAttributesToSort != null) {
				try {
					lSortedFacetableAttributeIds = HDMFacetHelper
							.sortFacetsForKeywordSearch(aFacetableAttributesToSort);
				} catch (Exception e) {
					String strExceptionMessage = ExceptionHelper
							.convertStackTraceToString(e);
					if (HDMFacetHelper.LOGGER.isLoggable(Level.SEVERE)) {
						HDMFacetHelper.LOGGER.logp(Level.SEVERE,
								HDMFacetHelper.CLASSNAME, METHODNAME,
								EXCEPTION_LOGGER, strExceptionMessage);
					}
					throw e;
				}
			}
			List listResult = new ArrayList(1);
			listResult.add(lSortedFacetableAttributeIds);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final String STORECGRP_TABLE = CacheRule.STORECGRP_TABLE;
	private static final String STORECGRP_KEY_CATGROUP_ID_1 = CacheRule.STORECGRP_KEY_COLUMN_CATGROUP_ID_1;
	private static final String GET_CATEGORY_STORE_ID = "getCategoryStoreId";

	private static final class MyGetCategoryStoreID implements
			CacheableDataGenerator, CacheDependencyIdGenerator {
		private static final String MY_CLASS_NAME = MyGetCategoryStoreID.class
				.getName();
		private static final CacheRule[] DEPENDENCIES = {

		new CacheRule(HDMFacetHelper.STORECGRP_TABLE,
				new CacheRule.ColumnInfo[] { CacheRule.newColumnInfo(
						HDMFacetHelper.STORECGRP_KEY_CATGROUP_ID_1,
						CacheRule.newFinderArgIndex(1)) }) };

		public Collection generateCacheableData(
				AbstractFinderResult aFinderResult) throws Exception {
			final String METHODNAME = "generateCacheableData";
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.entering(MY_CLASS_NAME, METHODNAME);
			}
			List listFinderArgs = aFinderResult.getFinderArgs();
			String categoryId = (String) listFinderArgs.get(1);

			List listResult = new ArrayList(1);

			String storeId = HDMFacetHelper.fetchStoreIdForCategory(categoryId);

			listResult.add(storeId);
			if (LoggingHelper.isEntryExitTraceEnabled(HDMFacetHelper.LOGGER)) {
				HDMFacetHelper.LOGGER.exiting(MY_CLASS_NAME, METHODNAME,
						listResult);
			}
			return listResult;
		}

		public Collection generateCacheDependencyIds(
				AbstractFinderResult aFinderResult) throws Exception {
			aFinderResult.setDependencyIdGenerationRules(DEPENDENCIES);
			return null;
		}
	}

	private static final MyGetCategoryStoreID MY_GET_CATEGORY_STORE_ID = new MyGetCategoryStoreID();
	private static final DistributedMapCache MY_GET_CATEGORY_STORE_ID_CACHE = myGetCategoryStoreIDInitializeDistributedMapCache();
	private static final String INDEX_TYPE_QUERY_PARAMETER = "indexType";
	private static final String STORE_LIST_QUERY_PARAMETER = "storeList";
	private static final String CATALOG_ID_QUERY_PARAMETER = "catalogId";
	private static final String SEARCH_FIELD_QUERY_PARAMETER = "searchFieldName";
	private static final String LANGUAGE_ID_QUERY_PARAMETER = "languageId";
	private static final String PROPERTY_VALUE_LIST_QUERY_PARAMETER = "propertyValueList";
	private static final String PROPERTY_VALUE_QUERY_PARAMETER = "propertyValue";
	private static final String STR_FACETABLE = "FACETABLE";
	private static final String STR_FACET_ID = "FACET_ID";
	private static final String STR_ATTR_ID = "ATTR_ID";
	private static final String STR_SRCHATTR_ID = "SRCHATTR_ID";
	private static final String STR_PROPERTYNAME = "PROPERTYNAME";
	private static final String STR_PROPERTYVALUE = "PROPERTYVALUE";
	private static final String STR_SRCHATTRIDENTIFIER = "SRCHATTRIDENTIFIER";
	private static final String STR_SELECTION = "SELECTION";
	private static final String STR_KEYWORD_SEARCH = "KEYWORD_SEARCH";
	private static final String STR_ZERO_DISPLAY = "ZERO_DISPLAY";
	private static final String STR_MAX_DISPLAY = "MAX_DISPLAY";
	private static final String STR_SEQUENCE = "SEQUENCE";
	private static final String STR_FFIELD1 = "FFIELD1";
	private static final String STR_FFIELD2 = "FFIELD2";
	private static final String STR_FFIELD3 = "FFIELD3";
	private static final String STR_FNAME = "FNAME";
	private static final String STR_FDESC = "FDESC";
	private static final String STR_FDESCFIELD1 = "FDESCFIELD1";
	private static final String STR_FDESCFIELD2 = "FDESCFIELD2";
	private static final String STR_FDESCFIELD3 = "FDESCFIELD3";
	private static final String STR_STOREENT_ID = "STOREENT_ID";
	private static final String STR_ATTRIDENTIFIER = "ATTRIDENTIFIER";
	private static final String STR_ATTRSEQUENCE = "ATTRSEQUENCE";
	private static final String STR_ATTRNAME = "ATTRNAME";
	private static final String STR_ATTRDESC = "ATTRDESC";
	private static final String STR_SORT_ORDER = "SORT_ORDER";
	private static final String STR_GROUP_ID = "GROUP_ID";
	private static final String UNDERSCORE = "_";
	public static final String FACET_PROPERTY_FACET_ID = "facet_id";
	public static final String FACET_PROPERTY_ATTRIBUTE_ID = "attr_id";
	public static final String FACET_PROPERTY_SRCHATTR_ID = "srchattr_id";
	public static final String FACET_PROPERTY_PROPERTYNAME = "propertyname";
	public static final String FACET_PROPERTY_PROPERTYVALUE = "propertyvalue";
	public static final String FACET_PROPERTY_SRCHATTRIDENTIFIER = "srchattridentifier";
	public static final String FACET_PROPERTY_SELECTION_TYPE = "selection";
	public static final String FACET_PROPERTY_FACET_VALUE_SORT_ORDER = "sortorder";
	public static final String FACET_PROPERTY_FACET_VALUE_GROUP_ID = "group_id";
	public static final String FACET_PROPERTY_KEYWORD_SEARCH = "keyword_search";
	public static final String FACET_PROPERTY_ZERO_DISPLAY = "zero_display";
	public static final String FACET_PROPERTY_MAX_DISPLAY = "max_display";
	public static final String FACET_PROPERTY_FIELD1 = "ffield1";
	public static final String FACET_PROPERTY_FIELD2 = "ffield2";
	public static final String FACET_PROPERTY_FIELD3 = "ffield3";
	public static final String FACET_STOREENT_ID = "storeent_id";
	public static final String FACET_PROPERTY_ATTRIBUTE_IDENTIFIER = "attridentifier";
	public static final String FACET_PROPERTY_ATTRIBUTE_SEQUENCE = "attrsequence";
	public static final String FACET_PROPERTY_ATTRIBUTE_NAME = "attrname";
	public static final String FACET_PROPERTY_ATTRIBUTE_DESCRIPTION = "attrdesc";
	public static final String FACET_PROPERTY_FACET_NAME = "fname";
	public static final String FACET_PROPERTY_FACET_DESCRIPTION = "fdesc";
	public static final String FACET_PROPERTY_FACET_DESCRIPTION_FIELD1 = "fdescfield1";
	public static final String FACET_PROPERTY_FACET_DESCRIPTION_FIELD2 = "fdescfield2";
	public static final String FACET_PROPERTY_FACET_DESCRIPTION_FIELD3 = "fdescfield3";
	public static final String FACET_PROPERTY_FACET_DISPLAYABLE = "displayable";
	public static final String FACET_PROPERTY_FACET_SEQUENCE = "displaySequence";
	public static final String SORT_BY_COUNT = "0";
	public static final String SORT_ALPHANUMERICALLY = "1";
	public static final String SORT_BY_SEQUENCE = "2";
	private static final String FEATURED_FACET_VALUE = "1";

	/**
	 * @return dmapCache
	 */
	private static final DistributedMapCache myGetCategoryStoreIDInitializeDistributedMapCache() {
		final String CATSTORE_CACHE_NAME = CACHE_NAME + ".catstore";
		DistributedMapCache dmapCache = new DistributedMapCache(
				CATSTORE_CACHE_NAME);
		if (dmapCache.isDynamicCachingEnabled()) {
			CacheRule.enableDynamicInvalidationsForTable(STORECGRP_TABLE,
					CATSTORE_CACHE_NAME);
		}
		dmapCache.setCacheableDataGenerator(MY_GET_CATEGORY_STORE_ID);
		dmapCache.setDependencyIdGenerator(MY_GET_CATEGORY_STORE_ID);
		return dmapCache;
	}

	/**
	 * @param astrIndexName
	 * @param astrSearchProfile
	 * @param astrCurrencyCode
	 * @param astrLanguageId
	 * @param anStoreId
	 * @return facetPropertiesForIndexColumns
	 * @throws Exception
	 */
	private static final HashMap<String, Map<String, String>> myGetFacetProperties(
			String astrIndexName, String astrSearchProfile,
			String astrCurrencyCode, String astrLanguageId, Integer anStoreId)
			throws Exception {
		HashMap<String, Map<String, String>> facetPropertiesForIndexColumns = null;
		List<String> searchColumnsToLookupConfigFor = getAllFacetColumns(
				astrIndexName, astrSearchProfile, astrCurrencyCode, anStoreId);
		if (!searchColumnsToLookupConfigFor.isEmpty()) {
			facetPropertiesForIndexColumns = getFacetPropertiesForIndexColumns(
					CATALOGENTRY, searchColumnsToLookupConfigFor,
					astrLanguageId, anStoreId);
		}
		return facetPropertiesForIndexColumns == null ? new HashMap(1)
				: facetPropertiesForIndexColumns;
	}

	/**
	 * @param storeId
	 * @param categoryId
	 * @param facetPropertiesForIndexColumns
	 * @param includeNonDisplayableFacets
	 * @return mapResult
	 * @throws Exception
	 */
	private static HashMap<String, HashMap<String, String>> filterFacetsForCategory(
			Integer storeId,
			String categoryId,
			HashMap<String, HashMap<String, String>> facetPropertiesForIndexColumns,
			Boolean includeNonDisplayableFacets) throws Exception {
		final String METHODNAME = "filterFacetsForCategory";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { storeId,
					categoryId, facetPropertiesForIndexColumns,
					includeNonDisplayableFacets });
		}
		HashMap<String, HashMap<String, String>> mapResult = facetPropertiesForIndexColumns;
		if ((facetPropertiesForIndexColumns != null)
				&& (!facetPropertiesForIndexColumns.isEmpty())) {
			JDBCQueryService service = new JDBCQueryService(COMPONENT_ID);

			Map queryParameters = new HashMap(5);

			List storeList = getStoreIds(storeId);
			queryParameters.put(STORE_LIST_QUERY_PARAMETER, storeList);

			List categoryList = new ArrayList(1);
			categoryList.add(Long.valueOf(categoryId));
			final String CATEGORY_ID = "categoryId";
			queryParameters.put(CATEGORY_ID, categoryList);

			List<HashMap> results = null;
			SolrSearchConfigurationRegistry.getInstance();
			String readSchema = SolrSearchConfigurationRegistry.getReadSchema();
			if ((readSchema != null) && (readSchema.length() > 0)) {
				ArrayList list = new ArrayList();
				list.add(readSchema);
				queryParameters.put(SCHEMA, list);
				results = service.executeQuery(
						"SELECT_CATEGORY_FACET_OVERRIDE_WORKSPACE",
						queryParameters);
			} else {
				results = service.executeQuery(
						"SELECT_CATEGORY_FACET_OVERRIDE", queryParameters);
			}
			Iterator<HashMap> recordIterator = results.iterator();
			Map<String, String> displayMap = new HashMap();
			Map<String, String> sequenceMap = new HashMap();

			final String ZERO = SORT_BY_COUNT;
			String sequence;
			while (recordIterator.hasNext()) {
				HashMap<String, String> record = (HashMap) recordIterator
						.next();
				sequence = String.valueOf(record.get(STR_SEQUENCE));
				String displayable = String.valueOf(record.get("DISPLAYABLE"));
				String propertyvalue = getColumnValue(record, STR_PROPERTYVALUE);
				String catgroupId = String.valueOf(record.get("CATGROUP_ID"));
				if (propertyvalue != null) {
					HashMap config = mapResult.get(propertyvalue);
					if (config != null) {
						if (catgroupId.equals(categoryId)
								|| (catgroupId.equals(ZERO))
								&& (!displayMap.containsKey(propertyvalue))) {
							displayMap.put(propertyvalue, displayable);
						}
						sequenceMap.put(propertyvalue, sequence);
					}
				}
			}
			for (String propertyvalue : displayMap.keySet()) {
				if ((!includeNonDisplayableFacets.booleanValue())
						&& ((displayMap.get(propertyvalue)).equals(ZERO))) {
					mapResult.remove(propertyvalue);
				} else {
					HashMap config = mapResult.get(propertyvalue);
					config = (HashMap) config.clone();
					mapResult.put(propertyvalue, config);
					config.put(FACET_PROPERTY_FACET_DISPLAYABLE,
							displayMap.get(propertyvalue));
					if (!"null".equals(sequenceMap.get(propertyvalue))) {
						config.put(FACET_PROPERTY_FACET_SEQUENCE,
								String.valueOf(sequenceMap.get(propertyvalue)));
					}
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, mapResult);
		}
		return mapResult;
	}

	/**
	 * @param indexName
	 * @param searchColumnsToLookupConfigFor
	 * @param languageId
	 * @param storeId
	 * @return facetPropertiesForIndexColumns
	 * @throws Exception
	 */
	public static HashMap<String, Map<String, String>> getFacetPropertiesForIndexColumns(
			String indexName, List searchColumnsToLookupConfigFor,
			String languageId, Integer storeId) throws Exception {
		final String METHODNAME = "getFacetPropertiesForIndexColumns(String, List, String, Integer)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { indexName,
					searchColumnsToLookupConfigFor, languageId, storeId });
		}
		HashMap<String, Map<String, String>> facetPropertiesForIndexColumns = new HashMap(
				1 + searchColumnsToLookupConfigFor.size()
						* DEFAULT_LOAD_FACTOR_NUMERATOR
						/ DEFAULT_LOAD_FACTOR_DENOMINATOR);
		if (!searchColumnsToLookupConfigFor.isEmpty()) {
			if ((languageId == null) || (languageId.length() < 1)) {
				languageId = RemoteContextServiceFactory
						.getContextAsString("com.ibm.commerce.context.globalization.language");
			}
			if (languageId != null) {
				Map queryParameters = new HashMap(7);

				List indexNameList = new ArrayList(1);
				indexNameList.add(indexName);
				queryParameters.put(INDEX_TYPE_QUERY_PARAMETER, indexNameList);
				if (LOGGER.isLoggable(Level.FINER)) {
					LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
							"indexType:{0} ", Arrays.asList(indexNameList));
				}
				List languageList = new ArrayList(1);
				languageList.add(languageId);
				queryParameters.put(LANGUAGE_ID_QUERY_PARAMETER, languageList);
				if (LOGGER.isLoggable(Level.FINER)) {
					LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
							"languageId: {0}", Arrays.asList(languageList));
				}
				List storeIdList = getStoreIds(storeId);

				queryParameters.put(STORE_LIST_QUERY_PARAMETER, storeIdList);
				if (LOGGER.isLoggable(Level.FINER)) {
					LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
							"storeList:{0} ", storeIdList);
				}
				int limit = 950;
				if (searchColumnsToLookupConfigFor.size() <= limit) {
					queryParameters.put(PROPERTY_VALUE_QUERY_PARAMETER,
							searchColumnsToLookupConfigFor);
					if (LOGGER.isLoggable(Level.FINER)) {
						LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
								"propertyValue: {0}",
								Arrays.asList(searchColumnsToLookupConfigFor));
					}
					facetPropertiesForIndexColumns = fetchFacetableInfo(2,
							queryParameters, facetPropertiesForIndexColumns);
				} else {
					String[] searchIndexColumns = (String[]) searchColumnsToLookupConfigFor
							.toArray(new String[searchColumnsToLookupConfigFor
									.size()]);

					List searchColumnsSubset = new ArrayList();
					for (int i = 0; i < searchIndexColumns.length; i++) {
						String searchColumn = searchIndexColumns[i];

						searchColumnsSubset.add(searchColumn);
						if ((i > 0) && (i % limit == 0)) {
							queryParameters.put(PROPERTY_VALUE_QUERY_PARAMETER,
									searchColumnsSubset);
							if (LOGGER.isLoggable(Level.FINER)) {
								LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
										"propertyValue:{0} ",
										Arrays.asList(searchColumnsSubset));
							}
							facetPropertiesForIndexColumns = fetchFacetableInfo(
									2, queryParameters,
									facetPropertiesForIndexColumns);

							searchColumnsSubset = new ArrayList();
						}
					}
					if (!searchColumnsSubset.isEmpty()) {
						queryParameters.put(PROPERTY_VALUE_QUERY_PARAMETER,
								searchColumnsSubset);
						if (LOGGER.isLoggable(Level.FINER)) {
							LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
									"propertyValue: {0} ",
									Arrays.asList(searchColumnsSubset));
						}
						facetPropertiesForIndexColumns = fetchFacetableInfo(2,
								queryParameters, facetPropertiesForIndexColumns);
					}
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
		return facetPropertiesForIndexColumns;
	}

	/**
	 * @param queryCode
	 * @param aQueryParameters
	 * @param aFacetPropertiesForIndexColumns
	 * @return aFacetPropertiesForIndexColumns
	 * @throws SQLException
	 * @throws QueryServiceApplicationException
	 */
	private static HashMap<String, Map<String, String>> fetchFacetableInfo(
			int queryCode, Map aQueryParameters,
			HashMap<String, Map<String, String>> aFacetPropertiesForIndexColumns)
			throws SQLException, QueryServiceApplicationException {
		final String METHODNAME = "fetchFacetableInfo(int, Map, HashMap<String, Map<String, String>>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					new Object[] { Integer.valueOf(queryCode),
							aQueryParameters, aFacetPropertiesForIndexColumns });
		}
		JDBCQueryService queryService = new JDBCQueryService(COMPONENT_ID);
		List<HashMap> results = null;
		SolrSearchConfigurationRegistry.getInstance();
		String readSchema = SolrSearchConfigurationRegistry.getReadSchema();
		if ((readSchema != null) && (readSchema.length() > 0)) {
			ArrayList list = new ArrayList();
			list.add(readSchema);
			aQueryParameters.put(SCHEMA, list);
		}
		switch (queryCode) {
		case 1:
			results = queryService
					.executeQuery(
							"SELECT_FACETABLE_INFORMATION_FOR_KEYWORD_SEARCH_WORKSPACE",
							aQueryParameters);
			break;
		case 2:
			results = queryService.executeQuery(
					"SELECT_FACETABLE_INFORMATION_WORKSPACE", aQueryParameters);
			break;
		default:
			results = queryService
					.executeQuery(
							"SELECT_FACETABLE_INFORMATION_FOR_KEYWORD_SEARCH_WORKSPACE",
							aQueryParameters);

			switch (queryCode) {
			case SELECT_FACETABLE_INFORMATION_FOR_KEYWORD_SEARCH:
				results = queryService.executeQuery(
						"SELECT_FACETABLE_INFORMATION_FOR_KEYWORD_SEARCH",
						aQueryParameters);
				break;
			case SELECT_FACETABLE_INFORMATION:
				results = queryService.executeQuery(
						"SELECT_FACETABLE_INFORMATION", aQueryParameters);
				break;
			default:
				results = queryService.executeQuery(
						"SELECT_FACETABLE_INFORMATION_FOR_KEYWORD_SEARCH",
						aQueryParameters);
			}
			break;
		}
		if (aFacetPropertiesForIndexColumns == null) {
			aFacetPropertiesForIndexColumns = new HashMap(1 + results.size()
					* DEFAULT_LOAD_FACTOR_NUMERATOR
					/ DEFAULT_LOAD_FACTOR_DENOMINATOR);
		}
		Iterator<HashMap> recordIterator = results.iterator();
		while (recordIterator.hasNext()) {
			HashMap<String, String> record = (HashMap) recordIterator.next();

			String facetable = null;
			String facetID = null;
			String attrID = null;
			String srchattrId = null;
			String propertyname = null;
			String propertyvalue = null;
			String srchattridentifier = null;
			String facetSelection = null;
			String facetKS = null;
			String facetZeroDisplay = null;
			String facetMaxDisplay = null;
			String facetSequence = null;
			String facetField1 = null;
			String facetField2 = null;
			String facetField3 = null;
			String fname = null;
			String fdesc = null;
			String fdescfield1 = null;
			String fdescfield2 = null;
			String fdescfield3 = null;
			String facetStore = null;
			String attrIdentifier = null;
			String attrSequence = null;
			String attrName = null;
			String attrDesc = null;
			String sortOrder = null;
			String groupId = null;

			propertyvalue = getColumnValue(record, STR_PROPERTYVALUE);
			if (propertyvalue != null) {
				facetable = getColumnValue(record, STR_FACETABLE);
				if ((facetable == null)
						|| (!facetable.equals(ATTRIBUTE_IS_NOT_FACETABLE))) {
					facetID = getColumnValue(record, STR_FACET_ID);
					attrID = getColumnValue(record, STR_ATTR_ID);
					srchattrId = getColumnValue(record, STR_SRCHATTR_ID);
					propertyname = getColumnValue(record, STR_PROPERTYNAME);

					srchattridentifier = getColumnValue(record,
							STR_SRCHATTRIDENTIFIER);
					facetSelection = getColumnValue(record, STR_SELECTION);
					facetKS = getColumnValue(record, STR_KEYWORD_SEARCH);
					facetZeroDisplay = getColumnValue(record, STR_ZERO_DISPLAY);
					facetMaxDisplay = getColumnValue(record, STR_MAX_DISPLAY);
					facetSequence = getColumnValue(record, STR_SEQUENCE);
					facetField1 = getColumnValue(record, STR_FFIELD1);
					facetField2 = getColumnValue(record, STR_FFIELD2);
					facetField3 = getColumnValue(record, STR_FFIELD3);
					fname = getColumnValue(record, STR_FNAME);
					fdesc = getColumnValue(record, STR_FDESC);
					fdescfield1 = getColumnValue(record, STR_FDESCFIELD1);
					fdescfield2 = getColumnValue(record, STR_FDESCFIELD2);
					fdescfield3 = getColumnValue(record, STR_FDESCFIELD3);
					facetStore = getColumnValue(record, STR_STOREENT_ID);
					attrIdentifier = getColumnValue(record, STR_ATTRIDENTIFIER);
					attrSequence = getColumnValue(record, STR_ATTRSEQUENCE);
					attrName = getColumnValue(record, STR_ATTRNAME);
					attrDesc = getColumnValue(record, STR_ATTRDESC);
					sortOrder = getColumnValue(record, STR_SORT_ORDER);
					groupId = getColumnValue(record, STR_GROUP_ID);

					Map<String, String> facetProperties = new HashMap(37);

					facetProperties.put(FACET_PROPERTY_FACET_ID, facetID);
					facetProperties.put(FACET_PROPERTY_FACET_DISPLAYABLE, "1");
					facetProperties.put(FACET_PROPERTY_FACET_SEQUENCE,
							facetSequence);
					facetProperties.put(FACET_PROPERTY_ATTRIBUTE_ID, attrID);
					facetProperties.put(FACET_PROPERTY_SRCHATTR_ID, srchattrId);
					facetProperties.put(FACET_PROPERTY_PROPERTYNAME,
							propertyname);
					facetProperties.put(FACET_PROPERTY_PROPERTYVALUE,
							propertyvalue);
					facetProperties.put(FACET_PROPERTY_SRCHATTRIDENTIFIER,
							srchattridentifier);
					facetProperties.put(FACET_PROPERTY_SELECTION_TYPE,
							facetSelection);
					facetProperties.put(FACET_PROPERTY_KEYWORD_SEARCH, facetKS);
					facetProperties.put(FACET_PROPERTY_ZERO_DISPLAY,
							facetZeroDisplay);
					facetProperties.put(FACET_PROPERTY_MAX_DISPLAY,
							facetMaxDisplay);
					facetProperties.put(FACET_PROPERTY_FIELD1, facetField1);
					facetProperties.put(FACET_PROPERTY_FIELD2, facetField2);
					facetProperties.put(FACET_PROPERTY_FIELD3, facetField3);
					facetProperties.put(FACET_PROPERTY_FACET_NAME, fname);
					facetProperties
							.put(FACET_PROPERTY_FACET_DESCRIPTION, fdesc);
					facetProperties.put(
							FACET_PROPERTY_FACET_DESCRIPTION_FIELD1,
							fdescfield1);
					facetProperties.put(
							FACET_PROPERTY_FACET_DESCRIPTION_FIELD2,
							fdescfield2);
					facetProperties.put(
							FACET_PROPERTY_FACET_DESCRIPTION_FIELD3,
							fdescfield3);
					facetProperties.put(FACET_STOREENT_ID, facetStore);
					facetProperties.put(FACET_PROPERTY_ATTRIBUTE_IDENTIFIER,
							attrIdentifier);
					facetProperties
							.put(FACET_PROPERTY_ATTRIBUTE_NAME, attrName);
					facetProperties.put(FACET_PROPERTY_ATTRIBUTE_DESCRIPTION,
							attrDesc);
					facetProperties.put(FACET_PROPERTY_ATTRIBUTE_SEQUENCE,
							attrSequence);
					facetProperties.put(FACET_PROPERTY_FACET_VALUE_SORT_ORDER,
							sortOrder);
					facetProperties.put(FACET_PROPERTY_FACET_VALUE_GROUP_ID,
							groupId);
					aFacetPropertiesForIndexColumns.put(propertyvalue,
							facetProperties);
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME,
					aFacetPropertiesForIndexColumns);
		}
		return aFacetPropertiesForIndexColumns;
	}

	/**
	 * gets all configured facets from facet table
	 * 
	 * @param indexName
	 * @param searchProfile
	 * @param currencyCode
	 * @param storeId
	 * @return facetProperties
	 * @throws Exception
	 */
	public static List<String> getAllFacetColumns(String indexName,
			String searchProfile, String currencyCode, Integer storeId)
			throws Exception {
		final String METHODNAME = "getAllFacetColumns(String, String, String, String)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { indexName,
					searchProfile, currencyCode, storeId });
		}
		ArrayList<String> facetConfig = new ArrayList();
		ArrayList<String> facetClassicAttributeConfig = new ArrayList();
		ArrayList<String> facetRangeConfig = new ArrayList();
		ArrayList<String> facetCategoryConfig = new ArrayList();

		JDBCQueryService service = new JDBCQueryService(COMPONENT_ID);

		Map queryParameters = new HashMap(2);

		List indexNameList = new ArrayList(1);
		indexNameList.add(indexName);
		queryParameters.put(INDEX_TYPE_QUERY_PARAMETER, indexNameList);

		List masterCatalogIdList = new ArrayList(1);
		masterCatalogIdList.add(SolrSearchConfigurationRegistry.getInstance(
				COMPONENT_ID).getMasterCatalog(storeId));
		queryParameters.put("masterCatalogId", masterCatalogIdList);

		List<HashMap> results = null;
		String readSchema = SolrSearchConfigurationRegistry.getReadSchema();
		if ((readSchema != null) && (readSchema.length() > 0)) {
			ArrayList list = new ArrayList();
			list.add(readSchema);
			queryParameters.put(SCHEMA, list);
			results = service.executeQuery(
					"SELECT_FACETABLE_COLUMNS_SRCHATTRPROP_WORKSPACE",
					queryParameters);
		} else {
			results = service.executeQuery(
					"SELECT_FACETABLE_COLUMNS_SRCHATTRPROP", queryParameters);
		}
		Iterator<HashMap> recordIterator = results.iterator();
		final String OPEN_CURLY_BR = "{";
		final String CLOSE_CURLY_BR = "}";
		while (recordIterator.hasNext()) {
			HashMap<String, String> record = recordIterator.next();

			String propertyName = getColumnValue(record, STR_PROPERTYNAME);
			String propertyValue = getColumnValue(record, STR_PROPERTYVALUE);
			if (propertyName != null) {
				if (propertyName.equals(SRCHATTRPROP_PROPERTY_FACET)) {
					if (propertyValue != null) {
						boolean facetAlreadyProcessed = false;
						if ((propertyValue.contains(OPEN_CURLY_BR))
								&& (propertyValue.contains(CLOSE_CURLY_BR))) {
							facetRangeConfig.add(propertyValue);
							facetAlreadyProcessed = true;
						}
						if (propertyValue
								.equals(PARENT_CATGROUP_ID_SEARCH_FIELD)) {
							facetCategoryConfig.add(propertyValue);
							facetAlreadyProcessed = true;
						}
						if (!facetAlreadyProcessed) {
							facetConfig.add(propertyValue);
						}
					}
				} else if (propertyName
						.equals(SRCHATTRPROP_PROPERTY_FACET_RANGE)) {
					if (propertyValue != null) {
						facetRangeConfig.add(propertyValue);
					}
				} else if (propertyName
						.equals(SRCHATTRPROP_PROPERTY_FACET_CATEGORY)) {
					if (propertyValue != null) {
						facetCategoryConfig.add(propertyValue);
					}
				} else if ((propertyName
						.equals(SRCHATTRPROP_PROPERTY_FACET_CLASSIC_ATTRIBUTE))
						&& (propertyValue != null)) {
					facetClassicAttributeConfig.add(propertyValue);
				}
			}
		}
		facetRangeConfig = SolrSearchConfigurationRegistry
				.removePriceRangeFacetsForOtherCurrencies(facetRangeConfig,
						searchProfile, currencyCode);
		List facetProperties = new ArrayList(facetConfig.size()
				+ facetRangeConfig.size() + facetCategoryConfig.size()
				+ facetClassicAttributeConfig.size());
		facetProperties.addAll(facetConfig);
		facetProperties.addAll(facetRangeConfig);
		facetProperties.addAll(facetCategoryConfig);
		facetProperties.addAll(facetClassicAttributeConfig);
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
		return facetProperties;
	}

	/**
	 * @param astrSearchProfile
	 * @param currencyCode
	 * @return arrstrPriceFieldNameAndPrefix
	 */
	private static String[] getPriceFieldNameAndPrefix(
			String astrSearchProfile, String currencyCode) {
		final String METHODNAME = "getPriceFieldNameAndPrefix(String)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, astrSearchProfile);
		}
		String[] arrstrPriceFieldNameAndPrefix = new String[2];

		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(COMPONENT_ID);

		String tempPhysicalField = searchRegistry.getIndexFieldName(
				astrSearchProfile,
				"CatalogEntryView/Price/Value[(Currency='*')]");
		if (tempPhysicalField != null) {
			arrstrPriceFieldNameAndPrefix[0] = tempPhysicalField.replace("*",
					"");
			arrstrPriceFieldNameAndPrefix[1] = tempPhysicalField.replace("*",
					currencyCode);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME,
					Arrays.toString(arrstrPriceFieldNameAndPrefix));
		}
		return arrstrPriceFieldNameAndPrefix;
	}

	private static String getColumnValue(HashMap<String, String> aRecord,
			String astrColumnName) {
		Object objColumnValue = aRecord.get(astrColumnName);
		return objColumnValue != null ? String.valueOf(objColumnValue).trim()
				: null;
	}

	/**
	 * @param anStoreId
	 * @return storeIdList
	 */
	private static List getStoreIds(Integer anStoreId) {
		StoreObject[] storePath = StoreHelper.getStorePath(anStoreId);
		int len = storePath == null ? 0 : storePath.length;
		List storeIdList = new ArrayList(len + 1);

		storeIdList.add(Integer.valueOf(0));
		for (int i = 0; i < len; i++) {
			Integer relatedStoreId = storePath[i].getUniqueIdentifier();
			if ((relatedStoreId != null)
					&& (!storeIdList.contains(relatedStoreId))) {
				storeIdList.add(relatedStoreId);
			}
		}
		return storeIdList;
	}

	/**
	 * @param astrCategory
	 * @param aFacetableAttributesToSort
	 * @param storeId
	 * @return sortedFacetableAttributes
	 * @throws Exception
	 */
	public static List<Long> sortFacetsForNavigation(String astrCategory,
			Set<Long> aFacetableAttributesToSort, Integer storeId)
			throws Exception {
		final String METHODNAME = "sortFacetsForNavigation(String, Set<Long>, Integer)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { astrCategory,
					aFacetableAttributesToSort, storeId });
		}
		List<Long> sortedFacetableAttributes = new ArrayList(0);
		List<SortInfo> sortedInfoList = new ArrayList(0);
		try {
			if ((astrCategory != null) && (aFacetableAttributesToSort != null)
					&& (!aFacetableAttributesToSort.isEmpty())) {
				Map queryParameters = new HashMap(5);

				ArrayList lCategory = new ArrayList(1);
				lCategory.add(astrCategory);
				queryParameters.put("catalogGroupId", lCategory);
				ArrayList lStoreId = new ArrayList(1);
				lStoreId.add(storeId);
				queryParameters.put("storeId", lStoreId);

				int limit = 950;
				List facetableADToSortSubset;
				if (aFacetableAttributesToSort.size() <= limit) {
					queryParameters.put(SRCHATTRID, new ArrayList(
							aFacetableAttributesToSort));
					sortedInfoList = orderFacetsBySequence(
							SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION,
							queryParameters, sortedInfoList);
				} else {
					Long[] facetableAttributesToSort = aFacetableAttributesToSort
							.toArray(new Long[0]);
					facetableADToSortSubset = new ArrayList();
					for (int i = 0; i < facetableAttributesToSort.length; i++) {
						Long facetableADToSort = facetableAttributesToSort[i];
						facetableADToSortSubset.add(facetableADToSort);
						if ((i > 0) && (i % limit == 0)) {
							queryParameters.put(SRCHATTRID, new ArrayList(
									facetableADToSortSubset));
							sortedInfoList = orderFacetsBySequence(
									SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION,
									queryParameters, sortedInfoList);
							facetableADToSortSubset = new ArrayList();
						}
					}
					if (!facetableADToSortSubset.isEmpty()) {
						queryParameters.put(SRCHATTRID, new ArrayList(
								facetableADToSortSubset));
						sortedInfoList = orderFacetsBySequence(
								SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION,
								queryParameters, sortedInfoList);
					}
				}
				for (SortInfo sortInfo : sortedInfoList) {
					sortedFacetableAttributes.add(sortInfo.getSrchattrId());
				}
			}
		} catch (Exception e) {
			String strExceptionMessage = ExceptionHelper
					.convertStackTraceToString(e);
			if (LOGGER.isLoggable(Level.SEVERE)) {
				LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME,
						"Exception call stack:{0} ", strExceptionMessage);
			}
			throw e;
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, sortedFacetableAttributes);
		}
		return sortedFacetableAttributes;
	}

	/**
	 * @param aFacetableAttributesToSort
	 * @return sortedFacets
	 * @throws Exception
	 */
	public static List<Long> sortFacetsForKeywordSearch(
			Set<Long> aFacetableAttributesToSort) throws Exception {
		final String METHODNAME = "sortFacetsForKeywordSearch(Set<Long>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}
		List<Long> sortedFacets = new ArrayList(0);
		List<SortInfo> sortedInfoList = new ArrayList(0);
		try {
			if ((aFacetableAttributesToSort != null)
					&& (!aFacetableAttributesToSort.isEmpty())) {
				Map queryParameters = new HashMap(3);

				int limit = 950;
				List facetableADToSortSubset;
				if (aFacetableAttributesToSort.size() <= limit) {
					queryParameters.put(SRCHATTRID, new ArrayList(
							aFacetableAttributesToSort));
					sortedInfoList = orderFacetsBySequence(
							SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH,
							queryParameters, sortedInfoList);
				} else {
					Long[] facetableAttributesToSort = aFacetableAttributesToSort
							.toArray(new Long[0]);
					facetableADToSortSubset = new ArrayList();
					for (int i = 0; i < facetableAttributesToSort.length; i++) {
						Long facetableADToSort = facetableAttributesToSort[i];
						facetableADToSortSubset.add(facetableADToSort);
						if ((i > 0) && (i % limit == 0)) {
							queryParameters.put(SRCHATTRID, new ArrayList(
									facetableADToSortSubset));
							sortedInfoList = orderFacetsBySequence(
									SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH,
									queryParameters, sortedInfoList);
							facetableADToSortSubset = new ArrayList();
						}
					}
					if (!facetableADToSortSubset.isEmpty()) {
						queryParameters.put(SRCHATTRID, new ArrayList(
								facetableADToSortSubset));
						sortedInfoList = orderFacetsBySequence(
								SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH,
								queryParameters, sortedInfoList);
					}
				}
				for (SortInfo sortInfo : sortedInfoList) {
					sortedFacets.add(sortInfo.getSrchattrId());
				}
			}
		} catch (Exception e) {
			String strExceptionMessage = ExceptionHelper
					.convertStackTraceToString(e);
			if (LOGGER.isLoggable(Level.SEVERE)) {
				LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME,
						"Exception call stack:{0} ", strExceptionMessage);
			}
			throw e;
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
		return sortedFacets;
	}

	/**
	 * @param queryCode
	 * @param aQueryParameters
	 * @param sortInfo
	 * @return sortInfo
	 * @throws Exception
	 */
	private static List<SortInfo> orderFacetsBySequence(int queryCode,
			Map aQueryParameters, List<SortInfo> sortInfo) throws Exception {
		final String METHODNAME = "orderFacetsBySequence(int, Map, List<SortInfo>";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					new Object[] { Integer.valueOf(queryCode),
							aQueryParameters, sortInfo });
		}
		List<SortInfo> subSortInfo = new ArrayList();
		StringBuilder sbTraceAttrSequence = null;
		if (LOGGER.isLoggable(Level.FINEST)) {
			sbTraceAttrSequence = new StringBuilder(100);
		}
		JDBCQueryService service = new JDBCQueryService(COMPONENT_ID);
		List<HashMap> results = null;
		SolrSearchConfigurationRegistry.getInstance();
		String readSchema = SolrSearchConfigurationRegistry.getReadSchema();
		if ((readSchema != null) && (readSchema.length() > 0)) {
			ArrayList list = new ArrayList();
			list.add(readSchema);
			aQueryParameters.put(SCHEMA, list);
		}
		switch (queryCode) {
		case SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION:
			results = service.executeQuery(
					"SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION_WORKSPACE",
					aQueryParameters);
			break;
		case SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH:
			results = service.executeQuery(
					"SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH_WORKSPACE",
					aQueryParameters);
			break;
		default:
			results = service.executeQuery(
					"SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH_WORKSPACE",
					aQueryParameters);

			switch (queryCode) {
			case SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION:
				results = service.executeQuery(
						"SELECT_FACET_ORDER_FOR_CATEGORY_NAVIGATION",
						aQueryParameters);
				break;
			case SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH:
				results = service.executeQuery(
						"SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH",
						aQueryParameters);
				break;
			default:
				results = service.executeQuery(
						"SELECT_FACET_ORDER_FOR_KEYWORD_SEARCH",
						aQueryParameters);
			}
			break;
		}
		Iterator<HashMap> recordIterator = results.iterator();
		HDMFacetHelper f = new HDMFacetHelper();
		final String COLON = ":";
		final String SPACE = " ";
		Long nSearchAttrId;
		Double sequence;
		for (; recordIterator.hasNext(); subSortInfo
				.add(new HDMFacetHelper().new SortInfo(nSearchAttrId, sequence))) {
			HashMap record = recordIterator.next();
			nSearchAttrId = Long
					.valueOf(record.get(STR_SRCHATTR_ID).toString());
			sequence = Double.valueOf(record.get(STR_SEQUENCE).toString());
			if ((LOGGER.isLoggable(Level.FINEST))
					&& (sbTraceAttrSequence != null)) {
				sbTraceAttrSequence.append(nSearchAttrId).append(COLON)
						.append(sequence).append(SPACE);
			}
			f.getClass();
		}
		if (!sortInfo.isEmpty()) {
			sortInfo = mergeSortInfo(subSortInfo, sortInfo);
		} else {
			sortInfo = subSortInfo;
		}
		if (LOGGER.isLoggable(Level.FINEST)) {
			LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
					"searchAttrId:sequence - {0} ", sbTraceAttrSequence);
		}
		return sortInfo;
	}

	/**
	 * @param subSortInfo
	 * @param sortInfo
	 * @return mergedInfo
	 */
	private static List<SortInfo> mergeSortInfo(List<SortInfo> subSortInfo,
			List<SortInfo> sortInfo) {
		final String METHODNAME = "mergeSortInfo(List<SortInfo>, List<SortInfo>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { subSortInfo,
					sortInfo });
		}
		List<SortInfo> mergedInfo = new ArrayList();
		int subSortInfoIndex = 0;
		int sortInfoIndex = 0;
		while ((subSortInfo.size() > subSortInfoIndex)
				&& (sortInfo.size() > sortInfoIndex)) {
			if ((subSortInfo.get(subSortInfoIndex)).getSequence().doubleValue() < (sortInfo
					.get(sortInfoIndex)).getSequence().doubleValue()) {
				mergedInfo.add(subSortInfo.get(subSortInfoIndex));
				subSortInfoIndex++;
			} else if ((subSortInfo.get(subSortInfoIndex)).getSequence()
					.doubleValue() > (sortInfo.get(sortInfoIndex))
					.getSequence().doubleValue()) {
				mergedInfo.add((SortInfo) sortInfo.get(sortInfoIndex));
				sortInfoIndex++;
			} else {
				mergedInfo.add((SortInfo) subSortInfo.get(subSortInfoIndex));
				mergedInfo.add((SortInfo) sortInfo.get(sortInfoIndex));
				subSortInfoIndex++;
				sortInfoIndex++;
			}
		}
		if (subSortInfo.size() < subSortInfoIndex) {
			for (int i = subSortInfoIndex; i < subSortInfo.size(); i++) {
				mergedInfo.add((SortInfo) subSortInfo.get(i));
			}
		} else {
			for (int i = sortInfoIndex; i < sortInfo.size(); i++) {
				mergedInfo.add((SortInfo) sortInfo.get(i));
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
		return mergedInfo;
	}

	private class SortInfo {
		private Long srchattrId;
		private Double sequence;

		public SortInfo(Long srchattrId, Double sequence) {
			this.srchattrId = srchattrId;
			this.sequence = sequence;
		}

		public Long getSrchattrId() {
			return this.srchattrId;
		}

		public void setSrchattrId(Long srchattrId) {
			this.srchattrId = srchattrId;
		}

		public Double getSequence() {
			return this.sequence;
		}

		public void setSequence(Double sequence) {
			this.sequence = sequence;
		}
	}

	/**
	 * @param categoryId
	 * @param facetPropertiesForIndexColumns
	 * @param astrSearchProfile
	 * @param storeId
	 * @param langId
	 * @param catalogId
	 * @return facetPropertiesForIndexColumns
	 * @throws Exception
	 */
	private static HashMap<String, HashMap<String, String>> determineFacetFieldsForCategory(
			String categoryId,
			HashMap<String, HashMap<String, String>> facetPropertiesForIndexColumns,
			String astrSearchProfile, Integer storeId, Integer langId,
			Long catalogId) throws Exception {
		final String METHODNAME = "determineFacetFieldsForCategory(String, Map<String, Map<String, String>>, String, Integer, Integer, Long)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { categoryId,
					facetPropertiesForIndexColumns, astrSearchProfile, storeId,
					langId, catalogId });
		}
		if ((storeId != null) && (langId != null) && (catalogId != null)) {
			if (isDeepSearchEnabled(storeId.toString())) {
				facetPropertiesForIndexColumns = findFacetFieldsForAllSubcategories(
						categoryId, facetPropertiesForIndexColumns,
						astrSearchProfile, storeId, langId, catalogId);
			} else {
				facetPropertiesForIndexColumns = findFacetFieldsForBrowsedCategoryOnly(
						categoryId, facetPropertiesForIndexColumns,
						astrSearchProfile, storeId, langId, catalogId);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					facetPropertiesForIndexColumns);
		}
		return facetPropertiesForIndexColumns;
	}

	/**
	 * @param categoryId
	 * @param facetPropertiesForIndexColumns
	 * @param astrSearchProfile
	 * @param storeId
	 * @param langId
	 * @param catalogId
	 * @return facetPropertiesForIndexColumnsForCategory
	 * @throws Exception
	 */
	private static HashMap<String, HashMap<String, String>> findFacetFieldsForAllSubcategories(
			String categoryId,
			HashMap<String, HashMap<String, String>> facetPropertiesForIndexColumns,
			String astrSearchProfile, Integer storeId, Integer langId,
			Long catalogId) throws Exception {
		final String METHODNAME = "findFacetFieldsForAllSubcategories(String, Map<String, Map<String, String>>, String, Integer, Integer, Long)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { categoryId,
					facetPropertiesForIndexColumns, astrSearchProfile, storeId,
					langId, catalogId });
		}
		List<String> categoryFacetColumns = new ArrayList();

		SolrSearchConfigurationRegistry registry = SolrSearchConfigurationRegistry
				.getInstance();
		if (LOGGER.isLoggable(Level.FINER)) {
			StringBuilder sb = new StringBuilder("Store ID: ");
			sb.append(storeId);
			sb.append(", Language ID: ");
			sb.append(langId);
			sb.append(", Catalog ID: ");
			sb.append(catalogId);
			LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME, sb.toString());
		}
		if ((storeId != null) && (langId != null) && (catalogId != null)) {
			String readSchema = null;
			ContentContext currentContext = SolrSearchWorkspaceHelper
					.getWorkspaceContext();
			if (currentContext != null) {
				String workspace = currentContext.getWorkspace();
				if (workspace != null) {
					readSchema = currentContext.getWorkspaceData()
							.getReadSchema();
				}
			}
			String coreName = registry.getCoreName(storeId, CATALOGENTRY,
					langId);
			String newCore = coreName;
			String indexName = CATALOGENTRY;
			if (readSchema != null) {
				newCore = newCore.replace(indexName, indexName + UNDERSCORE
						+ readSchema);
			}
			if (LOGGER.isLoggable(Level.FINER)) {
				StringBuilder sb = new StringBuilder("Core name: ");
				sb.append(coreName);
				sb.append("New core name: ");
				sb.append(newCore);
				LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME, sb.toString());
			}
			if (coreName != null) {
				SolrServer server = null;
				try {
					server = registry.getServer(newCore);
					if (server == null) {
						server = registry.getServer(coreName, indexName,
								readSchema);
					}
					if (server != null) {
						if (LOGGER.isLoggable(Level.FINER)) {
							StringBuilder sb = new StringBuilder(
									"Server obtained.");
							sb.append("  Querying the CatalogEntry index for all of the facets for category: ");
							sb.append(categoryId);
							LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
									sb.toString());
						}
						Set keySet = facetPropertiesForIndexColumns.keySet();
						String[] facetColumns = (String[]) keySet
								.toArray(new String[0]);
						if (LOGGER.isLoggable(Level.FINER)) {
							StringBuilder sb = new StringBuilder(
									"Total Facet fields: ");
							sb.append(keySet);
							LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME,
									sb.toString());
						}
						String sMaxFacetFieldsToRequest = SolrSearchConfigurationRegistry
								.getInstance()
								.getExtendedConfigurationPropertyValue(
										CONFIG_KEY_MAX_FACET_FIELDS_TO_REQUEST);
						if (LOGGER.isLoggable(Level.FINEST)) {
							LOGGER.logp(
									Level.FINEST,
									CLASSNAME,
									METHODNAME,
									"maximumFacetFieldsToRequest from wc-component.xml= {0}",
									new Object[] { sMaxFacetFieldsToRequest });
						}
						Integer iMaxFacetFieldsToRequest = Integer
								.valueOf(DEFAULT_MAX_FACET_FIELDS_TO_REQUEST);
						if ((sMaxFacetFieldsToRequest != null)
								&& (sMaxFacetFieldsToRequest.length() > 0)) {
							iMaxFacetFieldsToRequest = Integer.valueOf(Integer
									.parseInt(sMaxFacetFieldsToRequest));
							if (LOGGER.isLoggable(Level.FINEST)) {
								LOGGER.logp(
										Level.FINEST,
										CLASSNAME,
										METHODNAME,
										"iMaxFacetFieldsToRequest set to value from wc-component.xml={0}",
										new Object[] { iMaxFacetFieldsToRequest });
							}
						} else if (LOGGER.isLoggable(Level.FINEST)) {
							LOGGER.logp(
									Level.FINEST,
									CLASSNAME,
									METHODNAME,
									"iMaxFacetFieldsToRequest set to default={0}",
									new Object[] { iMaxFacetFieldsToRequest });
						}
						int count = 0;
						int totalFacetFields = facetColumns.length;
						List<String> facetFieldsToRequest = new ArrayList(
								totalFacetFields > iMaxFacetFieldsToRequest
										.intValue() ? iMaxFacetFieldsToRequest
										.intValue() : totalFacetFields);
						for (int i = 0; i < totalFacetFields; i++) {
							facetFieldsToRequest.add(facetColumns[i]);

							count++;
							if (count % iMaxFacetFieldsToRequest.intValue() == 0) {
								List<String> categoryFacetColumnsSubset = executeSolrQueryAndDetermineFacetFieldSubsetForCategory(
										server, storeId, catalogId, categoryId,
										facetFieldsToRequest, astrSearchProfile);

								categoryFacetColumns
										.addAll(categoryFacetColumnsSubset);

								count = 0;

								facetFieldsToRequest.clear();
							}
						}
						if (count > 0) {
							List<String> categoryFacetColumnsSubset = executeSolrQueryAndDetermineFacetFieldSubsetForCategory(
									server, storeId, catalogId, categoryId,
									facetFieldsToRequest, astrSearchProfile);

							categoryFacetColumns
									.addAll(categoryFacetColumnsSubset);
						}
						keySet = facetPropertiesForIndexColumns.keySet();
						Iterator<String> allFacetColumnsIterator = keySet
								.iterator();
						while (allFacetColumnsIterator.hasNext()) {
							String facetColumn = allFacetColumnsIterator.next();
							if (facetColumn.contains(":")) {
								categoryFacetColumns.add(facetColumn);
								if (LOGGER.isLoggable(Level.FINE)) {
									LOGGER.logp(Level.FINE, CLASSNAME,
											METHODNAME,
											"Added range column: {0} ",
											new Object[] { facetColumn });
								}
							}
						}
						if (!categoryFacetColumns
								.contains(PARENT_CATGROUP_ID_SEARCH_FIELD)) {
							categoryFacetColumns
									.add(PARENT_CATGROUP_ID_SEARCH_FIELD);
						}
					}
				} catch (Exception e) {
					if (LOGGER.isLoggable(Level.FINE)) {
						StringBuilder sb = new StringBuilder(
								"An error occurred while getting the articles from the search engine. ");
						sb.append("The error was: ");
						sb.append(e.getMessage());
						sb.append(". The stack trace is: ");
						sb.append(ExceptionHelper.convertStackTraceToString(e));
						LOGGER.logp(Level.FINE, CLASSNAME, METHODNAME,
								sb.toString());
					}
					throw e;
				}
			}
		}
		HashMap<String, HashMap<String, String>> facetPropertiesForIndexColumnsForCategory = new HashMap(
				categoryFacetColumns.size());

		Iterator<String> it = categoryFacetColumns.iterator();
		while (it.hasNext()) {
			String categoryFacetColumn = it.next();
			HashMap<String, String> facetColumnProperties = facetPropertiesForIndexColumns
					.get(categoryFacetColumn);
			if (facetColumnProperties != null) {
				facetPropertiesForIndexColumnsForCategory.put(
						categoryFacetColumn, facetColumnProperties);
			} else if (LOGGER.isLoggable(Level.FINEST)) {
				LOGGER.logp(
						Level.FINEST,
						CLASSNAME,
						METHODNAME,
						"The category facet column: {0} was not found in the global facet map: {1}",
						new Object[] {
								categoryFacetColumn,
								facetPropertiesForIndexColumnsForCategory
										.keySet() });
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					facetPropertiesForIndexColumnsForCategory);
		}
		return facetPropertiesForIndexColumnsForCategory;
	}

	/**
	 * @param server
	 * @param storeId
	 * @param catalogId
	 * @param categoryId
	 * @param facetFields
	 * @param astrSearchProfile
	 * @return categoryFacetColumns
	 * @throws Exception
	 */
	private static List<String> executeSolrQueryAndDetermineFacetFieldSubsetForCategory(
			SolrServer server, Integer storeId, Long catalogId,
			String categoryId, List<String> facetFields,
			String astrSearchProfile) throws Exception {
		final String METHODNAME = "executeSolrQueryAndDetermineFacetFieldSubsetForCategory";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { storeId,
					catalogId, categoryId, facetFields, astrSearchProfile });
		}
		List<String> categoryFacetColumns = new ArrayList();

		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setFacet(true);
		solrQuery.setFacetLimit(1);
		solrQuery.setFacetMinCount(1);
		solrQuery.setFacetSort(COUNT);

		int facetRequestSize = facetFields.size();
		String strFacetField;
		if (LOGGER.isLoggable(Level.FINER)) {
			StringBuilder sb = new StringBuilder(
					"Facet fields being requested: ");
			sb.append(LINE_SEPARATOR);
			for (Iterator localIterator = facetFields.iterator(); localIterator
					.hasNext();) {
				strFacetField = (String) localIterator.next();
				sb.append("Facet field: ");
				sb.append(strFacetField);
				sb.append(LINE_SEPARATOR);
			}
			sb.append("Facet fields request size: ");
			sb.append(facetRequestSize);
			LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME, sb.toString());
		}
		for (String strFacetField1 : facetFields) {
			solrQuery.addFacetField(new String[] { strFacetField1 });
		}
		StringBuilder queryStringBuilder = new StringBuilder();
		if (storeId != null) {
			List<Integer> lStorePath = getStoreIds(storeId);

			Object itrStoreIds = lStorePath.iterator();
			if (((Iterator) itrStoreIds).hasNext()) {
				queryStringBuilder.append("(");
				queryStringBuilder.append("storeent_id:");
				queryStringBuilder.append(((Iterator) itrStoreIds).next());
			}
			while (((Iterator) itrStoreIds).hasNext()) {
				queryStringBuilder.append(" OR ");
				queryStringBuilder.append("storeent_id:");
				queryStringBuilder.append(((Iterator) itrStoreIds).next());
			}
			if (queryStringBuilder.length() > 0) {
				queryStringBuilder.append(")");
			}
		}
		queryStringBuilder.append(" AND ");
		queryStringBuilder.append("catalog_id:");
		queryStringBuilder.append(catalogId);
		queryStringBuilder.append(" AND ");
		queryStringBuilder.append("parentCatgroup_id_search:");
		queryStringBuilder.append(catalogId);
		queryStringBuilder.append(UNDERSCORE);
		queryStringBuilder.append(categoryId);
		solrQuery.setQuery(queryStringBuilder.toString());
		int totalNum = 0;
		solrQuery.setRows(Integer.valueOf(totalNum));

		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(COMPONENT_ID);
		String strFacetMethod = searchRegistry
				.getFacetMethod(astrSearchProfile);
		if (strFacetMethod != null) {
			solrQuery.set("facet.method", new String[] { strFacetMethod });
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"Setting the facet.method to {0}", strFacetMethod);
			}
		}
		if (LOGGER.isLoggable(Level.FINER)) {
			StringBuilder sb = new StringBuilder("Query being executed:");
			sb.append(solrQuery.toString());
			LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME, sb.toString());
		}
		QueryResponse response = server.query(solrQuery);
		List<FacetField> facetFieldsFromResponse = response.getFacetFields();
		Iterator<FacetField> facetIterator = facetFieldsFromResponse.iterator();
		while (facetIterator.hasNext()) {
			FacetField facet = facetIterator.next();
			String facetColumnName = facet.getName();
			int facetCount = facet.getValueCount();
			if (facetCount > 0) {
				categoryFacetColumns.add(facetColumnName);
			} else if (LOGGER.isLoggable(Level.FINEST)) {
				LOGGER.logp(
						Level.FINEST,
						CLASSNAME,
						METHODNAME,
						"The facet column: {0} had a count of 0 and was not included for the category: {1}",
						new Object[] { facetColumnName, categoryId });
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, categoryFacetColumns);
		}
		return categoryFacetColumns;
	}

	/**
	 * @param categoryId
	 * @param facetPropertiesForIndexColumns
	 * @param astrSearchProfile
	 * @param storeId
	 * @param langId
	 * @param catalogId
	 * @return facetPropertiesForIndexColumns
	 * @throws Exception
	 */
	private static HashMap<String, HashMap<String, String>> findFacetFieldsForBrowsedCategoryOnly(
			String categoryId,
			HashMap<String, HashMap<String, String>> facetPropertiesForIndexColumns,
			String astrSearchProfile, Integer storeId, Integer langId,
			Long catalogId) throws Exception {
		final String METHODNAME = "findFacetFieldsForBrowsedCategoryOnly(String, Map<String, Map<String, String>>, String, Integer, Integer, Long)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { categoryId,
					facetPropertiesForIndexColumns, astrSearchProfile, storeId,
					langId, catalogId });
		}
		try {
			HashSet<String> attrIdSet = new HashSet();
			JDBCQueryService queryService = new JDBCQueryService(COMPONENT_ID);

			Map queryParameters = new HashMap();
			List paramList = new ArrayList(1);
			paramList.add(categoryId);
			final String CATGROUP_ID = "catgroupId";
			queryParameters.put(CATGROUP_ID, paramList);

			paramList = new ArrayList(1);
			paramList.add(catalogId);
			queryParameters.put(CATALOG_ID_QUERY_PARAMETER, paramList);

			List results = null;
			SolrSearchConfigurationRegistry.getInstance();
			String readSchema = SolrSearchConfigurationRegistry.getReadSchema();
			if ((readSchema != null) && (readSchema.length() > 0)) {
				ArrayList list = new ArrayList();
				list.add(readSchema);
				queryParameters.put(SCHEMA, list);
				results = queryService.executeQuery(
						"SELECT_ATTR_ID_FOR_CATEGORY_WORKSPACE",
						queryParameters);
			} else {
				results = queryService.executeQuery(
						"SELECT_ATTR_ID_FOR_CATEGORY", queryParameters);
			}
			Iterator it = results.iterator();
			while (it.hasNext()) {
				Map<String, Object> record = (Map) it.next();
				String attrId = record.get(STR_ATTR_ID).toString();
				attrIdSet.add(attrId);
			}
			Iterator iter = facetPropertiesForIndexColumns.entrySet()
					.iterator();
			while (iter.hasNext()) {
				Map.Entry<String, HashMap<String, String>> entry = (Map.Entry) iter
						.next();
				HashMap<String, String> valueMap = entry.getValue();
				String strFeatured = valueMap
						.get(FACET_PROPERTY_FACET_VALUE_GROUP_ID);
				boolean bFeatured = (strFeatured != null)
						&& (strFeatured.equals(FEATURED_FACET_VALUE));
				if (!bFeatured) {
					String attrIdInMap = valueMap
							.get(FACET_PROPERTY_ATTRIBUTE_ID);
					if (attrIdSet.isEmpty()) {
						if (attrIdInMap != null) {
							iter.remove();
						}
					} else if ((attrIdInMap != null)
							&& (!attrIdSet.contains(attrIdInMap))) {
						iter.remove();
					}
				}
			}
			if (LOGGER.isLoggable(Level.FINER)) {
				StringBuilder sb = new StringBuilder(
						"Facet fields belong to the category: ");
				sb.append(facetPropertiesForIndexColumns.keySet());
				LOGGER.logp(Level.FINER, CLASSNAME, METHODNAME, sb.toString());
			}
		} catch (SQLException | QueryServiceApplicationException e) {
			if (LOGGER.isLoggable(Level.FINE)) {
				StringBuilder sb = new StringBuilder(
						"An error occurred while getting the articles. ");
				sb.append("The error was: ");
				sb.append(e.getMessage());
				sb.append(". The stack trace is: ");
				sb.append(ExceptionHelper.convertStackTraceToString(e));
				LOGGER.logp(Level.FINE, CLASSNAME, METHODNAME, sb.toString());
			}
			throw e;
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					facetPropertiesForIndexColumns);
		}
		return facetPropertiesForIndexColumns;
	}

	/**
	 * @param storeId
	 * @return bDeepSearchEnabled.booleanValue()
	 */
	private static boolean isDeepSearchEnabled(String storeId) {
		final String METHODNAME = "isDeepSearchEnabled";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}
		Boolean bDeepSearchEnabled = StoreHelper.isFeatureEnabled(
				DEEP_SEARCH_STORE_CONFIG_FLAG, Integer.valueOf(storeId));
		if (bDeepSearchEnabled == null) {
			bDeepSearchEnabled = Boolean.valueOf(false);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, bDeepSearchEnabled);
		}
		return bDeepSearchEnabled.booleanValue();
	}

	/**
	 * @param queryParameters
	 * @param facetConfiguration
	 * @return facetConfiguration
	 * @throws QueryServiceApplicationException
	 * @throws SQLException
	 */
	private static Map<String, Map<String, String>> fetchFacetConfigurations(
			Map queryParameters,
			Map<String, Map<String, String>> facetConfiguration)
			throws QueryServiceApplicationException, SQLException {
		final String METHODNAME = "fetchFacetConfigurations(Map, Map<String, Map<String, String>>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					queryParameters, facetConfiguration });
		}
		JDBCQueryService service = new JDBCQueryService(COMPONENT_ID);

		List<HashMap> results = null;
		SolrSearchConfigurationRegistry.getInstance();
		String readSchema = SolrSearchConfigurationRegistry.getReadSchema();
		if ((readSchema != null) && (readSchema.length() > 0)) {
			ArrayList list = new ArrayList();
			list.add(readSchema);
			queryParameters.put(SCHEMA, list);
			results = service.executeQuery(
					"SELECT_FACET_CONFIGURATION_FOR_SEARCH_COLUMNS_WORKSPACE",
					queryParameters);
		} else {
			results = service.executeQuery(
					"SELECT_FACET_CONFIGURATION_FOR_SEARCH_COLUMNS",
					queryParameters);
		}
		Iterator<HashMap> recordIterator = results.iterator();
		while (recordIterator.hasNext()) {
			Map<String, String> facetProperties = new HashMap(7);

			HashMap<String, String> record = (HashMap) recordIterator.next();
			String propertyValue = String
					.valueOf(record.get(STR_PROPERTYVALUE));
			String selection = String.valueOf(record.get(STR_SELECTION));
			String sortOrder = String.valueOf(record.get(STR_SORT_ORDER));
			String keywordSearch = String.valueOf(record
					.get(STR_KEYWORD_SEARCH));
			String zeroDisplay = String.valueOf(record.get(STR_ZERO_DISPLAY));
			String maxDisplay = String.valueOf(record.get(STR_MAX_DISPLAY));
			if ((selection != null) && (selection.length() > 0)) {
				selection = selection.trim();
				facetProperties.put(FACET_PROPERTY_SELECTION_TYPE, selection);
			}
			if ((sortOrder != null) && (sortOrder.length() > 0)) {
				sortOrder = sortOrder.trim();
				facetProperties.put(FACET_PROPERTY_FACET_VALUE_SORT_ORDER,
						sortOrder);
			}
			if ((keywordSearch != null) && (keywordSearch.length() > 0)) {
				keywordSearch = keywordSearch.trim();
				facetProperties.put(FACET_PROPERTY_KEYWORD_SEARCH,
						keywordSearch);
			}
			if ((zeroDisplay != null) && (zeroDisplay.length() > 0)) {
				zeroDisplay = zeroDisplay.trim();
				facetProperties.put(FACET_PROPERTY_ZERO_DISPLAY, zeroDisplay);
			}
			if ((maxDisplay != null) && (maxDisplay.length() > 0)) {
				maxDisplay = maxDisplay.trim();
				facetProperties.put(FACET_PROPERTY_MAX_DISPLAY, maxDisplay);
			}
			if ((propertyValue != null) && (propertyValue.length() > 0)) {
				propertyValue = propertyValue.trim();
				facetConfiguration.put(propertyValue, facetProperties);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, facetConfiguration);
		}
		return facetConfiguration;
	}

	/**
	 * @param queryParameters
	 * @return facetValueInfo
	 */
	private static Map<String, List> fetchAttributeImageAndSeq(
			Map queryParameters) {
		final String METHODNAME = "fetchAttributeImageAndSeq(Map, Map<String, List>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					new Object[] { queryParameters });
		}
		JDBCQueryService service = new JDBCQueryService(COMPONENT_ID);

		Map<String, List> facetValueInfo = new HashMap();
		if (!skipFetchingFacetValueImageAndSequence(COMPONENT_ID)) {
			try {
				SolrSearchConfigurationRegistry.getInstance();
				String readSchema = SolrSearchConfigurationRegistry
						.getReadSchema();
				List<HashMap> results;
				if ((readSchema != null) && (readSchema.length() > 0)) {
					ArrayList list = new ArrayList();
					list.add(readSchema);
					queryParameters.put(SCHEMA, list);
					results = service.executeQuery(
							"SELECT_FACET_VALUE_IMAGE_AND_SEQUENCE_WORKSPACE",
							queryParameters);
				} else {
					results = service.executeQuery(
							"SELECT_FACET_VALUE_IMAGE_AND_SEQUENCE",
							queryParameters);
				}
				Iterator<HashMap> recordIterator = results.iterator();
				while (recordIterator.hasNext()) {
					HashMap<String, String> record = (HashMap) recordIterator
							.next();

					String searchFieldName = null;
					Object oSearchFieldName = record.get("SRCHFIELDNAME");
					if (oSearchFieldName != null) {
						searchFieldName = String.valueOf(oSearchFieldName);
						searchFieldName = searchFieldName.trim();
					}
					String storeentId = null;
					Object oStoreentId = record.get(STR_STOREENT_ID);
					if (oStoreentId != null) {
						storeentId = String.valueOf(oStoreentId);
						storeentId = storeentId.trim();
					}
					String value = null;
					Object oValue = record.get("VALUE");
					if (oValue != null) {
						value = String.valueOf(oValue);
						value = value.trim();
					}
					String sequence = null;
					Object oSequence = record.get(STR_SEQUENCE);
					if (oSequence != null) {
						sequence = String.valueOf(oSequence);
						sequence = sequence.trim();
					}
					String image1 = null;
					Object oImage1 = record.get("IMAGE1");
					if (oImage1 != null) {
						image1 = String.valueOf(oImage1);
						image1 = image1.trim();
					}
					String image2 = null;
					Object oImage2 = record.get("IMAGE2");
					if (oImage2 != null) {
						image2 = String.valueOf(oImage2);
						image2 = image2.trim();
					}
					String qtyUnitId = null;
					Object oqtyUnidId = record.get("QTYUNIT_ID");
					if (oqtyUnidId != null) {
						qtyUnitId = String.valueOf(oqtyUnidId).trim();
					}
					String uomDescription = null;
					Object ouomDescription = record.get("DESCRIPTION");
					if (ouomDescription != null) {
						uomDescription = String.valueOf(ouomDescription).trim();
					}
					String strField1 = getColumnValue(record, "FIELD1");
					String strField2 = getColumnValue(record, "FIELD2");
					String strField3 = getColumnValue(record, "FIELD3");
					if (searchFieldName != null) {
						List<String> facetValueInformation = new ArrayList();
						if (sequence == null) {
							sequence = "0.0";
						}
						facetValueInformation.add(sequence);
						facetValueInformation.add(image1);
						facetValueInformation.add(image2);
						facetValueInformation.add(storeentId);
						facetValueInformation.add(strField1);
						facetValueInformation.add(strField2);
						facetValueInformation.add(strField3);
						facetValueInformation.add(qtyUnitId);
						facetValueInformation.add(uomDescription);
						facetValueInfo.put(value, facetValueInformation);
					}
				}
			} catch (Exception e) {
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(
							LoggingHelper.DEFAULT_TRACE_LOG_LEVEL,
							CLASSNAME,
							METHODNAME,
							"Unable to mediate the IMAGE and SEQUENCE information from the database and update the FacetEntryViewType's.  The error was: "
									+

									ExceptionHelper
											.convertStackTraceToString(e));
				}
				throw new RuntimeException(e);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, facetValueInfo);
		}
		return facetValueInfo;
	}

	/**
	 * @param aFacet
	 * @param abSkipLastValue
	 * @param aCategoryId
	 * @param aCatalogId
	 * @param aStoreId
	 * @param selectionCriteria
	 * @param searchRegistry
	 * @return facetEntries
	 * @throws AbstractApplicationException
	 */
	public static List<Map<String, Object>> facetValues(
			SolrSearchAttributeConfig aFacet, Boolean abSkipLastValue,
			String aCategoryId, String aCatalogId, String aStoreId,
			SelectionCriteria selectionCriteria)
			throws AbstractApplicationException {
		final String METHODNAME = "facetValues(SolrSearchAttributeConfig,Boolean)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { aFacet,
					abSkipLastValue });
		}
		FacetField facetField = aFacet.getFacetField();
		List<FacetField.Count> facetFieldValues = aFacet.getFacetField()
				.getValues();
		if (LOGGER.isLoggable(Level.FINEST)) {
			LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
					"map for facetFieldValues: {0}",
					new Object[] { aFacet.getFacetFieldValues() });
		}
		List<Map<String, Object>> facetEntries = new LinkedList();
		if ((facetFieldValues != null) && (!facetFieldValues.isEmpty())) {
			String strFacetName = facetField.getName();
			if ((strFacetName.equalsIgnoreCase(PARENT_CATGROUP_ID_SEARCH_FIELD))
					|| (strFacetName
							.equalsIgnoreCase(PARENT_CATGROUP_ID_FACET_FIELD))) {
				facetEntries = populateCategoryFacets(aCategoryId, facetField,
						abSkipLastValue, aCatalogId, aStoreId,
						selectionCriteria);
			} else {
				if (abSkipLastValue.booleanValue()) {
					int valueCount = facetFieldValues.size();
					if (valueCount > 1) {
						facetFieldValues = new ArrayList(facetFieldValues);

						facetFieldValues.remove(valueCount - 1);
					}
				}
				Iterator<FacetField.Count> itrValues = facetFieldValues
						.iterator();
				while (itrValues.hasNext()) {
					FacetField.Count facetFieldCountValue = itrValues.next();
					long count = facetFieldCountValue.getCount();
					String strValueName = facetFieldCountValue.getName();
					if (strValueName != null) {
						strValueName = strValueName.replaceAll("\"",
								ESCAPE_QUOTE);
					}
					Map<String, Object> facetEntry = new LinkedHashMap();

					String strLabel = facetFieldCountValue.getName();
					String strFacetValue = null;
					String strUniqueId = null;
					Map<String, Object> extendedData = new LinkedHashMap();

					strFacetValue = strFacetName + ":" + "\"" + strValueName
							+ "\"";

					facetEntry.put(LABEL, strLabel);
					strFacetValue = encodeFacetValue(strFacetValue);
					facetEntry.put(VALUE, strFacetValue);

					String strCount = String.valueOf(count);
					if (strUniqueId == null) {
						strUniqueId = String.valueOf(aFacet.getSrchattrId())
								+ SpecialCharacterHelper
										.convertStrToAscii(facetFieldCountValue
												.getName());
					}
					extendedData.put(UNIQUE_ID, strUniqueId);
					if (LOGGER.isLoggable(Level.FINEST)) {
						LOGGER.logp(
								Level.FINEST,
								CLASSNAME,
								METHODNAME,
								"strFacetValue: {0} strCount: {1}  strLabel: {2}  strUniqueId: {3}",
								new Object[] { strFacetValue, strCount,
										strLabel, strUniqueId });
					}
					facetEntry.put(COUNT, strCount);
					facetEntry.put(EXTENDED_DATA, extendedData);
					facetEntries.add(facetEntry);
				}
			}
		} else if (LOGGER.isLoggable(Level.FINEST)) {
			LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
					"The search engine response does not contain any facet values");
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, facetEntries);
		}
		return facetEntries;
	}

	/**
	 * @param aCategoryId
	 * @param aFacet
	 * @param abSkipLastValue
	 * @param aCatalogId
	 * @param aStoreId
	 * @param selectionCriteria
	 * @param searchRegistry
	 * @return facetEntries
	 * @throws AbstractApplicationException
	 */
	private static List<Map<String, Object>> populateCategoryFacets(
			String aCategoryId, FacetField aFacet, Boolean abSkipLastValue,
			String aCatalogId, String aStoreId,
			SelectionCriteria selectionCriteria)
			throws AbstractApplicationException {
		final String METHODNAME = "populateCategoryFacets(String,FacetField,Boolean,String,String,SelectionCriteria,SolrSearchConfigurationRegistry)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { aCategoryId,
					aFacet, abSkipLastValue, aCatalogId, aStoreId,
					selectionCriteria, "<searchRegistry>" });
		}
		List<Map<String, Object>> facetEntries = new LinkedList();
		if ((aCategoryId != null) && (aCategoryId.trim().length() == 0)) {
			aCategoryId = null;
		}
		List ancestorCategoriesList = getFirstAncestorCategories(aCategoryId,
				selectionCriteria);

		List<FacetField.Count> values = aFacet.getValues();
		if (values != null) {
			if (LOGGER.isLoggable(Level.FINEST)) {
				LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
						"values: {0} ", new Object[] { values });
			}
			HashSet validCatalogs = determineValidCatalogs(aCategoryId,
					aCatalogId, aStoreId);
			if (LOGGER.isLoggable(Level.FINEST)) {
				LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
						"validCatalogs: {0} ", new Object[] { validCatalogs });
			}
			List storePath = getStorePath(Integer.valueOf(aStoreId));

			List<Object[]> categories = new ArrayList(values.size());
			List<String> categoryNames = new ArrayList();
			HashSet<String> previousCategories = new HashSet();
			int nNumberOfValues = 0;
			if (abSkipLastValue.booleanValue()) {
				nNumberOfValues = values.size() - 1;
			} else {
				nNumberOfValues = values.size();
			}
			boolean isDeepSearch = isDeepSearchEnabled(aStoreId);
			for (int j = 0; j < nNumberOfValues; j++) {
				boolean createFacet = true;

				FacetField.Count facetValueObject = values.get(j);

				BigInteger nCount = BigInteger.valueOf(facetValueObject
						.getCount());

				String catalogIdCategoryId = facetValueObject.getName();

				String catalogIdFromIndex = null;
				String categoryIdFromIndex = null;

				int pos = catalogIdCategoryId.indexOf(UNDERSCORE);
				if (pos > -1) {
					catalogIdFromIndex = catalogIdCategoryId.substring(0, pos);
					categoryIdFromIndex = catalogIdCategoryId
							.substring(pos + 1);
				}
				createFacet = validateCategory(validCatalogs,
						catalogIdFromIndex, categoryIdFromIndex, aCategoryId,
						ancestorCategoriesList, previousCategories,
						selectionCriteria, storePath, isDeepSearch);
				if (createFacet) {
					// flag to add current category to list of categories to
					// generateCategoryFacets
					boolean addFacet = true;
					// in case of search flow/pages,
					// add current category to list of categories, only if it is
					// a department(top category) at the time of initial search
					// add current category to list of categories, only if it is
					// a immediate child category of the categoryfacet clicked
					// in search results page when we click of category facet in
					// search result
					if (isKeywordSearch(selectionCriteria)) {
						List ancestorCategoriesListForIteration = getFirstAncestorCategories(
								categoryIdFromIndex, selectionCriteria);
						if (StringUtils.isBlank(aCategoryId)) {
							if (ancestorCategoriesListForIteration != null
									&& !ancestorCategoriesListForIteration
											.isEmpty()) {
								addFacet = false;
							}
						} else {
							String parentId = null;
							parentId = (ancestorCategoriesListForIteration != null && !ancestorCategoriesListForIteration
									.isEmpty()) ? (String) ancestorCategoriesListForIteration
									.get(ancestorCategoriesListForIteration
											.size() - 1) : "";
							if (categoryIdFromIndex != null
									&& (categoryIdFromIndex.equals(aCategoryId) || !parentId
											.equals(aCategoryId))) {
								addFacet = false;
							}
						}
						ancestorCategoriesListForIteration = null;
					}
					if (addFacet) {
						previousCategories.add(categoryIdFromIndex);
						if (LOGGER.isLoggable(Level.FINEST)) {
							LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
									"createFacet: {0} for {1} "
											+ new Object[] { createFacet,
													categoryIdFromIndex });
						}
						String categoryName = HierarchyHelper.getCategoryName(
								categoryIdFromIndex,
								(SearchCriteria) selectionCriteria);
						if (categoryName != null) {
							Object[] facetInfo = { facetValueObject,
									catalogIdFromIndex, categoryIdFromIndex,
									nCount, categoryName, aFacet.getName() };
							categoryNames.add(categoryName);
							categories.add(facetInfo);
						}
					}
				}
			}
			facetEntries = generateCategoryFacets(categories, selectionCriteria);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, facetEntries);
		}
		return facetEntries;
	}

	/**
	 * @param aCategoryId
	 * @param aCatalogId
	 * @param aStoreId
	 * @param searchRegistry
	 * @return validCatalogs
	 */
	private static HashSet<String> determineValidCatalogs(String aCategoryId,
			String aCatalogId, String aStoreId) {
		final String METHODNAME = "determineValidCatalogs(String,String,String,SolrSearchConfigurationRegistry)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { aCategoryId,
					aCatalogId, aStoreId, "<SolrSearchConfigurationRegistry>" });
		}
		HashSet<String> validCatalogs = new HashSet();

		Long catalogId = Long.valueOf(aCatalogId);
		if (catalogId != null) {
			validCatalogs.add(String.valueOf(catalogId));
		}
		if (aCategoryId != null) {
			Integer storeId = null;
			try {
				storeId = Integer.valueOf(aStoreId);
				if (storeId != null) {
					List storeIds = getStoreIds(storeId);
					if ((storeIds != null) && (!storeIds.isEmpty())) {
						JDBCQueryService service = new JDBCQueryService(
								COMPONENT_ID);
						Map queryParameters = new HashMap(2);

						queryParameters.put("storeIds", storeIds);

						List<HashMap> results = null;
						String readSchema = SolrSearchConfigurationRegistry
								.getReadSchema();
						if ((readSchema != null) && (readSchema.length() > 0)) {
							ArrayList list = new ArrayList();
							list.add(readSchema);
							queryParameters.put(SCHEMA, list);
							results = service
									.executeQuery(
											"SELECT_CATALOG_ID_FROM_STORECAT_WORKSPACE",
											queryParameters);
						} else {
							results = service.executeQuery(
									SELECT_FROM_STORECAT, queryParameters);
						}
						Iterator<HashMap> recordIterator = results.iterator();
						while (recordIterator.hasNext()) {
							HashMap<String, String> record = (HashMap) recordIterator
									.next();
							String validCatalog = String.valueOf(record
									.get("CATALOG_ID"));
							if (validCatalog != null) {
								validCatalogs.add(validCatalog);
							}
						}
					}
				}
			} catch (Exception e) {
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"An error occurred while getting the valid catalogs for the given store: {0}. The error was: {1}",
							new Object[] { storeId, e.getMessage() });
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, validCatalogs);
		}
		return validCatalogs;
	}

	/**
	 * custom logic to include current & parent categories in case of the
	 * request is from browsing pages
	 * 
	 * @param validCatalogs
	 * @param catalogIdFromIndex
	 * @param categoryIdFromIndex
	 * @param categoryFromRequest
	 * @param ancestorCategoriesList
	 * @param previousCategories
	 * @param selectionCriteria
	 * @param storePath
	 * @param isDeepSearch
	 * @return createFacet
	 * @throws AbstractApplicationException
	 */
	private static boolean validateCategory(HashSet<String> validCatalogs,
			String catalogIdFromIndex, String categoryIdFromIndex,
			String categoryFromRequest, List<String> ancestorCategoriesList,
			HashSet<String> previousCategories,
			SelectionCriteria selectionCriteria, List storePath,
			boolean isDeepSearch) throws AbstractApplicationException {
		/*
		 * custom logic to include current & parent categories in case of the
		 * request is from browsing pages
		 */
		final String METHODNAME = "validateCategory(HashSet<String>,String,String,String,List<String>,HashSet<String>,SelectionCrteria, boolean)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(
					CLASSNAME,
					METHODNAME,
					new Object[] { validCatalogs, catalogIdFromIndex,
							categoryIdFromIndex, categoryFromRequest,
							ancestorCategoriesList, previousCategories,
							selectionCriteria, storePath,
							Boolean.valueOf(isDeepSearch) });
		}
		boolean createFacet = true;
		if ((catalogIdFromIndex != null) && (validCatalogs != null)
				&& (!validCatalogs.isEmpty())) {
			if (validCatalogs.contains(catalogIdFromIndex)) {
				createFacet = true;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = true] CATEGORY: {0} from catalog: {2} is in the set of valid catalogs {3}",
							new Object[] { categoryIdFromIndex,
									catalogIdFromIndex, validCatalogs });
				}
			} else {
				createFacet = false;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = false] CATEGORY: {0}  from catalog: {1} is NOT in the set of valid catalogs {2} ",
							new Object[] { categoryIdFromIndex,
									catalogIdFromIndex, validCatalogs });
				}
			}
		}
		if ((createFacet) && (categoryIdFromIndex != null)
				&& (storePath != null) && (storePath.size() > 1)) {
			Serializable[] key = { GET_CATEGORY_STORE_ID, categoryIdFromIndex };

			String catStoreId = null;

			Collection collResult = null;
			try {
				collResult = MY_GET_CATEGORY_STORE_ID_CACHE.getOrPut(key);
			} catch (Exception e) {
				if (LoggingHelper.isTraceEnabled(LOGGER)) {
					LOGGER.logp(
							LoggingHelper.DEFAULT_TRACE_LOG_LEVEL,
							CLASSNAME,
							METHODNAME,
							"Check category store id failed: "
									+ ExceptionHelper
											.convertStackTraceToString(e));
				}
			}
			if ((collResult != null) && (!collResult.isEmpty())) {
				catStoreId = (String) collResult.iterator().next();
			}
			if ((catStoreId != null)
					&& (!storePath.contains(Integer.valueOf(catStoreId)))) {
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"Ignore CATEGORY:{0} since it belongs to: {1} which is not in the store path.",
							new Object[] { categoryIdFromIndex, catStoreId });
				}
				createFacet = false;
			}
		}
		if ((createFacet) && (categoryIdFromIndex != null)
				&& (previousCategories != null)
				&& (!previousCategories.isEmpty())) {
			if (previousCategories.contains(categoryIdFromIndex)) {
				createFacet = false;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = false] CATEGORY: {0} from catalog: {1} is in the set of previously processed categories: {2}"
									+ new Object[] { categoryIdFromIndex,
											catalogIdFromIndex,
											previousCategories });
				}
			} else {
				createFacet = true;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = true] CATEGORY: {0} from catalog: {1} is not a previously processed category: {2}",
							new Object[] { categoryIdFromIndex,
									catalogIdFromIndex, previousCategories });
				}
			}
		}
		if ((createFacet) && (categoryIdFromIndex != null)
				&& (categoryFromRequest != null)) {
			if (categoryIdFromIndex.equals(categoryFromRequest)) {
				// orginally createFacet was set to false to not to include
				// current category as facet
				// now createFacet is set to true to consider current category
				// also to create facet
				createFacet = true;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = false] CATEGORY : {0} IS the same as the category from the request: {1} ",
							new Object[] { categoryIdFromIndex,
									categoryFromRequest });
				}
			} else {
				createFacet = true;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = true] CATEGORY : {0} is NOT the same as the category from the request: {1}",
							new Object[] { categoryIdFromIndex,
									categoryFromRequest });
				}
			}
		}
		if ((createFacet) && (categoryIdFromIndex != null)
				&& (categoryFromRequest != null)) {
			if (isCategoryAChild(categoryFromRequest, categoryIdFromIndex,
					selectionCriteria, isDeepSearch)) {
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = true] CATEGORY : {0}  IS a child of {1} ",
							new Object[] { categoryIdFromIndex,
									categoryFromRequest });
				}
				// orginally createFacet was set to false to not to include
				// parent of category facet/categoryFromRequest
				// now createFacet is set to true to consider parent of category
				// facet/categoryFromRequest also to create facet
				createFacet = true;
			} else {
				createFacet = true;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = false] CATEGORY : {0} is NOT a child of {1}",
							new Object[] { categoryIdFromIndex,
									categoryFromRequest });
				}
			}
		}
		if ((createFacet) && (categoryFromRequest != null)
				&& (categoryIdFromIndex != null)
				&& (ancestorCategoriesList != null)
				&& (!ancestorCategoriesList.isEmpty())) {
			if (ancestorCategoriesList.contains(categoryIdFromIndex)) {
				createFacet = true;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = false] CATEGORY : {0} IS in ancestorCategoriesSet {1}",
							new Object[] { categoryIdFromIndex,
									ancestorCategoriesList });
				}
			} else {
				createFacet = true;
				if (LOGGER.isLoggable(Level.FINEST)) {
					LOGGER.logp(
							Level.FINEST,
							CLASSNAME,
							METHODNAME,
							"[createFacet = true] CATEGORY : {0} IS NOT in ancestorCategoriesSet {1}",
							new Object[] { categoryIdFromIndex,
									ancestorCategoriesList });
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, Boolean.valueOf(createFacet));
		}
		return createFacet;
	}

	/**
	 * @param categoryIdParent
	 * @param categoryIdChild
	 * @param selectionCriteria
	 * @param isDeepSearch
	 * @return isParent
	 * @throws AbstractApplicationException
	 */
	private static boolean isCategoryAChild(String categoryIdParent,
			String categoryIdChild, SelectionCriteria selectionCriteria,
			boolean isDeepSearch) throws AbstractApplicationException {
		final String METHODNAME = "isCategoryAChild(String,String,SelectionCriteria, boolean)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					categoryIdParent, categoryIdChild, selectionCriteria,
					Boolean.valueOf(isDeepSearch) });
		}
		boolean isParent = false;
		List<List<String>> ancestorLists = HierarchyHelper.getNavigationPath(
				categoryIdChild, (SearchCriteria) selectionCriteria);
		if ((ancestorLists != null) && (!ancestorLists.isEmpty())) {
			for (List<String> ancestorList : ancestorLists) {
				if ((isDeepSearch)
						&& (!isDisplayImmediateChildCategoryFacetOnly()
								.booleanValue())) {
					isParent = ancestorList.contains(categoryIdParent);
				} else if (ancestorList.size() > 2) {
					isParent = categoryIdParent.equals(ancestorList
							.get(ancestorList.size() - 2));
				}
				if (isParent) {
					break;
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, Boolean.valueOf(isParent));
		}
		return isParent;
	}

	/**
	 * @param categoryFacets
	 * @param categoryNames
	 * @param selectionCriteria
	 * @return facetEntries
	 * @throws AbstractApplicationException
	 */
	private static List<Map<String, Object>> generateCategoryFacets(
			List<Object[]> categoryFacets, SelectionCriteria selectionCriteria)
			throws AbstractApplicationException {
		final String METHODNAME = "generateCategoryFacets(List<Object[]>,List<String>,SelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			List<Object> categoryFacetsList = new ArrayList(
					categoryFacets.size());
			for (Object[] objArray : categoryFacets) {
				categoryFacetsList.add(Arrays.asList(objArray));
			}
		}

		Iterator<Object[]> it = categoryFacets.iterator();
		List<Map<String, Object>> facetEntries = new LinkedList();
		while (it.hasNext()) {
			Object[] facetInfo = it.next();
			String categoryId = (String) facetInfo[2];
			BigInteger nfacetCount = (BigInteger) facetInfo[3];
			String categoryName = (String) facetInfo[4];

			Map<String, Object> facetEntry = new LinkedHashMap();

			Map<String, Object> extendedData = new HashMap();
			extendedData.put(UNIQUE_ID, categoryId);

			Set categorySet = getAncestorCategoriesSet(categoryId,
					selectionCriteria, true);
			Iterator iter = categorySet.iterator();
			StringBuilder parentIds = new StringBuilder();
			while (iter.hasNext()) {
				parentIds.append(String.valueOf(iter.next()) + UNDERSCORE);
			}
			if (parentIds.length() > 0) {
				extendedData.put(
						PARENT_IDS,
						parentIds.toString().substring(0,
								parentIds.toString().length() - 1));
			}
			facetEntry.put(LABEL, categoryName);
			facetEntry.put(VALUE, categoryId);
			String strCount = String.valueOf(nfacetCount);
			facetEntry.put(COUNT, strCount);
			facetEntry.put(EXTENDED_DATA, extendedData);
			facetEntries.add(facetEntry);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
		return facetEntries;
	}

	/**
	 * @param aCategoryId
	 * @param selectionCriteria
	 * @return ancestorCategoriesList
	 * @throws AbstractApplicationException
	 */
	private static List getFirstAncestorCategories(String aCategoryId,
			SelectionCriteria selectionCriteria)
			throws AbstractApplicationException {
		final String METHODNAME = "getFirstAncestorCategories(String,SelectionCriteria, boolean)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { aCategoryId,
					selectionCriteria });
		}
		List ancestorCategoriesList = new ArrayList();
		if ((aCategoryId != null) && (aCategoryId.length() > 0)) {
			List<List<String>> ancestorLists = HierarchyHelper
					.getNavigationPath(aCategoryId,
							(SearchCriteria) selectionCriteria);
			if ((ancestorLists != null) && (!ancestorLists.isEmpty())) {
				Iterator<String> anIt = ancestorLists.get(0).iterator();
				while (anIt.hasNext()) {
					String ancestorId = anIt.next();
					if ((ancestorId != null) && (!ancestorId.equals("-1"))) {
						ancestorCategoriesList.add(ancestorId);
					}
				}
				ancestorCategoriesList.remove(aCategoryId);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, ancestorCategoriesList);
		}
		return ancestorCategoriesList;
	}

	/**
	 * @param aCategoryId
	 * @param selectionCriteria
	 * @param isOneLevel
	 * @return ancestorCategories
	 * @throws AbstractApplicationException
	 */
	private static Set getAncestorCategoriesSet(String aCategoryId,
			SelectionCriteria selectionCriteria, boolean isOneLevel)
			throws AbstractApplicationException {
		final String METHODNAME = "getAncestorCategoriesSet";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { aCategoryId,
					selectionCriteria, Boolean.valueOf(isOneLevel) });
		}
		Set<String> ancestorCategories = new LinkedHashSet();
		if ((aCategoryId != null) && (aCategoryId.length() > 0)) {
			List<List<String>> ancestorLists = HierarchyHelper
					.getNavigationPath(aCategoryId,
							(SearchCriteria) selectionCriteria);
			if ((ancestorLists != null) && (!ancestorLists.isEmpty())) {
				Iterator<String> anIt;
				for (Iterator localIterator = ancestorLists.iterator(); localIterator
						.hasNext();) {
					List<String> ancestorList = (List) localIterator.next();
					anIt = ancestorList.iterator();
					while (anIt.hasNext()) {
						String ancestorId = anIt.next();
						if ((ancestorId != null) && (!ancestorId.equals("-1"))) {
							ancestorCategories.add(ancestorId);
						}
					}
				}
				ancestorCategories.remove(aCategoryId);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, ancestorCategories);
		}
		return ancestorCategories;
	}

	/**
	 * @param astrFacetValue
	 * @return strFacetValue
	 */
	private static String encodeFacetValue(String astrFacetValue) {
		final String METHODNAME = "encodeFacetValue(String)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, astrFacetValue);
		}
		String strFacetValue = astrFacetValue;
		try {
			String strEncodedFacetValue = URLEncoder.encode(strFacetValue,
					"UTF-8");
			strFacetValue = strEncodedFacetValue;
		} catch (UnsupportedEncodingException e) {
			if (LOGGER.isLoggable(Level.FINEST)) {
				LOGGER.logp(
						Level.FINEST,
						CLASSNAME,
						METHODNAME,
						"Unable to URL encode facet value ' {0}'. The value will not be encoded. ",
						new Object[] { strFacetValue });
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, strFacetValue);
		}
		return strFacetValue;
	}

	/**
	 * @param selectionCriteria
	 * @return bKeywordSearch
	 */
	public static boolean isKeywordSearch(SelectionCriteria selectionCriteria) {
		final String METHODNAME = "isKeywordSearch()";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}
		Map<String, List<RelationalExpression>> controlParameters = selectionCriteria
				.getControlParameters();
		List<RelationalExpression> searchTermList = controlParameters
				.get(HDMConstants._WCF_SEARCH_TERM);

		String strSearchTerm = (searchTermList != null)
				&& (!searchTermList.isEmpty()) ? (String) (searchTermList
				.get(0)).getValues().get(0) : null;

		boolean bKeywordSearch = StringUtils.isNotBlank(strSearchTerm);
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME,
					Boolean.valueOf(bKeywordSearch));
		}
		return bKeywordSearch;
	}

	/**
	 * @param categoryId
	 * @return storeId
	 */
	private static String fetchStoreIdForCategory(String categoryId) {
		final String METHODNAME = "fetchStoreIdForCategory(String categoryId)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { categoryId });
		}
		JDBCQueryService service = new JDBCQueryService(COMPONENT_ID);

		Map queryParameters = new HashMap(2);
		List categoryIdList = new ArrayList();
		categoryIdList.add(categoryId);
		queryParameters.put("catgroupId", categoryIdList);

		String storeId = null;
		try {
			SolrSearchConfigurationRegistry.getInstance();
			String readSchema = SolrSearchConfigurationRegistry.getReadSchema();
			List<HashMap> results;
			if ((readSchema != null) && (readSchema.length() > 0)) {
				ArrayList list = new ArrayList();
				list.add(readSchema);
				queryParameters.put(SCHEMA, list);
				results = service.executeQuery(
						"SELECT_STORE_ID_FROM_STORECGRP_WORKSPACE",
						queryParameters);
			} else {
				results = service.executeQuery(
						"SELECT_STORE_ID_FROM_STORECGRP", queryParameters);
			}
			Iterator<HashMap> recordIterator = results.iterator();
			while (recordIterator.hasNext()) {
				HashMap<String, String> record = (HashMap) recordIterator
						.next();

				Object oStoreId = record.get(STR_STOREENT_ID);
				if (oStoreId != null) {
					storeId = String.valueOf(oStoreId);
					if (LOGGER.isLoggable(Level.FINEST)) {
						LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
								"Got store id: {0} for category: {1}"
										+ new Object[] { storeId, categoryId });
					}
				}
			}
		} catch (Exception e) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(LoggingHelper.DEFAULT_TRACE_LOG_LEVEL, CLASSNAME,
						METHODNAME,
						"Unable to get store id for specified cateogry.  The error was: "
								+ ExceptionHelper.convertStackTraceToString(e));
			}
			throw new RuntimeException(e);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, storeId);
		}
		return storeId;
	}

	/**
	 * @param storeId
	 * @return storePath
	 */
	private static List getStorePath(Integer storeId) {
		final String METHODNAME = "getStorePath(Integer storeId)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] { storeId });
		}
		StoreObject[] storePathArray = StoreHelper.getStorePath(storeId);
		int len = storePathArray == null ? 0 : storePathArray.length;
		List storePath = new ArrayList(len);
		for (int i = 0; i < len; i++) {
			Integer relatedStoreId = storePathArray[i].getUniqueIdentifier();

			if (relatedStoreId != null) {
				storePath.add(relatedStoreId);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, storePath);
		}
		return storePath;
	}

	/**
	 * @param astrComponentId
	 * @return bSkipFetchingFacetValueImageAndSequence
	 */
	public static boolean skipFetchingFacetValueImageAndSequence(
			String astrComponentId) {
		final String METHODNAME = "skipFetchingFacetValueImageAndSequence()";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}
		boolean bSkipFetchingFacetValueImageAndSequence = false;

		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(astrComponentId);
		String strSkipFetchingFacetValueImageAndSequence = searchRegistry
				.getExtendedConfigurationPropertyValue(PROPERTY_USE_SKIP_FETCHING_FACET_VALUE_IMAGE_AND_SEQ);
		if ((strSkipFetchingFacetValueImageAndSequence != null)
				&& (Boolean
						.parseBoolean(strSkipFetchingFacetValueImageAndSequence))) {
			bSkipFetchingFacetValueImageAndSequence = true;
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME,
					Boolean.valueOf(bSkipFetchingFacetValueImageAndSequence));
		}
		return bSkipFetchingFacetValueImageAndSequence;
	}

	/**
	 * @return config
	 */
	private static Boolean isDisplayImmediateChildCategoryFacetOnly() {
		final String METHODNAME = "isDisplayImmediateChildCategoryFacetOnly()";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME);
		}
		SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
				.getInstance(COMPONENT_ID);
		String categoryFacetDisplayConfig = searchRegistry
				.getExtendedConfigurationPropertyValue("LimitDeepCategoryFacetValuesToImmediateChildrenOnly");
		Boolean config = Boolean.valueOf((categoryFacetDisplayConfig != null)
				&& (Boolean.parseBoolean(categoryFacetDisplayConfig)));
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, config);
		}
		return config;
	}
}
