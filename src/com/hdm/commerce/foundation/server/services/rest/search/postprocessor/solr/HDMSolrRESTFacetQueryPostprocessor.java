package com.hdm.commerce.foundation.server.services.rest.search.postprocessor.solr;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;

import com.hdm.commerce.constants.HDMConstants;
import com.hdm.commerce.foundation.server.services.rest.search.helper.HDMFacetHelper;
import com.ibm.commerce.foundation.common.exception.AbstractApplicationException;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.common.exception.ExceptionHelper;
import com.ibm.commerce.foundation.internal.common.exception.FoundationApplicationException;
import com.ibm.commerce.foundation.internal.server.services.context.RemoteContextServiceFactory;
import com.ibm.commerce.foundation.internal.server.services.registry.BaseStoreProperty;
import com.ibm.commerce.foundation.internal.server.services.registry.StoreObject;
import com.ibm.commerce.foundation.internal.server.services.registry.StoreRegistry;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchAttributeConfig;
import com.ibm.commerce.foundation.internal.server.services.search.config.solr.SolrSearchConfigurationRegistry;
import com.ibm.commerce.foundation.internal.server.services.search.util.FacetEntrySequenceComparator;
import com.ibm.commerce.foundation.internal.server.services.search.util.FacetHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.FacetQuery;
import com.ibm.commerce.foundation.internal.server.services.search.util.HierarchyHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.SearchQueryHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.SpecialCharacterHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.StoreHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.dataaccess.bom.exception.BusinessObjectMediatorException;
import com.ibm.commerce.foundation.server.services.rest.search.SearchCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPostprocessor;
import com.ibm.commerce.foundation.server.services.valuemapping.ValueMappingService;

/**
 * @author TCS
 * 
 *         this post processor class is having OOB FacetHelper logic with
 *         required changes/customizations in the logic 1.changes for category
 *         facet by invoking custom facet helper instead of OOB facet helper
 *         2.changes for price facet to include multi value selection flag in
 *         extended data field of rest response
 */
public class HDMSolrRESTFacetQueryPostprocessor extends
		AbstractSolrSearchQueryPostprocessor implements
		SearchQueryPostprocessor {
	private static final String CLASSNAME = HDMSolrRESTFacetQueryPostprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASSNAME);
	private String iComponentId = null;
	private SolrSearchConfigurationRegistry iSearchRegistry = null;
	private LinkedList<Map<String, Object>> iFacetValues = new LinkedList();
	private String iSearchProfile = null;
	private String iCatalogId = null;
	private String iCategoryId = null;
	private String iLanguageId = null;
	private String iStoreId = null;
	private String iCurrencyCode = null;
	private String iFacetEntry = null;
	private String iFacetName = null;
	private String iFacetValue = null;
	private static final String COUNT = "count";
	private static final String LABEL = "label";
	private static final String UNIQUE_ID = "uniqueId";
	private static final String SEQUENCE = "sequence";
	private static final String ENTRY = "Entry";
	private static final String NAME = "name";
	private static final String VALUE = "value";
	private static final String IMAGE = "image";
	private static final String IMAGE1 = "image1";
	private static final String UOMID = "unitID";
	private static final String UOMDESCRIPTION = "unitOfMeasure";
	private static final String IMAGE2 = "image2";
	private static final String FIELD1 = "field1";
	private static final String FIELD2 = "field2";
	private static final String FIELD3 = "field3";
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	private static final String ATTRIBUTE_FACET_ALLOW_MUTIPLE_VALUE_SELECTION = "allowMultipleValueSelection";
	private static final String ATTRIBUTE_FACET_ALL_VALUES_RETURNED = "allValuesReturned";
	private static final String ATTRIBUTE_FACET_MAX_DISPLAY = "maximumValuesToDisplay";
	private static final String ATTRIBUTE_MERCHANDISING_FACET_GROUP_ID = "groupId";
	private static final String EXTENDED_DATA = "extendedData";
	private static final String STR_SRCHATTR_ID = "srchattr_id";
	private static final String STR_SELECTION = "selection";
	private static final String STR_MAX_DISPLAY = "max_display";
	private static final String STR_GROUP_ID = "group_id";
	private static final String STR_AD = "AD";
	private static final String STR_AD_FLOAT = "ADF";
	private static final String STR_NTK_CS = "_NTK_CS";
	private static final String LANGUAGE_ID = "langId";

	/**
	 * @param componentId
	 */
	public HDMSolrRESTFacetQueryPostprocessor(String componentId) {
		this.iComponentId = componentId;
		this.iSearchRegistry = SolrSearchConfigurationRegistry
				.getInstance(this.iComponentId);
	}

	/**
	 * main method invoking custom logic
	 */
	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryResponseObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryResponseObjects });
		}
		super.invoke(selectionCriteria, queryResponseObjects);

		boolean bSkip = false;
		if ((this.iSearchResponseObject == null)
				|| (this.iSearchResponseObject.getTotalMatches() == BigInteger.ZERO)) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"The total number of objects is zero. {0}",
						new Object[] { this.iSearchResponseObject });
			}
			bSkip = true;
		}
		if (((this.iQueryResponse.getFacetFields() == null) || (this.iQueryResponse
				.getFacetFields().isEmpty()))
				&& ((this.iQueryResponse.getFacetQuery() == null) || (this.iQueryResponse
						.getFacetQuery().isEmpty()))) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(
						Level.INFO,
						CLASSNAME,
						METHODNAME,
						"iQueryResponse.getFacetFields(): {0} iQueryResponse.getFacetQuery(): {1}",
						new Object[] { this.iQueryResponse.getFacetFields(),
								this.iQueryResponse.getFacetQuery() });
			}
			bSkip = true;
		}
		if (bSkip) {
			if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
				LOGGER.exiting(CLASSNAME, METHODNAME);
			}
			return;
		}
		try {
			this.iSearchProfile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
			this.iCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
			this.iCatalogId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CATALOG);
			this.iCategoryId = getControlParameterValue(HDMConstants._WCF_SEARCH_CATEGORY);
			this.iLanguageId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_LANGUAGE);
			this.iStoreId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);

			String responseTemplate = getControlParameterValue(HDMConstants._WCF_SEARCH_INTERNAL_RESP_TEMPLATE);
			SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
					.getInstance(selectionCriteria.getComponentId());
			if ((responseTemplate == null) || (responseTemplate.length() == 0)) {
				responseTemplate = searchRegistry
						.getExtendedConfigurationPropertyValue("ResponseTemplateDefault");
			}
			if ((responseTemplate == null) || (responseTemplate.length() == 0)) {
				responseTemplate = "0";
			}
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"Response template: {0}",
						new Object[] { responseTemplate });
			}
			if (responseTemplate.equals("-1")) {
				if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
					LOGGER.exiting(CLASSNAME, METHODNAME);
				}
				return;
			}
			String resourceName = getControlParameterValue("_wcf.search.internal.service.resource");
			String mapperReferenceName = "ResponseTemplate" +

			HDMConstants.FORWARD_SLASH + responseTemplate +

			HDMConstants.FORWARD_SLASH + resourceName +

			HDMConstants.FORWARD_SLASH + "FacetView";
			String mapperName = searchRegistry
					.getExtendedConfigurationPropertyValue(mapperReferenceName);
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"Resource name: {0}", new Object[] { resourceName });
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"Mapper reference name: {0}",
						new Object[] { mapperReferenceName });
				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"Mapper name: {0}", new Object[] { mapperName });
			}
			ValueMappingService mapper = ValueMappingService
					.getInstance(this.iSelectionCriteria.getComponentId());
			final String M_FACET_VIEW = String.valueOf(mapper.getExternalValue(
					mapperName, "FacetView"));
			final String M_FACET_VIEW_SLASH = M_FACET_VIEW
					+ HDMConstants.FORWARD_SLASH;
			int lenPrefix = M_FACET_VIEW_SLASH.length();
			this.iFacetEntry = String.valueOf(mapper.getExternalValue(
					mapperName, ENTRY));
			if (this.iFacetEntry.startsWith(M_FACET_VIEW_SLASH)) {
				this.iFacetEntry = this.iFacetEntry.substring(lenPrefix);
			}
			this.iFacetValue = String.valueOf(mapper.getExternalValue(
					mapperName, VALUE));
			if (this.iFacetValue.startsWith(M_FACET_VIEW_SLASH)) {
				this.iFacetValue = this.iFacetValue.substring(lenPrefix);
			}
			this.iFacetName = String.valueOf(mapper.getExternalValue(
					mapperName, NAME));
			if (this.iFacetName.startsWith(M_FACET_VIEW_SLASH)) {
				this.iFacetName = this.iFacetName.substring(lenPrefix);
			}
			Map<Long, SolrSearchAttributeConfig> facetConfigurations = new HashMap();
			Map<Long, SolrSearchAttributeConfig> facetsFromSolrResponse = FacetHelper
					.getFacetInfo(this.iQueryResponse.getFacetFields(),
							this.iSearchProfile, this.iStoreId,
							this.iCurrencyCode, this.iLanguageId);
			if ((facetsFromSolrResponse != null)
					&& (!facetsFromSolrResponse.isEmpty())) {
				facetConfigurations.putAll(facetsFromSolrResponse);
			}
			Map<Long, SolrSearchAttributeConfig> facetQueriesFromSolrResponse = getFacetQueryInfo(this.iQueryResponse
					.getFacetQuery());
			FacetQuery facetQuery = null;
			if ((facetQueriesFromSolrResponse != null)
					&& (!facetQueriesFromSolrResponse.isEmpty())) {
				facetQuery = new FacetQuery(
						this.iQueryResponse.getFacetQuery(),
						facetQueriesFromSolrResponse);

				Iterator localIterator1 = facetQueriesFromSolrResponse
						.entrySet().iterator();
				while (localIterator1.hasNext()) {
					Map.Entry<Long, SolrSearchAttributeConfig> entry = (Map.Entry) localIterator1
							.next();
					facetConfigurations.put((entry.getValue()).getSrchattrId(),
							(SolrSearchAttributeConfig) entry.getValue());
				}
			}
			TreeSet<Long> lnFacetableAttributes = new TreeSet();
			if ((facetConfigurations != null)
					&& (!facetConfigurations.isEmpty())) {
				for (Long key : facetConfigurations.keySet()) {
					if (key != null) {
						lnFacetableAttributes.add(key);
					}
				}
			}
			if (!lnFacetableAttributes.isEmpty()) {
				Object lnSortedFacetableAttributes = new ArrayList();
				try {
					if (lnFacetableAttributes.size() == 1) {
						((List) lnSortedFacetableAttributes)
								.addAll(lnFacetableAttributes);
					} else {
						lnSortedFacetableAttributes = FacetHelper.sortFacets(
								Integer.valueOf(this.iStoreId),
								this.iCategoryId,
								FacetHelper.isKeywordSearch(selectionCriteria),
								lnFacetableAttributes);
					}
				} catch (Exception e) {
					throw new BusinessObjectMediatorException(
							e.getLocalizedMessage(), CLASSNAME, METHODNAME, e);
				}
				Object itrSortedFacetableAttributes = ((List) lnSortedFacetableAttributes)
						.iterator();
				while (((Iterator) itrSortedFacetableAttributes).hasNext()) {
					Long nFacetableAttributeId = (Long) ((Iterator) itrSortedFacetableAttributes)
							.next();

					SolrSearchAttributeConfig solrSearchAttributeConfig = facetConfigurations
							.get(nFacetableAttributeId);
					if (solrSearchAttributeConfig != null) {
						if (facetQueriesFromSolrResponse != null
								&& (facetQueriesFromSolrResponse
										.containsKey(solrSearchAttributeConfig
												.getSrchattrId()))
								&& (facetQuery != null)) {
							List<FacetQuery.FacetNameValueCount> lFacetsFromFacetQuery = facetQuery
									.getFacetQueriesById().get(
											nFacetableAttributeId);
							if (lFacetsFromFacetQuery == null) {
								continue;
							}
							mediateFacetQueryResult(lFacetsFromFacetQuery,
									solrSearchAttributeConfig);

							continue;
						}
						mediateFacet(solrSearchAttributeConfig,
								this.iSearchProfile, this.iCategoryId,
								selectionCriteria);
					}
				}
			}
			sortFacetValues(selectionCriteria);
			if ((this.iCategoryId != null) && (this.iCategoryId.length() > 0)) {
				sortCategoryFacetValues(selectionCriteria);
			}
			this.iSearchResponseObject.getResponse().put(M_FACET_VIEW,
					this.iFacetValues);

			// custom code for price
			List<Map<String, Object>> facetViews = (LinkedList<Map<String, Object>>) iSearchResponseObject
					.getResponse().get("facetView");

			if (facetViews != null) {
				for (Iterator<Map<String, Object>> iter = facetViews.iterator(); iter
						.hasNext();) {
					Map<String, Object> facetView = iter.next();
					String facetName = facetView.get(VALUE) != null ? facetView
							.get(VALUE).toString() : "";
					if (facetName
							.startsWith(HDMConstants.PRICE_FACET_NAME_PREFEIX)) {
						List<Map<String, Object>> facetEntries = (LinkedList<Map<String, Object>>) facetView
								.get("entry");

						for (Iterator<Map<String, Object>> iter1 = facetEntries
								.iterator(); iter1.hasNext();) {
							Map<String, Object> facetEntry = iter1.next();
							String label = (String) facetEntry.get(LABEL);
							facetEntry.put(LABEL, label.replace(".99", ""));

							// Added code to modify uniqueId, to facilitate url
							// sharing with facet selection - Begin
							String value = (String) facetEntry
									.get(HDMConstants.VALUE);
							if (null != value) {
								value = java.net.URLDecoder.decode(value,
										"UTF-8");
								String[] split1 = facetName
										.split(HDMConstants.UNDERSCORE);
								if (split1.length > 2 && null != split1[2]) {
									String replaceStr = HDMConstants.UNDERSCORE
											+ split1[2] + HDMConstants.COLON;
									value = value.replace(replaceStr,
											HDMConstants.COLON);
								}
								String uniqueId = SpecialCharacterHelper
										.convertStrToAscii(value);
								Map<String, Object> extData = (Map<String, Object>) facetEntry
										.get(HDMConstants.EXTENDED_DATA);
								if (null != extData
										&& null != uniqueId
										&& null != extData
												.get(HDMConstants.UNIQUEID)) {
									extData.put(HDMConstants.UNIQUEID, uniqueId);
								}
							}
							// Added code to modify uniqueId - End
						}
					}
				}
			}
			// custom code for price
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * @param selectionCriteria
	 */
	private void sortFacetValues(SelectionCriteria selectionCriteria) {
		final String METHODNAME = "sortFacetValues(SelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, selectionCriteria);
		}
		String isPreview = getControlParameterValue("_wcf.search.internal.preview");
		boolean isPreviewMode = false;
		if ((isPreview != null) && (isPreview.equals(Boolean.TRUE.toString()))) {
			isPreviewMode = true;
		}
		List<String> facetableSearchIndexColumList = new ArrayList();

		List<String> facetableAttributeList = new ArrayList();
		for (Map<String, Object> facet : this.iFacetValues) {
			String facetValue = facet.get(this.iFacetValue).toString();
			if ((facetValue != null) && (facetValue.length() > 0)) {
				String searchColumnName = facetValue.trim().toUpperCase();
				if (searchColumnName.startsWith(STR_AD)) {
					int pos = searchColumnName.indexOf(STR_NTK_CS);
					if (pos > -1) {
						searchColumnName = searchColumnName.substring(0, pos);
					}
					facetableAttributeList.add(searchColumnName);

					facetableSearchIndexColumList.add(facetValue);
				}
			}
		}
		if (LOGGER.isLoggable(Level.FINEST)) {
			LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
					"facetableAttributeList: {0}",
					new Object[] { facetableAttributeList });
			LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
					"facetableSearchIndexColumList: {0}",
					new Object[] { facetableSearchIndexColumList });
		}
		try {
			if (!facetableAttributeList.isEmpty()) {
				Map<String, Map<String, String>> facetPropertyConfiguration = FacetHelper
						.getFacetConfigurationForSearchColumns(Integer
								.valueOf(Integer.parseInt(this.iStoreId)),
								facetableSearchIndexColumList);

				String masterCatalogId = null;
				if (this.iStoreId != null) {
					StoreObject store = StoreRegistry.getInstance().get(
							Integer.valueOf(Integer.parseInt(this.iStoreId)));
					if (store != null) {
						BaseStoreProperty baseStoreProperty = (BaseStoreProperty) store
								.getStoreProperty("masterCatalogId");
						masterCatalogId = baseStoreProperty
								.getMasterCatalogId().toString();
					} else {
						masterCatalogId = RemoteContextServiceFactory
								.getContextAsString("com.ibm.commerce.context.catalog.catalog");
					}
				} else {
					masterCatalogId = RemoteContextServiceFactory
							.getContextAsString("com.ibm.commerce.context.catalog.catalog");
				}
				Map<String, Map> facetValueImageAndSequenceInfo = new HashMap(
						facetableAttributeList.size());

				facetValueImageAndSequenceInfo = FacetHelper
						.fetchAttributeValueImageAndSequence(Integer
								.valueOf(Integer.parseInt(this.iStoreId)),
								Integer.valueOf(Integer
										.parseInt(this.iLanguageId)), Long
										.valueOf(Long
												.parseLong(masterCatalogId)),
								facetableAttributeList);
				if (facetValueImageAndSequenceInfo.size() > 0) {
					updateFacetValue(facetPropertyConfiguration,
							facetValueImageAndSequenceInfo, isPreviewMode,
							selectionCriteria);
				}
			}
		} catch (Exception e) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(
						Level.INFO,
						CLASSNAME,
						METHODNAME,
						"Unable to mediate the IMAGE and SEQUENCE information from the database and update the FacetEntryViewType's.  The error was: {0}",
						new Object[] { ExceptionHelper
								.convertStackTraceToString(e) });
			}
			throw new RuntimeException(e);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * @param facetPropertyConfiguration
	 * @param facetValueImageAndSequenceInfo
	 * @param isPreviewMode
	 * @param selectionCriteria
	 */
	private void updateFacetValue(
			Map<String, Map<String, String>> facetPropertyConfiguration,
			Map<String, Map> facetValueImageAndSequenceInfo,
			boolean isPreviewMode, SelectionCriteria selectionCriteria) {
		final String METHODNAME = "updateFacetValue(Map<String, Map<String, String>>, Map<String, Map>, boolean, SelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					facetPropertyConfiguration, facetValueImageAndSequenceInfo,
					Boolean.valueOf(isPreviewMode), selectionCriteria });
		}
		for (Map<String, Object> facet : this.iFacetValues) {
			String searchColumn = facet.get(this.iFacetValue).toString();
			List<Map<String, Object>> facetEntries = (List) facet
					.get(this.iFacetEntry);

			boolean bProcessAttributeFacet = false;
			boolean bAttributeIsAFloat = false;
			if ((searchColumn != null) && (searchColumn.length() > 0)) {
				String searchColumnNameUpperCase = searchColumn.trim()
						.toUpperCase();
				if (searchColumnNameUpperCase.startsWith(STR_AD)) {
					bProcessAttributeFacet = true;
					if (searchColumnNameUpperCase.startsWith(STR_AD_FLOAT)) {
						bAttributeIsAFloat = true;
					}
				}
			}
			if (bProcessAttributeFacet) {
				Map<String, List> facetValueInfo = (Map) facetValueImageAndSequenceInfo
						.get(searchColumn);

				Map<String, String> facetProperties = facetPropertyConfiguration
						.get(searchColumn);
				boolean sortFacetEntriesByAttributeValueSequence = false;
				if (facetProperties != null) {
					String sortOrder = facetProperties.get("sortorder");
					if ((sortOrder != null) && (sortOrder.equals("2"))) {
						sortFacetEntriesByAttributeValueSequence = true;
					}
				}
				for (Map<String, Object> facetEntry : facetEntries) {
					String facetEntryValue = facetEntry.get(LABEL).toString();
					if ((facetValueInfo != null) && (facetValueInfo.size() > 0)
							&& (facetEntryValue != null)
							&& (facetEntryValue.length() > 0)) {
						if (bAttributeIsAFloat) {
							facetEntryValue = convertFacetValueToSolrFormat(facetEntryValue);
						}
						List<String> facetEntryValueInfo = (List) facetValueInfo
								.get(facetEntryValue);
						if ((LoggingHelper.isTraceEnabled(LOGGER))
								&& (facetEntryValueInfo == null)) {
							LOGGER.logp(
									Level.INFO,
									CLASSNAME,
									METHODNAME,
									"Unable to look up configuration for facet value '{0}' of the facet '{1}'",
									new Object[] { facetEntryValue,
											searchColumn });
						}
						if ((facetEntryValueInfo != null)
								&& (!facetEntryValueInfo.isEmpty())) {
							String facetEntryValueSequence = facetEntryValueInfo
									.get(0);
							String facetEntryValueImage1 = facetEntryValueInfo
									.get(1);
							String facetEntryValueImage2 = facetEntryValueInfo
									.get(2);
							String facetEntryStoreId = facetEntryValueInfo
									.get(3);
							String facetEntryUOMId = facetEntryValueInfo.get(7);
							String facetEntryUOMDescription = facetEntryValueInfo
									.get(8);
							if (facetEntryStoreId != null) {
								Integer attributeValueStoreId = Integer
										.valueOf(Integer
												.parseInt(facetEntryStoreId));
								if (facetEntryValueImage1 != null) {
									facetEntryValueImage1 = StoreHelper
											.getQualifiedImagePath(
													facetEntryValueImage1,
													attributeValueStoreId,
													isPreviewMode,
													selectionCriteria);
								}
								if (facetEntryValueImage2 != null) {
									facetEntryValueImage2 = StoreHelper
											.getQualifiedImagePath(
													facetEntryValueImage2,
													attributeValueStoreId,
													isPreviewMode,
													selectionCriteria);
								}
							}
							Map<String, Object> extendedData = (Map) facetEntry
									.get(EXTENDED_DATA);
							if (facetEntryValueSequence != null) {
								extendedData.put(SEQUENCE,
										facetEntryValueSequence);
							}
							if (facetEntryValueImage1 != null) {
								extendedData.put(IMAGE1, facetEntryValueImage1);
							}
							if (facetEntryValueImage2 != null) {
								extendedData.put(IMAGE2, facetEntryValueImage2);
							}
							if (facetEntryUOMId != null) {
								extendedData.put(UOMID, facetEntryUOMId);
							}
							if (facetEntryUOMDescription != null) {
								extendedData.put(UOMDESCRIPTION,
										facetEntryUOMDescription);
							}
							if ((facetEntryValueImage1 != null)
									&& (facetEntryValueImage1.length() > 0)) {
								facetEntry.put(IMAGE, facetEntryValueImage1);
							} else if ((facetEntryValueImage2 != null)
									&& (facetEntryValueImage2.length() > 0)) {
								facetEntry.put(IMAGE, facetEntryValueImage2);
							}
							if (facetEntryValueInfo.size() > 6) {
								populateExtendedData(extendedData, FIELD1,
										(String) facetEntryValueInfo.get(4));
								populateExtendedData(extendedData, FIELD2,
										(String) facetEntryValueInfo.get(5));
								populateExtendedData(extendedData, FIELD3,
										(String) facetEntryValueInfo.get(6));
							}
						}
					}
				}
				if (sortFacetEntriesByAttributeValueSequence) {
					sortFacetEntriesByAttributeValueSequence(facetEntries);
					trimFacetValues(facetEntries, searchColumn);
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * @param selectionCriteria
	 * @throws AbstractApplicationException
	 */
	private void sortCategoryFacetValues(SelectionCriteria selectionCriteria)
			throws AbstractApplicationException {
		final String METHODNAME = "sortCategoryFacetValues(SelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, selectionCriteria);
		}
		for (Map<String, Object> facet : this.iFacetValues) {
			String searchColumn = facet.get(this.iFacetValue).toString();
			if (StringUtils.isNotBlank(searchColumn)
					&& searchColumn.startsWith("parentCatgroup_id_")) {
				String displaySequence = FacetHelper
						.getCategoryFacetSortOrder();
				List<Map<String, Object>> facetEntries = (List) facet
						.get(this.iFacetEntry);
				Map<String, Object> facetEntry;
				if (displaySequence.equals("1")) {
					String localeName = (String) ValueMappingService
							.getInstance("com.ibm.commerce.foundation")
							.getExternalValue(LANGUAGE_ID, this.iLanguageId);
					Collator collator = Collator.getInstance(new Locale(
							localeName));
					Map<String, Map<String, Object>> sortedCategoryFacetEntries = new TreeMap(
							collator);
					for (Iterator localIterator2 = facetEntries.iterator(); localIterator2
							.hasNext();) {
						facetEntry = (Map) localIterator2.next();
						String label = (String) facetEntry.get(LABEL);
						String key = label.concat(HDMConstants.UNDERSCORE)
								.concat((String) facetEntry.get(VALUE));
						sortedCategoryFacetEntries.put(key, facetEntry);
					}
					facet.put(this.iFacetEntry, new LinkedList(
							sortedCategoryFacetEntries.values()));
				} else if (displaySequence.equals("2")) {
					List<String> categoryIds = HierarchyHelper
							.getAllSubCategories(this.iCategoryId,
									new SearchCriteria(selectionCriteria));
					Map<Integer, Map<String, Object>> sortedCategoryFacetEntries = new TreeMap();
					for (Map<String, Object> facetEntry1 : facetEntries) {
						String categoryId = (String) facetEntry1.get(VALUE);
						int i = categoryIds.indexOf(categoryId);
						if (i >= 0) {
							sortedCategoryFacetEntries.put(Integer.valueOf(i),
									facetEntry1);
						}
					}
					facet.put(this.iFacetEntry, new LinkedList(
							sortedCategoryFacetEntries.values()));
				}
				if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
					LOGGER.exiting(CLASSNAME, METHODNAME);
				}
				return;
			}
		}
		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(
					Level.WARNING,
					CLASSNAME,
					METHODNAME,
					"Cannot locate category facet in response.  Skipping category facet re-sequencing.");
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * @param amapExtendedData
	 * @param astrName
	 * @param astrValue
	 */
	private void populateExtendedData(Map<String, Object> amapExtendedData,
			String astrName, String astrValue) {
		if ((amapExtendedData != null) && (astrName != null)
				&& (astrValue != null) && (astrValue.length() > 0)) {
			amapExtendedData.put(astrName, astrValue);
		}
	}

	/**
	 * @param alFacetValues
	 * @param astrSearchColumnName
	 */
	private void trimFacetValues(List<Map<String, Object>> alFacetValues,
			String astrSearchColumnName) {
		final String METHOD_NAME = "trimFacetValues(List<Map<String, Object>>, String)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHOD_NAME, new Object[] {
					alFacetValues, astrSearchColumnName });
		}
		int nMaxDisplay = SearchQueryHelper
				.getFacetConstraintLimit(astrSearchColumnName);
		if ((nMaxDisplay > 0) && (nMaxDisplay < alFacetValues.size())) {
			List<Map<String, Object>> facetValues = new ArrayList(nMaxDisplay);
			facetValues.addAll(alFacetValues.subList(0, nMaxDisplay));
			alFacetValues.clear();
			alFacetValues.addAll(facetValues);
		}
		if (LoggingHelper.isTraceEnabled(LOGGER)) {
			LOGGER.logp(Level.INFO, CLASSNAME, METHOD_NAME,
					"Displaying {0} facet values: {1}", new Object[] {
							nMaxDisplay, alFacetValues });
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHOD_NAME);
		}
	}

	/**
	 * @param aFacetProperties
	 * @return nMaxDisplay
	 */
	private Integer getMaxDisplay(Map<String, String> aFacetProperties) {
		final String METHOD_NAME = "getMaxDispay(Map<String, String> )";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHOD_NAME, aFacetProperties);
		}
		Integer nMaxDisplay = Integer.valueOf(-1);
		String strMaxDisplay = aFacetProperties.get(STR_MAX_DISPLAY);
		try {
			nMaxDisplay = Integer.valueOf(strMaxDisplay);
		} catch (NumberFormatException ex) {
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				LOGGER.logp(Level.INFO, CLASSNAME, METHOD_NAME,
						"Unable to convert max display limit to integer: {0}",
						new Object[] { strMaxDisplay });
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHOD_NAME, nMaxDisplay);
		}
		return nMaxDisplay;
	}

	/**
	 * @param facetEntries
	 */
	private void sortFacetEntriesByAttributeValueSequence(
			List<Map<String, Object>> facetEntries) {
		final String METHODNAME = "sortFacetEntriesByAttributeValueSequence(List<Map<String, Object>>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, facetEntries);
		}
		if (LOGGER.isLoggable(Level.FINEST)) {
			LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
					"Facet entries before sorting: ({0}) {1}", new Object[] {
							facetEntries.size(), facetEntries });
		}
		List<Map<String, Object>> sortedFacetEntryList = new ArrayList();
		sortedFacetEntryList.addAll(facetEntries);

		Collections.sort(sortedFacetEntryList,
				new FacetEntrySequenceComparator());
		if (LOGGER.isLoggable(Level.FINEST)) {
			LOGGER.logp(Level.FINEST, CLASSNAME, METHODNAME,
					"Facet entries after sorting: ({0}) {1}", new Object[] {
							sortedFacetEntryList.size(), sortedFacetEntryList });
		}
		facetEntries.clear();
		facetEntries.addAll(sortedFacetEntryList);
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * custom logic to include multiple value selection flag in extended data
	 * field
	 * 
	 * @param alFacetsFromFacetQuery
	 * @param aSolrSearchAttributeConfig
	 * @throws NumberFormatException
	 * @throws FoundationApplicationException
	 */
	private void mediateFacetQueryResult(
			List<FacetQuery.FacetNameValueCount> alFacetsFromFacetQuery,
			SolrSearchAttributeConfig aSolrSearchAttributeConfig)
			throws NumberFormatException, FoundationApplicationException {
		final String METHODNAME = "mediateFacetQueryResult(List<FacetNameValueCount>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, alFacetsFromFacetQuery);
		}
		Iterator<FacetQuery.FacetNameValueCount> itrFacets = alFacetsFromFacetQuery
				.iterator();

		List<Map<String, Object>> lFacetEntryValues = new LinkedList();
		Map<String, Object> facetValue = new LinkedHashMap();
		Map<String, Object> facetEntry = null;
		this.iFacetValues.add(facetValue);
		if (itrFacets.hasNext()) {
			FacetQuery.FacetNameValueCount facetInfo = itrFacets.next();
			facetValue.put(this.iFacetValue, facetInfo.getFacetName());
			facetValue.put(this.iFacetName, facetInfo.getFacetName());

			facetEntry = addFacetEntry(facetInfo);
			lFacetEntryValues.add(facetEntry);
		}

		// extendedData for price facet is not populated by default
		// custom code to add extended data info to price facet with selection
		// flag - starts
		Integer selection = aSolrSearchAttributeConfig.getSelection();
		String strIsMultiSelection = FALSE;
		if (selection != null) {
			strIsMultiSelection = selection.equals(Integer.valueOf(1)) ? TRUE
					: FALSE;
		}

		Map<String, Object> extendedData = new LinkedHashMap();
		facetValue.put(EXTENDED_DATA, extendedData);

		extendedData.put(ATTRIBUTE_FACET_ALLOW_MUTIPLE_VALUE_SELECTION,
				strIsMultiSelection);
		// custom code to add extended data info to price facet with selection
		// flag - ends

		while (itrFacets.hasNext()) {
			FacetQuery.FacetNameValueCount facetInfo = itrFacets.next();
			facetEntry = addFacetEntry(facetInfo);
			lFacetEntryValues.add(facetEntry);
		}
		facetValue.put(this.iFacetEntry, lFacetEntryValues);
		if ((facetValue != null) && (facetValue.size() > 0)) {
			String facetGroupValue = facetValue.get(this.iFacetName).toString();
			String compatiblePriceIndexMode = StoreHelper
					.getCompatiblePriceIndexMode(this.iStoreId);
			if (LoggingHelper.isTraceEnabled(LOGGER)) {
				if (compatiblePriceIndexMode != null) {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"compatiblePriceIndexMode: {0}",
							new Object[] { compatiblePriceIndexMode });
				} else {
					LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
							"Not define compatiblePriceIndexMode, will use compatible mode");
				}
			}
			if ((this.iFacetName
					.startsWith(HDMConstants.PRICE_FACET_NAME_PREFEIX))
					&& (compatiblePriceIndexMode != null)
					&& ("1.0".equals(compatiblePriceIndexMode))) {
				String iCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
				String priceCurrencyPrefix = HDMConstants.PRICE_FACET_NAME_PREFEIX
						.concat(iCurrencyCode);
				facetGroupValue = priceCurrencyPrefix;
			}
			SolrSearchConfigurationRegistry searchRegistry = SolrSearchConfigurationRegistry
					.getInstance(this.iSelectionCriteria.getComponentId());

			String showName = searchRegistry
					.getIndexShowName(Integer.valueOf(this.iStoreId),
							Integer.valueOf(this.iLanguageId), facetGroupValue,
							"facet");
			if (showName == null) {
				showName = searchRegistry.getIndexShowName(
						Integer.valueOf(this.iStoreId),
						Integer.valueOf(this.iLanguageId), facetGroupValue,
						"facet-range");
			}
			if (showName == null) {
				showName = searchRegistry.getIndexShowName(
						Integer.valueOf(this.iStoreId),
						Integer.valueOf(this.iLanguageId), facetGroupValue,
						"facet-dynamicrange");
			}
			facetValue.put(this.iFacetName, showName);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * @param facetInfo
	 * @return facetEntry
	 */
	private Map<String, Object> addFacetEntry(
			FacetQuery.FacetNameValueCount facetInfo) {
		final String METHODNAME = "addFacetEntry(FacetNameValueCount)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, facetInfo);
		}
		String strFacetName = facetInfo.getFacetName();
		String strFacetEntryLabel = facetInfo.getFacetValue();
		Integer nCount = facetInfo.getCount();
		Map<String, Object> facetEntry = new HashMap();
		if (nCount != null) {
			facetEntry.put(COUNT, nCount);
			facetEntry.put(LABEL, strFacetEntryLabel);
			Map<String, Object> extendedData = new HashMap();
			facetEntry.put(EXTENDED_DATA, extendedData);

			String strExpression = strFacetName + ":" + strFacetEntryLabel;
			String uniqueId = SpecialCharacterHelper
					.convertStrToAscii(strExpression);

			extendedData.put(UNIQUE_ID, uniqueId);
			try {
				facetEntry.put(this.iFacetValue,
						URLEncoder.encode(strExpression, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				facetEntry.put(this.iFacetValue, strExpression);
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, facetEntry);
		}
		return facetEntry;
	}

	/**
	 * custom logic to invoke custom facet helper for category facet
	 * 
	 * @param aSolrSearchAttributeConfig
	 * @param astrSearchProfile
	 * @param astrCategory
	 * @param selectionCriteria
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	private void mediateFacet(
			SolrSearchAttributeConfig aSolrSearchAttributeConfig,
			String astrSearchProfile, String astrCategory,
			SelectionCriteria selectionCriteria) throws NumberFormatException,
			Exception {
		final String METHODNAME = "mediateFacet(SolrSearchAttributeConfig,String,String,SelectionCriteria)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					aSolrSearchAttributeConfig, astrSearchProfile,
					astrCategory, selectionCriteria });
		}
		FacetField facetField = aSolrSearchAttributeConfig.getFacetField();
		String facetName = facetField.getName();
		if ((facetName.equals("parentCatgroup_id_facet"))
				&& (FacetHelper.displayLeafCategoriesOnly())) {
			facetName = "parentCatgroup_id_search";
		}
		int nFacetConstraintLimit = SearchQueryHelper
				.getFacetConstraintLimit(facetName);
		boolean bSkipLastValue = false;
		boolean bAllValuesReturned = true;
		if ((nFacetConstraintLimit > -1)
				&& (facetField.getValueCount() > nFacetConstraintLimit)) {
			bAllValuesReturned = false;
			if (!FacetHelper.sortValuesBySequence(aSolrSearchAttributeConfig,
					Integer.parseInt(this.iStoreId))) {
				bSkipLastValue = true;
			}
		}
		// the default OOB FacetHelper gets only child categories, where as we
		// required to fetch current and parent categories a well. Hence OOB
		// FacetHelper logic validateCategory() to fetch only child categories
		// is modified to fetch parent and current categories
		List<Map<String, Object>> facetEntries = HDMFacetHelper
				.facetValues(aSolrSearchAttributeConfig,
						Boolean.valueOf(bSkipLastValue), astrCategory,
						this.iCatalogId, this.iStoreId, selectionCriteria);
		if ((facetEntries == null) || (facetEntries.isEmpty())) {
			if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
				LOGGER.exiting(CLASSNAME, METHODNAME, facetEntries);
			}
			return;
		}
		Map<String, Object> facetValue = new LinkedHashMap();
		facetValue.put(this.iFacetValue, facetName);
		facetValue.put(this.iFacetEntry, facetEntries);
		this.iFacetValues.add(facetValue);

		Integer selection = aSolrSearchAttributeConfig.getSelection();
		String strIsMultiSelection = FALSE;
		if (selection != null) {
			strIsMultiSelection = selection.equals(Integer.valueOf(1)) ? TRUE
					: FALSE;
		}
		facetValue.put(this.iFacetName, FacetHelper.getFacetShowName(facetName,
				this.iSearchRegistry, this.iStoreId, this.iLanguageId));

		Map<String, Object> extendedData = new LinkedHashMap();
		facetValue.put(EXTENDED_DATA, extendedData);

		extendedData.put(ATTRIBUTE_FACET_ALLOW_MUTIPLE_VALUE_SELECTION,
				strIsMultiSelection);
		extendedData.put(ATTRIBUTE_FACET_MAX_DISPLAY,
				String.valueOf(aSolrSearchAttributeConfig.getMaxDisplay()));
		extendedData.put(ATTRIBUTE_MERCHANDISING_FACET_GROUP_ID,
				String.valueOf(aSolrSearchAttributeConfig.getGroupId()));
		extendedData.put(ATTRIBUTE_FACET_ALL_VALUES_RETURNED,
				Boolean.valueOf(bAllValuesReturned));

		Map<String, String> customFacetProperties = FacetHelper
				.getFacetConfigurationForSearchColumn("CatalogEntry",
						this.iSearchProfile, Integer.valueOf(this.iStoreId),
						this.iCurrencyCode, this.iLanguageId, facetName);
		customFacetProperties = removeNullValues(customFacetProperties);
		Iterator it = customFacetProperties.keySet().iterator();
		int size = customFacetProperties.size();
		for (int i = 0; i < size; i++) {
			String key = (String) it.next();
			String value = String.valueOf(customFacetProperties.get(key));
			extendedData.put(key, value);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

	/**
	 * @param facetQueryMap
	 * @return map
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	private Map<Long, SolrSearchAttributeConfig> getFacetQueryInfo(
			Map<String, Integer> facetQueryMap) throws NumberFormatException,
			Exception {
		final String METHODNAME = "getFacetQueryInfo(Map<String, Integer>)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, facetQueryMap);
		}
		Map<Long, SolrSearchAttributeConfig> map = new LinkedHashMap();
		if ((facetQueryMap != null) && (facetQueryMap.size() != 0)) {
			long nSrchAttIdOffset = 0L;
			Set<String> fieldNamesSet = new HashSet();
			for (Map.Entry<String, Integer> entry : facetQueryMap.entrySet()) {
				String strFacetQuery = entry.getKey();
				String[] arrstrFacetQuery = strFacetQuery.split(":", 2);
				if (arrstrFacetQuery.length == 2) {
					arrstrFacetQuery[0] = FacetHelper
							.extractFacetQueryFromExcludedFacetQuery(arrstrFacetQuery[0]);
					if (!fieldNamesSet.contains(arrstrFacetQuery[0])) {
						Map<String, String> facetProperties = FacetHelper
								.getFacetConfigurationForSearchColumn(
										"CatalogEntry", this.iSearchProfile,
										Integer.valueOf(this.iStoreId),
										this.iCurrencyCode, this.iLanguageId,
										arrstrFacetQuery[0]);
						if ((facetProperties != null)
								&& (!facetProperties.isEmpty())) {
							Long srchattrId = null;
							String tempStr = facetProperties
									.get(STR_SRCHATTR_ID);
							if (StringUtils.isNotBlank(tempStr)) {
								srchattrId = Long.valueOf(tempStr);
							}
							Integer facetSelection = null;
							tempStr = facetProperties.get(STR_SELECTION);
							if (StringUtils.isNotBlank(tempStr)) {
								facetSelection = Integer.valueOf(tempStr);
							}
							Integer facetMaxDisplay = null;
							tempStr = facetProperties.get(STR_MAX_DISPLAY);
							if (StringUtils.isNotBlank(tempStr)) {
								facetMaxDisplay = Integer.valueOf(tempStr);
							}
							Long facetGroupId = null;
							tempStr = facetProperties.get(STR_GROUP_ID);
							if (StringUtils.isNotBlank(tempStr)) {
								facetGroupId = Long.valueOf(tempStr);
							}
							SolrSearchAttributeConfig attr = new SolrSearchAttributeConfig(
									null, null, null, srchattrId,
									facetSelection, facetMaxDisplay,
									facetGroupId);
							FacetField facetField = new FacetField(
									arrstrFacetQuery[0]);
							attr.setFacetField(facetField);
							if (map.containsKey(srchattrId)) {
								map.put(Long.valueOf(Long.MIN_VALUE
										+ nSrchAttIdOffset++), attr);
							} else {
								map.put(srchattrId, attr);
							}
							fieldNamesSet.add(arrstrFacetQuery[0]);
						}
					}
				}
			}
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, map);
		}
		return map;
	}

	/**
	 * @param astrFacetFieldName
	 * @return strShowName
	 */
	private String getFacetShowName(String astrFacetFieldName) {
		final String METHODNAME = "getFacetShowName(String)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, astrFacetFieldName);
		}
		String strShowName = null;
		try {
			strShowName = this.iSearchRegistry.getIndexShowName(
					Integer.valueOf(this.iStoreId),
					Integer.valueOf(this.iLanguageId), astrFacetFieldName,
					"facet");
			if (strShowName == null) {
				strShowName = this.iSearchRegistry.getIndexShowName(
						Integer.valueOf(this.iStoreId),
						Integer.valueOf(this.iLanguageId), astrFacetFieldName,
						"facet-classicAttribute");
			}
		} catch (FoundationApplicationException ex) {
			throw new RuntimeException(ex);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, strShowName);
		}
		return strShowName;
	}

	/**
	 * @param aMap
	 * @return mapWithoutNullValues
	 */
	private Map removeNullValues(Map aMap) {
		Map mapWithoutNullValues = new HashMap(aMap.size());
		Iterator<String> itrKeys = aMap.keySet().iterator();
		while (itrKeys.hasNext()) {
			String strKey = itrKeys.next();
			Object value = aMap.get(strKey);
			if (value != null) {
				mapWithoutNullValues.put(strKey, value);
			}
		}
		return mapWithoutNullValues;
	}

	/**
	 * @param astrFacetValue
	 * @return strFacetValue
	 */
	private String convertFacetValueToSolrFormat(String astrFacetValue) {
		final String METHODNAME = "convertFacetValueToSolrFormat(String)";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, astrFacetValue);
		}
		String strFacetValue = astrFacetValue;
		Double fFacetEntryValue = Double.valueOf(astrFacetValue);
		double fFacetEntryDoubleValue = fFacetEntryValue.doubleValue();
		int nFacetEntryIntValue = fFacetEntryValue.intValue();
		if (fFacetEntryDoubleValue == nFacetEntryIntValue) {
			strFacetValue = String.valueOf(nFacetEntryIntValue);
		}
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME, strFacetValue);
		}
		return strFacetValue;
	}
}
