package com.hdm.commerce.foundation.server.services.rest.search.preprocessor.solr;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.FacetHelper;
import com.ibm.commerce.foundation.server.services.dataaccess.SelectionCriteria;
import com.ibm.commerce.foundation.server.services.search.query.SearchQueryPreprocessor;
import com.ibm.commerce.foundation.server.services.search.query.solr.AbstractSolrSearchQueryPreprocessor;

public class HDMSolrRESTContentFacetQueryPreprocessor extends
		AbstractSolrSearchQueryPreprocessor implements SearchQueryPreprocessor {

	private static final String CLASSNAME = HDMSolrRESTContentFacetQueryPreprocessor.class
			.getName();
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMSolrRESTContentFacetQueryPreprocessor.class);
	private String iComponentId = null, facetName;

	public HDMSolrRESTContentFacetQueryPreprocessor(String componentId) {
		this.iComponentId = componentId;
	}

	public void removeFacetTerms(String facetField) {
		iSolrQuery.remove("f." + facetField + ".facet.mincount");
		iSolrQuery.remove("f." + facetField + ".facet.limit");
		iSolrQuery.remove("f." + facetField + ".facet.sort");
	}

	public void invoke(SelectionCriteria selectionCriteria,
			Object... queryRequestObjects) throws RuntimeException {
		final String METHODNAME = "invoke(SelectionCriteria, Object[])";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME, new Object[] {
					selectionCriteria, queryRequestObjects });
		}
		super.invoke(selectionCriteria, queryRequestObjects);

		final String strSearchProfile = getControlParameterValue(HDMConstants._WCF_SEARCH_PROFILE);
		final String strCurrencyCode = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CURRENCY);
		final String languageId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_LANGUAGE);
		final String storeId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_STORE_ONLINE);
		final String indexName = HDMConstants.CATALOGENTRY;
		final String strCategory = getControlParameterValue(HDMConstants._WCF_SEARCH_CATEGORY);
		final String strTerm = getControlParameterValue(HDMConstants._WCF_SEARCH_TERM);
		final String strCatalogId = getFinalControlParameterValue(HDMConstants._WCF_SEARCH_CATALOG);

		Map<String, Map<String, String>> facetProperties = new HashMap();
		Set<Entry<String, Map<String, String>>> facetPropertyValues = null;

		try {
			if ((strCategory != null) && (strCategory.length() > 0)
					&& ((strTerm == null) || (strTerm.equals("")))) {
				facetProperties.putAll(FacetHelper
						.getFacetConfigurationForCategory(strCategory,
								languageId, indexName, strSearchProfile,
								Boolean.valueOf(true),
								Integer.valueOf(storeId), strCurrencyCode,
								strCatalogId));
			} else {
				facetProperties
						.putAll(FacetHelper
								.getFacetConfigurationForKeywordSearch(
										indexName, languageId,
										Integer.valueOf(storeId),
										strSearchProfile, strCurrencyCode,
										strCategory));

			}

			if (LoggingHelper.isTraceEnabled(LOGGER)) {

				LOGGER.logp(Level.INFO, CLASSNAME, METHODNAME,
						"facetProperties:::{0}",
						java.util.Arrays.asList(facetProperties));

			}

			/* Remove facet fields from solr request for content products */
			if (facetProperties != null && facetProperties.size() > 0) {
				facetPropertyValues = facetProperties.entrySet();
				Iterator<Entry<String, Map<String, String>>> iterator = facetPropertyValues
						.iterator();
				while (iterator.hasNext()) {
					Entry<String, Map<String, String>> entry = iterator.next();
					Map<String, String> valueMap = entry.getValue();
					if (valueMap.get("attridentifier") != null
							&& valueMap.get("attridentifier").startsWith(
									HDMConstants.CONTENT_FACET_NAME)) {
						facetName = entry.getKey();
					}
				}
			}

			if (LoggingHelper.isTraceEnabled(LOGGER)) {

				if (facetPropertyValues != null) {
					LOGGER.logp(
							Level.INFO,
							CLASSNAME,
							METHODNAME,
							"facetPropertyValues:::{0}",
							new Object[] { Arrays
									.asList(new Set[] { facetPropertyValues }) });
				}

			}

			String arrayFacetFields[] = iSolrQuery.getFacetFields();
			for (int i = 0; i < arrayFacetFields.length; i++) {
				boolean removeFacet = true;
				String value = arrayFacetFields[i];

				if (null != value && (value.equals("parentCatgroup_id_search"))) {
					removeFacet = false;
				} else if (null != value && (value.equals(facetName))) {
					removeFacet = false;
				}

				if (removeFacet) {
					iSolrQuery.removeFacetField(value);
					removeFacetTerms(value);
				}

			}

			/* Remove facet query from solr request for content products */
			String arrayFacetQuery[] = iSolrQuery.getFacetQuery();
			for (int i = 0; i < arrayFacetQuery.length; i++) {
				iSolrQuery.removeFacetQuery(arrayFacetQuery[i]);
			}
			String arrayPriceFacetField[] = arrayFacetQuery[0].split(":");
			String priceFacetField = arrayPriceFacetField[0];
			removeFacetTerms(priceFacetField);

		}

		catch (Exception e) {
			LOGGER.logp(Level.SEVERE, CLASSNAME, METHODNAME, e.getMessage(), e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME, METHODNAME);
		}
	}

}
