package com.hdm.commerce.search.utils;

import java.util.logging.Logger;

import javax.ws.rs.core.Cookie;

import org.apache.wink.server.handlers.MessageContext;

import com.hdm.commerce.constants.HDMConstants;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.internal.server.services.search.util.RemoteRestCallHelper;

public class HDMUtils {
	private static final String CLASS_NAME = HDMUtils.class.getName();
	private static final Logger LOGGER = LoggingHelper.getLogger(CLASS_NAME);

	public static String getCookieValue(final String cookieName) {
		final String METHOD_NAME = "getCookieValue";
		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASS_NAME, METHOD_NAME, cookieName);
		}

		MessageContext messageContext = RemoteRestCallHelper
				.getRequestMessageContext();
		Cookie cookie = messageContext.getHttpHeaders().getCookies()
				.get(cookieName);

		String cookieValue = null;
		if (null != cookie) {
			cookieValue = cookie.getValue();
		}
		if (cookieName.contains(HDMConstants.PHYSICALSTORE_COOKIENAME)
				&& cookieValue == null) {
			cookieValue = "8702";
		} else if (cookieName.contains(HDMConstants.MARKETID_COOKIENAME)
				&& cookieValue == null) {
			cookieValue = "21";
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASS_NAME, METHOD_NAME, cookieValue);
		}
		return cookieValue;
	}
}
