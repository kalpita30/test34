package com.hdm.commerce.search.handler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;

import com.hdm.commerce.store.facade.datatypes.PhysicalStoreGeoCodeType;
import com.hdm.commerce.store.facade.server.util.DistanceCalculationHelper;
import com.ibm.commerce.foundation.common.util.logging.LoggingHelper;
import com.ibm.commerce.foundation.server.services.rest.search.SearchCriteria;
import com.ibm.commerce.rest.javadoc.AdditionalParameterList;
import com.ibm.commerce.rest.javadoc.Description;
import com.ibm.commerce.rest.javadoc.ParameterDescription;
import com.ibm.commerce.rest.javadoc.ResponseSchema;
import com.ibm.commerce.rest.search.handler.v2_0.AbstractSearchResourceHandler;

@Description("This class provides RESTful services to get product data for search-based storelocator.")
@Path("store/{storeId}/storeview")
@Encoded
public class HDMStoreLocatorHandler extends AbstractSearchResourceHandler {
	private static final Logger LOGGER = LoggingHelper
			.getLogger(HDMStoreLocatorHandler.class);
	private static final String CLASSNAME = HDMStoreLocatorHandler.class
			.getName();

	@GET
	@Path("byStoreIdentifier/{storeIdentifier}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets matching for given store")
	@AdditionalParameterList({
			@ParameterDescription(description = "The store identifier", required = false, name = "storeIdentifier"),
			@ParameterDescription(description = "The currency code to use. Example usage : currency=USD. If no currency code is specified, the store default currency shall be used.", required = false, name = "currency", valueProviderClass = com.ibm.commerce.rest.javadoc.CurrencyIdProvider.class) })
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findStoreByIdentifier(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@PathParam("storeIdentifier") @ParameterDescription(description = "The store identifier.", required = true) String storeIdentifier,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStoreByIdentifier(String, String)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findStoreByIdentifier(String,int,String, List<String>)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		try {
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byStoreIdentifier/{storeIdentifier}");

			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:1 AND physicalstore_id:"
							+ storeIdentifier);
			
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findStoreByIdentifier(String,int,String, List<String>)");
		}
		return result;

	}
	
	@GET
	@Path("byStoreId/{storelocId}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets matching for given store")
	@AdditionalParameterList({
			@ParameterDescription(description = "The store identifier", required = false, name = "storelocId"),
			@ParameterDescription(description = "The currency code to use. Example usage : currency=USD. If no currency code is specified, the store default currency shall be used.", required = false, name = "currency", valueProviderClass = com.ibm.commerce.rest.javadoc.CurrencyIdProvider.class) })
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findStoreById(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,

			@PathParam("storelocId") @ParameterDescription(description = "The store identifier.", required = true) String storelocId,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStoreById(String, String)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findStoreById(String,int,String, List<String>)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		try {
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byStoreId/{storelocId}");

			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:1 AND stloc_id:"
							+ storelocId);
			
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findStoreById(String,int,String, List<String>)");
		}
		return result;

	}
	
	@GET
	@Path("byStoreIdentifiers")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets matching for given store")
	@AdditionalParameterList({
			@ParameterDescription(description = "The store identifier", required = false, name = "physicalStoreId"),
			})
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findStoreByIdentifier(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@QueryParam("physicalStoreId") @ParameterDescription(description="A list of physical store unique identifiers.", required = true, multipleValue = true) List<String> physicalStoreId	,		

			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {

		String METHODNAME = "findStoreByIdentifier(String, String)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findStoreByIdentifier(String,int,String, List<String>)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		try {
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byStoreIdentifiers");

			throwRestExceptionIfErrorsAreDetected();
			String delim = " OR ";

			String storeList = StringUtils.join(physicalStoreId, delim);
			if(!StringUtils.isEmpty(storeList)){
				storeList= storeList.replace("+", "");
			}
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:1 AND stloc_id: ("
							+ storeList + ")");
			
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findStoreByIdentifier(String,int,String, List<String>)");
		}
		return result;

	}
	
	@GET
	@Path("byStateIdentifier/{stateIdentifier}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets matching for given store")
	@AdditionalParameterList({
			@ParameterDescription(description = "The state identifier", required = false, name = "stateIdentifier") })
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findStoreByStateIdentifier(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@PathParam("stateIdentifier") @ParameterDescription(description = "The state identifier.", required = true) String stateidentifier,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStoreByIdentifier(String, String, int, int, String, String, List<String>)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findStoreByStateIdentifier(String,int,String, List<String>)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		try {
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byStateIdentifier/{stateIdentifier}");

			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:1 AND stategeonode_id:"
							+ stateidentifier);
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.sort", "physicalstore_id asc"
							);
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findStoreByStateIdentifier(String,int,String, List<String>)");
		}
		return result;

	}
	
	@GET
	@Path("byMarketIdentifier/{marketIdentifier}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets matching for given store")
	@AdditionalParameterList({
			@ParameterDescription(description = "The market identifier", required = false, name = "storeIdentifier") })
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findStoreByMarketIdentifier(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@PathParam("marketIdentifier") @ParameterDescription(description = "The market identifier.", required = true) String marketIdentifier,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStoreByIdentifier(String, String, int, int, String, String, List<String>)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findStoreByMarketIdentifier(String,int,String, List<String>)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		try {
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byMarketIdentifier/{marketIdentifier}");

			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:1 AND market_id:"
							+ marketIdentifier);
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findStoreByStateIdentifier(String,int,String, List<String>)");
		}
		return result;

	}
	
	@GET
	@Path("byStates/@top")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets All states")
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findAllState(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStoreByIdentifier(String, String, int, int, String, String, List<String>)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findStoreByStateIdentifier(String,int,String, List<String>)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		try {
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byStates/@top");

			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:1");

			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findStoreByStateIdentifier(String,int,String, List<String>)");
		}
		return result;

	}
	
	@GET
	@Path("byGeoNode/latitude/{latitude}/longitude/{longitude}/range/{range}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets All states")
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findNearestStore(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@PathParam("latitude") @ParameterDescription(description = "The latitude of the user", required = true) BigDecimal latitude,
			@PathParam("longitude") @ParameterDescription(description = "The longitude of the user", required = true) BigDecimal longitude,
			@PathParam("range") @ParameterDescription(description = "The range in KM", required = true) double radiusInKm,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStoreByIdentifier(String, String, int, int, String, String, List<String>)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findNearestStore(integer,String, String)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		try {
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byGeoNode/latitude/{latitude}/longitude/{longitude}/range/{range}");
			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:1");
			PhysicalStoreGeoCodeType geoCode= new PhysicalStoreGeoCodeType();
			geoCode.setLatitude(latitude);
			geoCode.setLongitude(longitude);
			PhysicalStoreGeoCodeType nwGeoCode = DistanceCalculationHelper
					.getNorthWestGeoCode(geoCode, radiusInKm);
			PhysicalStoreGeoCodeType seGeoCode = DistanceCalculationHelper
					.getSouthEastGeoCode(geoCode, radiusInKm);
			nwGeoCode = DistanceCalculationHelper.roundGeoCode(nwGeoCode);
			seGeoCode = DistanceCalculationHelper.roundGeoCode(seGeoCode);
			
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.filterquery", "latitude:["+seGeoCode.getLatitude()+" TO "+ nwGeoCode.getLatitude()+"] AND longitude:["+nwGeoCode.getLongitude()+" TO "+ seGeoCode.getLongitude()+"]");

			result = performSearch(searchCriteria);
			
			
			
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findNearestStore(integer,String, String)");
		}
		return result;

	}
	
	
	@GET
	@Path("byDistance/latitude/{latitude}/longitude/{longitude}/distance/{distance}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets All states")
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findNearbyStoreBySortedDistance(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@PathParam("latitude") @ParameterDescription(description = "The latitude of the user", required = true) BigDecimal latitude,
			@PathParam("longitude") @ParameterDescription(description = "The longitude of the user", required = true) BigDecimal longitude,
			@PathParam("distance") @ParameterDescription(description = "The range in KM", required = true) double radiusInKm,
			@PathParam("pages") @ParameterDescription(description = "numberofstores", required = true) double page,

			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findNearbyStoreBySortedDistance(String, String, int, int, String, String, List<String>)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findNearestStore(integer,String, String)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		
		try {	
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byDistance/latitude/{latitude}/longitude/{longitude}/distance/{distance}");
			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue("_wcf.search.internal.filterquery", "isreal:1");
			searchCriteria.setControlParameterValue("_wcf.search.internal.mandatory.query", "{!geofilt score=distance sfield=location pt="+latitude+","+longitude+" d="+radiusInKm+"}");
			searchCriteria.setControlParameterValue("_wcf.search.internal.response.fields", "*,score");
			searchCriteria.setControlParameterValue("_wcf.search.internal.sort", "score asc");
			
			
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findNearbyStoreBySortedDistance(integer,String, String)");
		}
		return result;

	}
	
	@GET
	@Path("byDistance/latitude/{latitude}/longitude/{longitude}/limit/{limit}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets All states")
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findNearestStoreBySortedDistance(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@PathParam("latitude") @ParameterDescription(description = "The latitude of the user", required = true) BigDecimal latitude,
			@PathParam("longitude") @ParameterDescription(description = "The longitude of the user", required = true) BigDecimal longitude,
			@PathParam("limit") @ParameterDescription(description = "numberofstores", required = true) BigInteger limit,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findNearestStoreBySortedDistance(String, String, int, int, String, String, List<String>)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findNearestStore(integer,String, String)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		
		try {	
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byDistance/latitude/{latitude}/longitude/{longitude}/limit/{limit}");
			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue("_wcf.search.internal.filterquery", "isreal:1");
			searchCriteria.setControlParameterValue("_wcf.search.internal.mandatory.query", "{!func}geodist(location,"+latitude+","+longitude+")");
			searchCriteria.setControlParameterValue("_wcf.search.internal.response.fields", "*,score");
			searchCriteria.setControlParameterValue("_wcf.search.internal.sort", "score asc");
			searchCriteria.setPagingMaxItems(limit);
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findNearestStoreBySortedDistance(integer,String, String)");
		}
		return result;

	}
	
	@GET
	@Path("byCountryAbbrGetAllStates")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets All states")
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findStateInCountry(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStateInCountry(int, String, String)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findNearestStore(integer,String, String)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		
		try {	
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byCountryAbbrGetAllStates");
			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:0");
			
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findNearestStore(integer,String, String)");
		}
		return result;

	}
	@GET
	@Path("byStateAbbrGetAllCity/{stateabbr}")
	@Produces({ "application/json", "application/xml", "application/xhtml+xml",
			"application/atom+xml" })
	@Description("Gets All states")
	@ResponseSchema(parameterGroup = "storeSummary", responseCodes = {
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 200, reason = "The requested completed successfully."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 400, reason = "Bad request. Some of the inputs provided to the request aren't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 401, reason = "Not authenticated. The user session isn't valid."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 403, reason = "The user isn't authorized to perform the specified request."),
			@com.ibm.commerce.rest.javadoc.ResponseCode(code = 500, reason = "Internal server error. Additional details will be contained on the server logs.") })
	public Response findCityInState(
			@PathParam("storeId") @ParameterDescription(description = "The store identifier.", required = true) String storeId,
			@PathParam("stateabbr") @ParameterDescription(description = "The store identifier.", required = true) String stateAbbr,

			@QueryParam("responseFormat") @ParameterDescription(description = "The response format. If the request has an input body, that body must also use the format specified in \"responseFormat\". Valid values include \"json\" and \"xml\" without the quotes. If the responseFormat isn't specified, the \"accept\" HTTP header shall be used to determine the format of the response. If the \"accept\" HTTP header isn't specified as well, the default response format shall be in json.", required = false) String responseFormat) {
		String METHODNAME = "findStateInCity(int, String, String)";

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.entering(CLASSNAME, METHODNAME,
					"findCityInState(integer,String, String)");
		}
		SearchCriteria searchCriteria = null;
		Response result = null;
		
		try {	
			searchCriteria = initSearchCriteria(storeId, "GET",
					"com.ibm.commerce.catalog", "storeview",
					"store/{storeId}/storeview/byStateAbbrGetAllCity/{stateabbr}");
			throwRestExceptionIfErrorsAreDetected();
			searchCriteria.setControlParameterValue(
					"_wcf.search.internal.optional.query", "isreal:0 AND stateabbr:" + stateAbbr);
			
			result = performSearch(searchCriteria);
		} catch (Exception e) {
			result = generateResponseFromRespData(searchCriteria, null, e);
		}

		if (LoggingHelper.isEntryExitTraceEnabled(LOGGER)) {
			LOGGER.exiting(CLASSNAME,METHODNAME,
					"findCityInState(integer,String, String)");
		}
		return result;

	}
	
	@Override
	public String getResourceName() {
		return "storeview";
	}

}
